<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->string('user_id', 50)->unique('IDX_user_PK');
			$table->string('user_name', 100)->index('IDX_user_1');
			$table->string('user_pass', 100);
			$table->string('user_email', 100);
			$table->string('user_avatar', 999);
			$table->string('real_name', 100)->nullable();
			$table->string('title', 100)->nullable();
			$table->string('tz', 100)->nullable();
			$table->string('token', 999)->nullable();
			$table->boolean('is_admin')->default(0);
			$table->boolean('is_owner')->default(0);
			$table->integer('notified')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->string('team_id', 50)->index('IDX_user_2_FK');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
