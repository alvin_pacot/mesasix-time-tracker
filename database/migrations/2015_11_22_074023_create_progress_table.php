<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('progress', function(Blueprint $table)
		{
			$table->bigInteger('prog_id', true);
			$table->string('description', 9999);
			$table->timestamps();
			$table->softDeletes();
			$table->string('user_id', 50)->index('IDX_progress_1_FK');
			$table->bigInteger('id')->index('IDX_progress_2_FK');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('progress');
	}

}
