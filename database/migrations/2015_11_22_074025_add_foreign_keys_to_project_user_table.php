<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_user', function(Blueprint $table)
		{
			$table->foreign('project_id', 'project_project_user')->references('project_id')->on('project')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'user_project_user')->references('user_id')->on('user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_user', function(Blueprint $table)
		{
			$table->dropForeign('project_project_user');
			$table->dropForeign('user_project_user');
		});
	}

}
