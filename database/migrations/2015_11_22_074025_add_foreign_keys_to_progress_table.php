<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProgressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('progress', function(Blueprint $table)
		{
			$table->foreign('id', 'time_log_progress')->references('id')->on('time_log')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'user_progress')->references('user_id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('progress', function(Blueprint $table)
		{
			$table->dropForeign('time_log_progress');
			$table->dropForeign('user_progress');
		});
	}

}
