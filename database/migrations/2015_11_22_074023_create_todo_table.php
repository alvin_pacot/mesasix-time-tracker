<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTodoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('todo', function(Blueprint $table)
		{
			$table->bigInteger('todo_id', true);
			$table->string('description', 9999);
			$table->timestamps();
			$table->softDeletes();
			$table->string('user_id', 50)->index('IDX_todo_2_FK');
			$table->bigInteger('id')->index('IDX_todo_3_FK');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('todo');
	}

}
