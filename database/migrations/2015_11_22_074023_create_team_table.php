<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team', function(Blueprint $table)
		{
			$table->string('team_id', 50)->unique('IDX_team_PK');
			$table->string('team_domain', 100)->nullable();
			$table->string('team_status', 50)->nullable()->default('FREE');
			$table->string('team_name', 100);
			$table->string('team_tz', 40)->nullable();
			$table->string('access_token', 500);
			$table->string('channel', 100)->nullable();
			$table->string('config_url', 9999)->nullable();
			$table->string('hook_url', 9999)->nullable();
			$table->boolean('notify')->nullable()->default(1);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team');
	}

}
