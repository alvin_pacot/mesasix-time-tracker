<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project', function(Blueprint $table)
		{
			$table->bigInteger('project_id', true);
			$table->string('project_name', 100);
			$table->string('project_desc', 999)->nullable();
			$table->timestamp('project_start')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('project_due')->nullable();
			$table->string('project_stat', 100)->default('ONGOING');
			$table->string('channel', 50)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('team_id', 50)->index('IDX_project_1_FK');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project');
	}

}
