<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTodoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('todo', function(Blueprint $table)
		{
			$table->foreign('id', 'time_log_todo')->references('id')->on('time_log')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'user_todo')->references('user_id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('todo', function(Blueprint $table)
		{
			$table->dropForeign('time_log_todo');
			$table->dropForeign('user_todo');
		});
	}

}
