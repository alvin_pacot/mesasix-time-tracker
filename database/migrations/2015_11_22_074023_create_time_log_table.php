<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimeLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_log', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('location', 100)->default('office');
			$table->string('command', 40);
			$table->timestamps();
			$table->softDeletes();
			$table->string('user_id', 50)->index('IDX_time_log_1_FK');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_log');
	}

}
