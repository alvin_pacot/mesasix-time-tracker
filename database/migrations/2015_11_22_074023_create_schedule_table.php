<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScheduleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedule', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title');
			$table->string('startdate', 48);
			$table->string('enddate', 48);
			$table->string('allDay', 5);
			$table->string('type', 40)->nullable();
			$table->string('user_id', 50)->index('par_ind');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('schedule');
	}

}
