<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Team;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract
{
  use Authenticatable, Authorizable, SoftDeletes;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'user';
  
  protected $primaryKey = 'user_id';
  
  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  // protected $fillable = ['real_name','user_id','user_name', 'user_email', 'user_pass','user_avatar','is_admin','is_owner','title','team_id','timezone','token','notified'];
  protected $guarded = ['created_at','deleted_at','updated_at'];

  /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'is_owner' => 'boolean',
    ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['user_pass'];

  public function getRememberToken()
  {
    return null; // not supported
  }

  public function setRememberToken($value){  }
    
  public function getRememberTokenName()
  {
    return null; // not supported
  }

  /**
   * Overrides the method to ignore the remember token.
   */
  public function setAttribute($key, $value)
  {
    $isRememberTokenAttribute = $key == $this->getRememberTokenName();
    if (!$isRememberTokenAttribute)
    {
      parent::setAttribute($key, $value);
    }
  }
  public function isOwner()
  {
    return ($this->is_owner); 
  }
  public function isAdminOrOwner()
  {
    return ($this->is_admin || $this->is_owner);
  }
  public function getTeam()
  {
    return DB::table('team')->where('team_id',$this->team_id)->first();
  }
  //get associated team created_at
  public function getTeamCreatedAt()
  {
    return DB::select("SELECT created_at FROM team WHERE team_id = '".$this->team_id."' LIMIT 1")[0];
  }
  //get associated team name
  public function getTeamName()
  {
    return DB::select("SELECT team_name FROM team WHERE team_id = '".$this->team_id."' LIMIT 1")[0];
  }
  //get associated unique team domain 
  public function getTeamDomain()
  {
    return DB::select("SELECT team_domain FROM team WHERE team_id = '".$this->team_id."' LIMIT 1")[0];
  }
  //get associated team tz 
  public function getTZ()
  {
    return DB::select("SELECT team_tz as tz FROM team WHERE team_id = '".$this->team_id."' LIMIT 1")[0];
  }
}
