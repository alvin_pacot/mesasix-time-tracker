<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Event extends Model 
{
   /**
   * The database table used by the model.
   *
   * @var string
   */
  
  protected $table = 'schedule';
  protected $primaryKey = 'id';

  // protected $fillable = ['start','user_id','end','text','resources'];
  protected $fillable = ['startdate','user_id','enddate','title','allDay','type'];
  public $timestamps = false;

}
