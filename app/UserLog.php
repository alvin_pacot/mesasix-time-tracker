<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserLog extends Model
{
   /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'time_log';
  
  protected $primaryKey = 'id';

  protected $guarded = ['created_at','deleted_at','updated_at'];

}
