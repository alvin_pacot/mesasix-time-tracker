<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTodo extends Model
{
    /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'todo';
  
  protected $primaryKey = 'todo_id';

  protected $guarded = ['created_at','deleted_at','updated_at'];
}
