<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \DateTime;

class TimeAdder extends Model
{
	private $hr = 0;
    private $min = 0;
    private $sec = 0;
    private $totaltime = '00:00:00';
    private $totalsec = 0;
    public function __construct($times){        
        if(is_array($times)){
            foreach ($times as $key => $val) {
                    $split = explode(":", @$val); 
                    $this->hr += @$split[0];
                    $this->min += @$split[1];
                    $this->sec += @$split[2];
            }
            $seconds = $this->sec % 60;
            $minutes = $this->sec / 60;
            $minutes = (integer)$minutes;
            $minutes += $this->min;
            $hours = $minutes / 60;
            $minutes = $minutes % 60;
            $hours = (integer)$hours;
            $hours += $this->hr % 24;
            $this->totaltime = $hours.":".$minutes.":".$seconds;
        }
    }
    public function getTotalTime(){
        return $this->totaltime;
    }
    public function getTotalSec(){
        $s = explode(':',$this->totaltime);
        $this->totalsec += (intval($s[0]) * 60 * 60);
        $this->totalsec += (intval($s[1]) * 60);
        $this->totalsec += intval($s[0]);
        return $this->totalsec;
    }
}
