<?php

namespace App\Http\Middleware;

use App\Repositories\TeamRepository;
use App\Repositories\ResponseRepository;
use Closure;
use Request;
use Redirect;
class SlackMiddleware
{
    protected $response;
    protected $team;

    public function __construct(TeamRepository $team,ResponseRepository $response)
    {
        $this->response = $response;
        $this->team = $team;    
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->team->isRegistered(Request::get('team_id'))) {
            if (Request::get('user_id') !== 'USLACKBOT') {
                return $next($request);
            }
            return;
        }
        return $this->response->notRegistered();
    }
}
