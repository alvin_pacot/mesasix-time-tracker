<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class OnlyAdminOrOwner
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check() && ($this->auth->user()->isAdminOrOwner())) 
        {
            return $next($request);
        }       
        return redirect()->route('dashboard::myprofile',['team'=>$this->auth->user()->getTeamDomain()->team_domain,'user'=>$this->auth->user()->user_name]);
    }

}