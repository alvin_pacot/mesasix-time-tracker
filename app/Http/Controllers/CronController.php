<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use App\Repositories\LogRepository;
use App\Repositories\ResponseRepository;
use App\Repositories\SlackHelper;
use Carbon\Carbon;
use App\UserLog;
class CronController extends Controller
{
  //this use for testing only should not use in actual deployment
    //Instead use the console command provided by the php artisan
    protected $team;
    protected $slack;
    protected $member;
    protected $msg;
    protected $log;

    public function __construct(ResponseRepository $msg,TeamRepository $team, SlackHelper $slack ,UserRepository $member,LogRepository $log)
    {
        $this->team = $team;
        $this->slack = $slack;
        $this->msg = $msg;
        $this->log = $log;
        $this->member = $member;
    }
    public function handle()
    {
        $teams = $this->team->getTeamWithNotifyOn();
        foreach ($teams as $key => $team) {
            $users = $this->member->getWorkingTeamMembers($team->team_id);
            foreach ($users as $key => $user) {
                if ($user->notified == 1) { //notified but does not extend
                    $this->doCutWork($user->user_id);
                    $this->member->doSetUserNotify($user->user_id,0);
                    $this->doNotify($team->channel,$team,$this->msg->doesnt_extend($user->user_name));
                }elseif ($user->notified == 2) { //extend
                    if($this->isWorkOver($user->user_id,16)) //16 hours of work during shift is non-productive
                    {
                        $this->doCutWork($user->user_id);
                        $this->member->doSetUserNotify($user->user_id,0);
                        $this->doNotify("@".$user->user_name,$team,$this->msg->already_extend($user->user_name));
                    }//else ignore this user
                }elseif ($user->notified == 0) { //neither notified nor extend
                    if ($this->isWorkOver($user->user_id,8)) {
                        $this->member->doSetUserNotify($user->user_id,1);
                        $this->doNotify("@".$user->user_name,$team,$this->msg->ask_extend($user->user_name));
                    }//else ignore this user
                }//if it reach this the notified has invalid value
            }
        }
        $data = array(
          'text' => 'Cron exec at '.date('Y-m-d H:i:s',time()),
          'channel' => '@kirby',
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true
        );
      	$this->slack->sendToSlack('xoxp-7363994471-7364017361-13366112725-82d84b3b2a',$data);        
        return json_encode(['status'=>"DONE"]);
    }
    public function doCutWork($user_id)
    {
       $newLog = new UserLog();
       $newLog->command = 'out';
       $newLog->user_id = $user_id;
       $newLog->save(); 
    }
    public function doNotify($channel,$team,$msg)
    {
        $access_token = $team->access_token;
        $data = array(
          'text' => $msg,
          'channel' => $channel,
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true
        );
      $this->slack->sendToSlack($access_token,$data);
    }
    public function isWorkOver($user_id,$hour)
    {
        $last_record = $this->log->getLastInId($user_id);
        if(!$last_record){
          return false;
        }else{
          $work_time = $this->log->getUserWorkingHour($user_id,$last_record[0]->id);//seconds
          return ($work_time >= ($hour*60*60)) ? true : false;
        }
    }
}
