<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Repositories\SlackHelper;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;


class TeamRegistrationController extends Controller
{
  	protected $baseUri = "https://slack.com/oauth/authorize";
    protected $scope = "incoming-webhook";//,commands
    protected $team_id;
    protected $request;
    protected $helper;
    protected $teams;
    protected $users;

    public function __construct(Request $request,SlackHelper $helper,TeamRepository $teams, UserRepository $users)
    {
    	$this->request = $request;
    	$this->helper = $helper;
    	$this->teams = $teams;
    	$this->users = $users;
    }
    public function promptRegistrationWithId($teamId)
    {
      if ($this->teams->isRegistered($teamId)) {
        alert()->error('Your team was already registered to '.env('APP_NAME').'.If you think there is something problem with this matter, Please do contact our support team.','Registration')->autoclose(4000);
        return Redirect::route('teamRegStart');
      }
      alert()->success('Authentication success.Click the PROCEED button and we will setup some of important configuration for your team.','Authentication')->persistent('OK');
      return view('pages.team_registration',['teamId'=>$teamId]);
    }
    public function registrationRedirectToSlackButton()
    {
      if (Input::has('team_id')) {
    	 $this->team_id = Input::get('team_id'); 
      }
      $this->request->getSession()->set('action','registration');
    	return redirect()->away($this->getURLToSlackButton());
    }
    public function handleSlackCallback()
    {
    	//check if there is code and valid state
	  	if( $this->hasCode() && $this->hasValidState()){
	   		return $this->requestTokenAndTeamWebhook(Input::get('code'));
	   	} else {
        alert()->error('Authorization failed. Invalid state or code.','Authorization')->persistent('Close');
        $this->request->getSession()->flush();
	   		return view('pages.setup_start');
	   	}
    }

    public function requestTokenAndTeamWebhook($code)
    {
    	$hook_data = $this->helper->getTeamWebhook($code);
      if($this->hasValidData($hook_data))
      {
        $all_data = $this->helper->getAllData($hook_data['access_token']);
      	$team = $this->teams->findByTeamIdOrCreate(array_merge($hook_data,$all_data));
        $this->saveMembers($all_data,$hook_data);
          alert()->success('Team '.$team->team_name.', Successfully added '.env('APP_NAME').' integration. Complete the setup by providing the ff:','Complete Setup')->persistent('OK');
          return view('pages.team_setup',['stage'=>3,'teamId'=>$all_data['team']['id']]);
      } else {
          alert()->error('Session expired. Please try again.','Session')->persistent('OK');
          return view('pages.team_setup',['teamId'=>$this->team_id]);
      }
    }

    protected function saveMembers($data,$hook_data)
    {
      $team_id = $data['team']['id'];
      $hook = $hook_data['hook_url'];
      $token = $hook_data['access_token'];
      foreach ($data['users'] as $key => $val) {
        if($this->users->isValidMember($val)){
          $pass = Str::random(10);
          $u = $this->users->createUser($val,$team_id,$token,$pass);
          $this->notifyUserforPassword($u->user_name,$u->user_email,$pass,$token);
        }
      }
    }

    public function notifyUserforPassword($username,$email,$password,$token)
    {
      $msg = "Your team has been registered to ".env('APP_NAME').". Your ".env('APP_NAME')." account has been created. Username: ".$email." Password: ".$password." visit ".env('APP_LINK')."/login to login. You can change your password after you login.";
      $data = array(
          'text' => $msg,
          'channel' => "@".$username,
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true
        );
      $this->helper->sendToSlack($token,$data);
    }
    protected function getURLToSlackButton()
    {
  		$state = Str::random(40);
  		$data = [
  			'team' => $this->team_id,
  			'scope' => $this->scope,
  			'client_id' => env("CLIENT_ID"),
  			'redirect_uri' => env("REDIRECT_URI_BUTTON"),
  			'state' => $state
  			];
      	$this->request->getSession()->set('state', $state);
      	return $this->baseUri.'?'.http_build_query($data);
    }

  protected function isAdminOrOwner($data)
  {
    return ($data['is_admin'] || $data['is_owner']);
  }

  protected function hasValidData($data)
  {
    return (empty($data) || $data === null) ? false : true;
  }

  protected function hasCode()
  {
    return Input::has('code'); 
  }

  protected function hasValidState()
  {
    if (Input::has('state') && ($this->request->getSession()->get('state') == Input::get('state'))) {
      return true;
    }
      return false;
  }
}
