<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Listeners\AuthenticateUserListener;
use Illuminate\Contracts\Auth\Guard; 
use App\Repositories\SlackHelper;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use Auth;
use Input;
use Redirect;
use Session;
class SlackAuthController extends Controller implements  AuthenticateUserListener 
{   
  protected $helper;
  protected $request;
  protected $team;
  protected $user;
  protected $auth;
  protected $scopes = 'identify search:read users:read chat:write:bot channels:read chat:write:user team:read';
  public function __construct(Guard $auth, SlackHelper $helper,Request $request, TeamRepository $team,UserRepository $user)
  {
    $this->helper = $helper;
    $this->request = $request;
    $this->team = $team;
    $this->user = $user;
    $this->auth = $auth;
  }

  public function signInWithSlack()
  {
    $this->request->getSession()->set('action',Input::get('action'));
    return redirect()->away($this->getRedirectToSlackURL());
  }

  public function handleSlackCallback()
  {
    //check if there is code and valid state
    if( $this->hasCode() && $this->hasValidState()) {
      return $this->requestToken(Input::get('code'));
    } else {
      if ($this->request->getSession()->get('action') == 'login') {
       alert()->error('Authentication failed. Access denied','Authentication')->persistent('OK');
       return Redirect::route('login::login');
      }else{
        alert()->error('Authentication failed. Access denied.Please check the requirement for team registration.','Authentication')->persistent('OK');
        return Redirect::route('teamRegStart');
      }
    }
  }

  public function requestToken($code)
  {
    $token = $this->helper->getToken($code);
    if ($this->hasValidData($token)) { 
      return $this->authenticateTeam($token); 
    } else {
      alert()->error('Token request error','Error')->persistent('OK');
      if ($this->request->getSession()->get('action') == 'login') {
        return Redirect::route('login::login');
      }else{
        return Redirect::route('teamRegStart');
      }
    }
  }
  
  protected function authenticateTeam($token)
  {  
    //get user_id and team_id of the user
	 	$result = $this->helper->getAllData($token);
	  if ($this->hasValidData($result)) {
      //check if team is registered to app
	  	if ($this->team->isRegistered($result['team']['id']) &&
          $this->request->getSession()->get('action') == 'login') {
          return $this->authenticateUser($result,$token);
      }else{
        return $this->promptToRegister($result);
      }
    }else{
      alert()->error('Error while requesting for user information. Please try again.','Error')->persistent('OK');
      $this->request->getSession()->flush();
      if ($this->request->getSession()->get('action') == 'login') {
       return Redirect::route('login::login');
      }else{
        return Redirect::route('teamRegStart');
      }
	  }
  }

  public function authenticateUser($arr,$token)
  {
    //get user detail from app then update info from slack
    $self = $this->user->findByUserIdOrCreate($arr,$token); 
    if ( $this->hasValidData($self)) {
      //sign in the user
      $this->auth->login($self, true);
      //redirect user
      return $this->userHasLoggedIn($self);
    }else{
      alert()->error('Authentication failed, This user has been deleted/disabled','Error')->persistent('OK');
      $this->request->getSession()->flush();
      if ($this->request->getSession()->get('action') == 'login') {
       return Redirect::route('login::login');
      }else{
        return Redirect::route('teamRegStart');
      }  
    }  
  }

  protected function promptToRegister($arr)
  {
    $index = $this->user->getIndex($arr);
    if($this->isAdminOrOwner($arr['users'][$index]) ){
      if ($this->request->getSession()->get('action') == 'login') {
        alert()->error('Your team was not registered to '.env('APP_NAME').' Click PROCEED if you want to registered your team','Error')->persistent('OK');
      }
      return redirect()->route('teamRegWithId',['teamId'=>$arr['team']['id']]);
    }else{
      alert()->error('Your are not authorized to register your team. Please ask your team admin/owner to sign up.','Error')->persistent('OK');
      $this->request->getSession()->flush();
      return Redirect::route('teamRegStart');
    }
  }

  protected function isAdminOrOwner($data)
  {
    return ($data['is_primary_owner'] || $data['is_admin'] || $data['is_owner']);
  }

  protected function hasValidData($data)
  {
    return ($data === null) ? false: true;
  }

  protected function hasCode()
  {
    return Input::has('code'); 
  }

  protected function hasValidState()
  {
    return (Input::has('state') && ($this->request->getSession()->get('state') == Input::get('state')));
  }
  
  protected function getRedirectToSlackURL()
  {
    $state = Str::random(40);
    $params = ['client_id' => env('CLIENT_ID'),
              'redirect_uri' => env('REDIRECT_URI'),
  	          'state' => $state,
              'scope' => $this->scopes,
              ];
    $this->request->getSession()->set('state', $state);
    return 'https://slack.com/oauth/authorize?'.http_build_query($params);
  }

  public function userHasLoggedIn($user)
  {
    return Redirect::route('dashboard::myprofile',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]);
  }

  public function userHasLogout() 
  {
    if(Auth::check()){
      Auth::logout();
      Session::flush();
      alert()->success('You have been logged out.', 'Good bye!')->autoclose(4000);
    }
    return Redirect::route('home');
  }
}
