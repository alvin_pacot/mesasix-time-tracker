<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ResponseRepository;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use App\Repositories\LogRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SlackHelper;
use Illuminate\Support\Facades\DB;
use App\Team;
use App\User;
use Input;
class SlashCommandController extends Controller
{
	protected $response;
	protected $slack;
	protected $teamrepo;
	protected $userrepo;
	protected $data;
    protected $log;
    protected $user;
	public function __construct(LogRepository $log,ResponseRepository $response,SlackHelper $slack,TeamRepository $teamrepo,UserRepository $userrepo)
    {
    	$this->response = $response;
    	$this->slack = $slack;
    	$this->userrepo = $userrepo;
    	$this->teamrepo = $teamrepo;
        $this->log = $log;
    }
    public function handleSlashCommand($data)
    {	
    	$cmd = self::trimIt(explode(' ',$data['text']));
        $this->user = User::where('user_id',$data['user_id'])->first();
        if (empty($this->user)) {
            return "You are not allowed to used this dedicated slash command. This command is exclusive only for non-restricted member of the team";
        }
    	if ((strtolower($cmd[0]) == 'check'|| strtolower($cmd[0]) == 'c' )&& sizeof($cmd) >= 3) {
    		if (strtolower($cmd[1]) == 'my' ) {
    			return "Your ".$this->computeCurrent(strtolower($cmd[2]),$data['user_id'],$data['team_id']);
    		}elseif (($cmd[1][0]) == '@') {
    			$target = $this->userrepo->getTeamMemberByName($data['team_id'],substr($cmd[1], 1));
    			if (!$target) { return 'User '.$cmd[1].' not found'; }
    			else{
    				return $cmd[1]." ".$this->computeCurrent(strtolower($cmd[2]),$target[0]->user_id,$data['team_id']);
    			}
    		}
    		return "Invalid command. For command list enter `/m6 help`.";
    	}elseif(strtolower($cmd[0]) == 'extend'){
    		if ($this->userrepo->isUserNotified($data['user_id'])) {
    			$this->userrepo->doSetUserNotify($data['user_id'],2);
    			return $this->response->valid_extend();
    		}else
    			return $this->response->invalid_extend();
    	}else
    		return $this->response->help();
    }
    public function computeCurrent($cmd,$user_id,$team_id)
    {

    	if ($cmd == 'shift' || $cmd == 'sh') {
    		$gained = $this->log->checkCurrentShift($user_id,$this->user->tz);
    		return " current shift accumulated is ".$gained;
    	}elseif($cmd == 'week' || $cmd == 'w'){
            $week = $this->log->checkCurrentWeek($user_id);
    		return " current weekly accumulated is ".$week;
    	}elseif($cmd == 'month' || $cmd == 'm'){
            $month = $this->log->checkCurrentMonth($user_id);
    		return " current monthly accumulated is ".$month;
    	}elseif($cmd == 'year' || $cmd == 'y'){
    		$year = $this->log->checkCurrentYear($user_id);
            return " current yearly accumulated is ".$year;
    	}elseif($cmd == 'status' || $cmd == 'st'){
    		return " current status is `".$this->log->checkCurrentStatus($user_id)."`";
    	}else{
    		return "command `".$cmd."` is not yet supported";
    	}
    }
    public static function trimIt($str = array())
    {
    	$list = [];
    	foreach ($str as $key => $value) {
    		if (!empty($value)) { $list[] = trim($value); }
    	}
    	return $list;
    }
}
