<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OutGoingWebhookController as Outgoing;
use App\Http\Controllers\SlashCommandController as Slash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;

/**
* Split the management of task to approriate controller depending of where the request came from
*/
class APIController extends Controller
{
	protected $outgoing;
	protected $slash;
	
	public function __construct(Outgoing $outgoing, Slash $slash)
	{
		$this->outgoing = $outgoing;
		$this->slash = $slash;
	}

    public function index()
    {   
    	if(Input::has('trigger_word')) //from outgoing webhook
            return $this->outgoing->handleOutgoingWebhook(Input::all());
    	return $this->slash->handleSlashCommand(Input::all());
    }
}
