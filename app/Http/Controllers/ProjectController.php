<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;
use App\Repositories\SlackHelper;
use App\Http\Requests;
use Carbon\Carbon;
use App\TimeAdder;
use App\UserTask;
use App\Project;
use App\User;
use Redirect;
use Session;
use Auth;
use DB;

class ProjectController extends Controller
{
	protected $repo;
  protected $helper;
  protected $users;

	public function __construct(ProjectRepository $repo,SlackHelper $helper, UserRepository $users)
	{
		$this->repo = $repo;
    $this->helper = $helper;
    $this->users = $users;
	}

    public function index()
    {
    	$projects = Project::where('team_id','=',(Auth::user()->team_id))->orderBy('project_name')->paginate(10);
      $projects->setPath(route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]));
      if (!empty($projects)) {
        foreach ($projects as $key => $project) {
          $overall = empty($project->project_due) ? 0: (int) dateDifference(Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d'),Carbon::now()->format('Y-m-d'));
          $project->id = $project->project_id;
          $project->name = $project->project_name;
          $project->desc = empty($project->project_desc) ? 'none' :$project->project_desc;
          $project->project_start_det = $this->format($project->project_start);
          $project->project_due_det = empty($project->project_due) ? 'NOT SET' : $this->format($project->project_due);
          $project->startformat = Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d');
          $project->dueformat = empty($project->project_due) ? '' : Carbon::createFromFormat('Y-m-d H:i:s',$project->project_due)->format('Y-m-d');
          $project->project_stat = $project->project_stat;
          $project->channel = $project->channel;
          $project->span = empty($project->project_due) ? 'Not Set' : ((int) dateDifference(Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d'),Carbon::createFromFormat('Y-m-d H:i:s',$project->project_due)->format('Y-m-d'))+1)." days";
          $pu_user = $this->repo->getProjectUser($project->project_id);
          $ass = [];
          if (!empty($pu_user)) { 
            foreach ($pu_user as $key => $user) {
             $ass[] = [$user->user_name,$user->task,$user->pu_id,$user->user_id];
            }
          }
          $project->assigned = $ass;
          $bar = [];
          if(empty($project->project_due) || is_null($project->project_due)){
            $dff = dateDifference(Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d'),Carbon::now()->format('Y-m-d'));
            $bar[] = [50,'green',($dff+1).' days since project started'];
            $bar[] = [50,'blue','Project due date not set'];
          }elseif ($project->project_stat == 'COMPLETED') {
            $bar[] = [100,'purple','Project complete'];
          }elseif ($project->project_stat == 'DUE') {
            $bar[] = [100,'red lighten-1','Project is due'];
          }elseif ($project->project_stat == 'NOT YET STARTED') {
            $bar[] = [100,'blue','Project is not yet started'];
          }elseif ($project->project_stat == 'ONGOING' && $project->project_due == date('Y-m-d 00:00:00',time())) {
            $dff = dateDifference(Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d'),Carbon::now()->format('Y-m-d'));
            $bar[] = [90,'green',($dff+1).' days since project started'];
            $bar[]  = [10,'purple','Due date is today'];
          }else{
              $all = dateDifference(Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d'),Carbon::createFromFormat('Y-m-d H:i:s',$project->project_due)->format('Y-m-d'));
              $dff = dateDifference(Carbon::createFromFormat('Y-m-d H:i:s',$project->project_start)->format('Y-m-d'),Carbon::now()->format('Y-m-d'));
              $bar[] = [ self::calcPercentage($dff,$all),'red lighten-1',$dff." days since projetc started"];
              $bar[] = [ self::calcPercentage(($all-$dff),$all),'green',($all-$dff)." days remaining"];
          }
          $project->bar = $bar;
      }//endforeach   
    }//endif
        $channels = $this->getChannelList(Auth::user()->token);
        // $channels = [];
        $userslist = $this->getUserList(Auth::user()->team_id);
        if (Session::has('success')) {
          $success = Session::get('success');
          return view('pages.team_project',compact('projects','userslist','channels','success'));    
        }elseif (Session::has('error')) {
          $success = Session::get('error');
          return view('pages.team_project',compact('projects','userslist','channels','error'));    
        }
        return view('pages.team_project',compact('projects','userslist','channels'));
    }
    public function getUserList($team_id)
    {
      $users = $this->users->getTeamMembers($team_id);
      if($users)
      {
        $user = [];
        foreach ($users as $key => $value) {
            $user[] = ['user_id'=>$value->user_id,'user_name'=>$value->user_name,'avatar'=>$value->user_avatar];
        }
        return $user;
      }
      return [];
    }
    public function getChannelList($token)
    {
      $channels =  $this->helper->getChannelList($token);
      if(!empty($channels))
      {
        $ch = [];
        foreach ($channels as $key => $value) {
            $ch[] = ['channel_id'=>$value['id'],'channel_name'=>$value['name']];
        }
        return $ch;
      }
      return [];
    }
    public static function calcPercentage($seconds,$overall){
        if ($overall == 0 || $seconds > $overall) {
            return 0;
        }
        return ($seconds * 100 )/$overall;
    }
    public function format($date)
    {
    	return Carbon::parse($date)->format('M, j Y');
    }
    public function getSecondSinceStart($start)
    {
    	if (Carbon::parse($start) >= Carbon::now()) {
    		return 0;
    	}else{
    		return DB::select("SELECT TIMESTAMPDIFF(SECOND,'".$start."',CURRENT_TIMESTAMP) as diff, SEC_TO_TIME(TIMESTAMPDIFF(SECOND,'".$start."',CURRENT_TIMESTAMP)) as hour");
    	}
    }
    public function removeProject(Request $request)
    {
       $project = Project::where('project_id',$request->get('project_id'))->first();
       Project::destroy($request->get('project_id'));
       if ($request->has('notifyChannelRemove')) {
           $msg = "Project Name :".$project->project_name;
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$project->project_name." project  has been removed",
              'text'=>"Removed on ".Carbon::now()->format('F j Y g:i A P'),
              'color'=> "warning",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];
           $this->notifyChannel($msg,Auth::user()->token,$project->channel,$attachments);
       }
       alert()->success("Removing project complete.",'Success')->autoclose(3000);
       return Redirect::route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]);
    }
    public function createProject(Request $request)
    {
      $projects = [
        'name' => $request->get('name'),
        'desc' => $request->get('desc'),
        'start'=> $request->get('start'),
        'end' => ($request->has('end')) ? $request->get('end') : null,
        'stat' => $request->get('stat'),
        'channel' => $request->get('channel'),
        'team_id' => Auth::user()->team_id
        ];
       $project = $this->repo->createProject($projects);
       if (!$project) {
            alert()->error("Project creation failed.",'Error')->autoclose(3000);
       }
       if ($request->has('notify')) {
           $msg = "Project name: *".$request->get('name')."* was created for this channel.";
           $end = ($request->has('end')) ? $request->get('end') : 'Not set';
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$request->get('name')." project created",
              'text'=>"Description: ".$request->get('desc')."\nStart date: ".$request->get('start')."\nDue date: ".$end,
              'color'=> "good",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];
           $this->notifyChannel($msg,Auth::user()->token,$request->get('channel'),$attachments);
       }
       alert()->success("Project successfully created.",'Success')->autoclose(3000);
       return Redirect::route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]);
    }
    public function updateProject(Request $request)
    {
        $proj = Project::where('project_id',$request->get('uproject_id'))->where('team_id',Auth::user()->team_id)->first();
        if(!$proj){
          alert()->error("Project updating failed.",'Error')->autoclose(3000);
        }else{
          $proj->project_name = $request->get('name');
          $proj->project_desc = $request->get('desc');
          $proj->project_start = $request->get('start');
          $proj->project_due = ($request->has('end')) ? $request->get('end') : null;
          $proj->project_stat = $request->get('stat');
          $proj->channel = $request->get('channel');
          $proj->save();
        }
        if ($request->has('notify')) {
           $pname = ($request->get('oldname') == $request->get('name')) ? $request->get('name') : $request->get('name')." (previously ".$request->get('oldname').")";
           $msg = "Project name: *".$pname."*";
           $end = ($request->has('end')) ? $request->get('end') : 'Not set';
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$request->get('name')." project was updated",
              'text'=>"Description: ".$request->get('desc')."\nStart date: ".$request->get('start')."\nDue date: ".$end,
              'color'=> "good",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];
           $this->notifyChannel($msg,Auth::user()->token,$request->get('channel'),$attachments);
        }
       alert()->success("Project successfully updated.",'Success')->autoclose(3000);
       return Redirect::route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]);
    }
    public function addTask(Request $request)
    {
      $projects = [
        'project_id' => $request->get('project_id'),
        'user_id' => $request->get('user_id'),
        'task'=> $request->get('task'),
        ];
       $task = $this->repo->createUserTask($projects);
       if (!$task) {
           alert()->error("Assigning task failed.",'Error')->autoclose(3000);
       }
       $project = Project::where('project_id',$request->get('project_id'))->first();
       $user = User::where('user_id',$request->get('user_id'))->first();
       if ($request->has('notifyChannel')) {
           $msg = "Project Name :".$project->project_name;
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$project->project_name." assigning of task",
              'text'=>"@".$user->user_name." task : ".$request->get('task')."\nAsk the author for more details",
              'color'=> "good",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];
           $this->notifyChannel($msg,Auth::user()->token,$project->channel,$attachments);
       }
       if ($request->has('notifyUser')) {
           $msg = "Project Name :".$project->project_name;
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$project->project_name." assigning of task",
              'text'=>"@".$user->user_name." task : ".$request->get('task')."\nChannel :".$project->channel."\nAsk the author for more details",
              'color'=> "good",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];
           $this->notifyChannel($msg,Auth::user()->token,"@".$user->user_name,$attachments);
       }
       alert()->success("Assigning of task complete.",'Success')->autoclose(3000);
       return Redirect::route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]);
    }
    public function removeTask(Request $request)
    {
       $pu_id  = $request->get('project_user_id');
       UserTask::destroy($pu_id);
       $project = Project::where('project_id',$request->get('project_id'))->first();
       $user = User::where('user_id',$request->get('user_id'))->first();
       if ($request->has('notifyChannelRemove')) {
           $msg = "Project Name :".$project->project_name;
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$project->project_name." project  has been removed",
              'text'=>"@".$user->user_name." has been removed from his/her task.",
              'color'=> "warning",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];
           $this->notifyChannel($msg,Auth::user()->token,$project->channel,$attachments);
       }
       if ($request->has('notifyUserRemove')) {
           $msg = "Project Name :".$project->project_name;
           $attachments = [
              'fallback'=>$msg,
              'pretext'=>$project->project_name." project  has been removed",
              'text'=>"@".$user->user_name.", your task in ".$project->project_name." has been removed.",
              'color'=> "warning",
              'author_name'=>Auth::user()->user_name,
              'author_icon'=>Auth::user()->user_avatar
           ];         
           $this->notifyChannel($msg,Auth::user()->token,"@".$user->user_name,$attachments);
       }
       alert()->success("Removing of task complete.",'Success')->autoclose(3000);
       return Redirect::route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]);
    }
    public function notifyChannel($msg, $token,$channel,$attachment="")
    {
      $projects = array(
          'text' => $msg,
          'channel' => $channel,
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true,
          'attachments'=>json_encode([$attachment])
        );
      $this->helper->sendToSlack($token,$projects);
    }
}
