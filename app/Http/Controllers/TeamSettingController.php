<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use App\Repositories\SlackHelper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\TimeAdder;
use App\Team;
use App\User;
use Auth;
use Redirect;
use Session;
use DB;
class TeamSettingController extends Controller
{
	protected $trepo;
	protected $urepo;
	protected $slack;

	public function __construct(TeamRepository $trepo,UserRepository $urepo,SlackHelper $slack)
	{
		$this->trepo = $trepo;
		$this->urepo = $urepo;
		$this->slack = $slack;
	}
    public function index()
    {

    	$teamInfo = $this->trepo->getTeamById(Auth::user()->team_id);
    	$teamMembers = $this->urepo->getTeamMembersWithTrashed(Auth::user()->team_id);
    	$zones = $this->getTimezones();
        $tab = (Session::has('tab'))? Session::get('tab') : 1;
    	return view('pages.team_setting',compact('teamInfo','teamMembers','zones','tab'));
    }
    public function updateTeam()
    {
    	$slackInfo = $this->slack->getAllData(Auth::user()->token);
    	$currentInfo = $this->trepo->getTeamById(Auth::user()->team_id);
    	$hasChanged = $this->trepo->updateTeam($slackInfo['team'],$currentInfo);
        if (!$hasChanged) {
            alert()->message("Team info was already up to date, nothing has been changed.",'Team Info')->persistent('OK');
        }
    	alert()->message("Team info successfully updated.",'Success')->autoclose(3000);
    	return Redirect::route('dashboard::teamsetting',['team'=>Auth::user()->getTeamDomain()->team_domain])->with('tab',1);
    }
    public function updateSetting(Request $request)
    {
    	$tz = $request->tz;
    	$notified = $request->notified;
    	$team = Team::where('team_id',Auth::user()->team_id)->first();
    	$team->team_tz = $tz;
    	$team->notify = (strtolower($notified)=='on')? 1 : 0;
    	$team->save();
    	alert()->message("Team setting successfully updated.",'Success')->autoclose(3000);
    	return Redirect::route('dashboard::teamsetting',['team'=>Auth::user()->getTeamDomain()->team_domain])->with('tab',3);
    }
    public function userEnable(Request $request)
    {
        if($request->has('user_id')){
            User::withTrashed()->where('user_id',$request->get('user_id'))->restore();
            alert()->message("Enabling account success",'Success')->autoclose(3000);
        }else{
            alert()->error("Enabling account failed",'Error')->persistent('OK');
        }
        return Redirect::route('dashboard::teamsetting',['team'=>Auth::user()->getTeamDomain()->team_domain])->with('tab',2);
        
    }
    public function userDisable(Request $request)
    {
        if($request->has('user_id')){
            User::destroy($request->get('user_id'));
            alert()->message("Disabling account success",'Success')->autoclose(3000);
        }else{
            alert()->error("Disabling account failed",'Error')->persistent('OK');
        }
        return Redirect::route('dashboard::teamsetting',['team'=>Auth::user()->getTeamDomain()->team_domain])->with('tab',2);
    }
    public function getTimezones()
    {
    	return DB::select('SELECT DISTINCT * FROM zone ORDER BY zone_name');
    }
    public function getChannelList($token)
    {
      $channels =  $this->slack->getChannelList($token);
      if(!empty($channels))
      {
        $ch = [];
        foreach ($channels as $key => $value) {
            $ch[] = ['channel_id'=>$value['id'],'channel_name'=>'#'.$value['name']];
        }
        return $ch;
      }
      return [];
    }
}
