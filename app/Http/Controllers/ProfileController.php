<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\LogRepository;
use App\Repositories\SlackHelper;
use App\Http\Requests;
use App\User;
use App\Team;
use Session;
use Auth;
use Redirect;
use Alert;
class ProfileController extends Controller
{
	protected $slack;
	protected $urepo;
    protected $log;
    protected $task;
	public function __construct(SlackHelper $slack, UserRepository $repo,LogRepository $log,ProjectRepository $task)
	{
		$this->slack = $slack;
		$this->urepo = $repo;
        $this->log = $log;
        $this->task = $task;
	}

    public function updateProfile()
    {
    	$slackData = $this->slack->getUserInfo(Auth::user()->token,Auth::user()->user_id);
    	$self = User::where('user_id',Auth::user()->user_id)->first();
    	$hasChanged = $this->urepo->updateUserInfo($slackData,$self);
    	if ($hasChanged) {
    		alert()->success("Profile successfully updated",'Success')->autoclose(3000);
        }
        alert()->info('User information was already up to date, nothing changed ', 'Update Profile')->persistent('OK');
        return Redirect::route('dashboard::myprofile',['team'=>$self->getTeamDomain()->team_domain,'user'=>$self->user_name]);
    }

    public function myProfile()
    {
        $stat = $this->log->checkCurrentShiftLite(Auth::user()->user_id);
        $team = Team::where('team_id',Auth::user()->team_id)->first();
        $projects = $this->task->getProjectTask(Auth::user()->user_id);
        $qoute = Inspiring::quote();
        return view('pages.myprofile',compact('qoute','stat','team','projects'));
    }
    public function myTime()
    {
        $stat = $this->log->checkCurrentShiftLite(Auth::user()->user_id);
        return json_encode(['response'=>$stat]);
    }
}
