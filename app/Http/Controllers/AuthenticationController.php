<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\TeamRepository;
use App\Repositories\SlackHelper;
use Illuminate\Contracts\Auth\Guard; 
use Hash;
use Input;
use Redirect;
use URL;
use Auth;
class AuthenticationController extends Controller
{
	protected $trepo; //team repo
	protected $urepo; //user repo
	protected $user;
	protected $auth;
  protected $helper;
	public function __construct(UserRepository $urepo,TeamRepository $trepo,Guard $auth,SlackHelper $helper){
		$this->trepo = $trepo;
		$this->urepo = $urepo;
		$this->auth = $auth;
    $this->helper = $helper;
	}
  public function getTeams()
  {
    $email = Input::get('email');
    $teams = $this->trepo->getTeamByUserEmail($email);
    if(!empty($teams)){
      $success = true;
      return view('pages.login',compact('teams','success','email'));
    }else{
      alert()->error("Looks like there`s no user with that email address. Please check the email and try again.",'Sign In')->persistent('OK');
      return back()->with('email',$email);
    }
  }
  public function signIn(Request $request)
  {
    $email = $request->get('myemail');
    $team_id = $request->get('myteam');
    $user_pass = $request->get('mypass');
  	$self = $this->urepo->getUser($email,$team_id,trim($user_pass));
    if($self == 'disabled'){
      alert()->error("Your account has been disabled by the team owner.",'Sign In')->persistent('Close');
      return Redirect::route('login::login',compact('email'));
    }elseif (!$self) {
      alert()->error("Invalid password",'Sign In')->persistent('Close');
      return Redirect::back()->withInput();
    }else{
  	  $this->auth->login($self, true);
      return $this->userHasLoggedIn($self);
    }
  }

  public function userHasLoggedIn($user)
  {
  	return Redirect::route('dashboard::myprofile',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]);
  }

  public function changePass(Request $request)
  {
      $user = auth()->user();
      $self = $this->urepo->getUser($user->user_email,$user->team_id,trim($request->get('old')));
      if (!$self) {
        alert()->error("Invalid password!")->autoclose(3000);
      }else{
        $self->user_pass = Hash::make(trim($request->get('new')));
        $self->save();
        $this->notifyUserforPasswordChange($user->token,$user->user_name);
        alert()->info("Password changed.",'Success')->autoclose(3000);
      }
      return Redirect::route('dashboard::myprofile',['team'=>$user->getTeamDomain()->team_domain,'user'=>$user->user_name]);
  }
  public function notifyUserforPasswordChange($token,$username)
    {
      $msg = "Your password for ".env('APP_NAME')." account has been changed.";
      $data = array(
          'text' => $msg,
          'channel' => "@".$username,
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true
        );
      $this->helper->sendToSlack($token,$data);
    }
}
