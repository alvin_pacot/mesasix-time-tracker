<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\LogRepository;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use App\Repositories\ProgressRepository;
use App\TimeAdder;
use App\User;
use Carbon\Carbon;
use Input;
use Auth;
use Redirect;
class MyWeeklyController extends Controller
{
    protected $trepo;
    protected $urepo;
    protected $prepo;
    protected $lrepo;

    public function __construct(TeamRepository $trepo, UserRepository $urepo, LogRepository $lrepo, ProgressRepository $prepo)
    {
        $this->trepo = $trepo;
        $this->urepo = $urepo;
        $this->prepo = $prepo;
        $this->lrepo = $lrepo;
    }
    public function index($team,$user,$year,$week,$id="")
    {
        if (!$this->validateYearWeek($year,$week)) {
            alert()->error('Invalid year or week number','Error')->autoclose(4000);
            $week = date('W');
            $year = date('Y');
            return Redirect::route('dashboard::myrecords',['week'=>$week,'year'=>$year,'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]);
        }
        $selecteduser = empty($id) ? Auth::user()->user_id : $id;
        $check = User::where('user_id',$selecteduser)->where('team_id',Auth::user()->team_id)->first();
        if(!$check){
            alert()->error('Invalid user, this user does not exist in the team.','Member not found!')->autoclose(4000);
            $week = date('W');
            $year = date('Y');
            return Redirect::route('dashboard::myrecords',['week'=>$week,'year'=>$year,'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]);
        }
        $wk = getStartAndEndDate($year,$week);
        $days = dateRange($wk['start'],$wk['end']);
        $data = array();
        $weekly = 0;
        foreach ($days as $key => $day) {
            $dt = explode('-',$day);
            $data[$day]['day'] = (new \DateTime)->setDate($dt[0],$dt[1],$dt[2])->format('D, M j');
            $reports = $this->getUserReports($selecteduser,$day);
            if (!empty($reports))
            {
                $overall = 0; //number of seconds since first in to last out
                $secwork = 0;
                $counter =  0;
                foreach ($reports as $k => $val) {
                    if ($val->prevcmd == 'in' || $val->prevcmd == 'back') {
                        $secwork += $val->second;   
                    }
                    $overall += $val->second;
                    $counter +=1;
                }
                $weekly += $secwork;
                $work = [];
                $breakdown = [];
                $progressbar = [];
                $activity = [];
                $todo = [];
                $break = [];
                $stop = [];
                $eight = 28800;//seconds
                $gained = 0;
                $start = 0;
                foreach ($reports as $key => $rpt) 
                {
                    if ($rpt->nextcmd !== 'last') {
                        if ($rpt->prevcmd == 'in' || $rpt->prevcmd == 'back') {
                            $work[] = $rpt->difference;
                            if(!empty($rpt->activity) && $rpt->prevcmd == 'in'){
                                $act = explode('+++',$rpt->activity);
                                foreach ($act as $val) {
                                    $activity[] = ucfirst($val);
                                }
                            }
                            if(!empty($rpt->todos) && $rpt->prevcmd == 'in'){
                                $tod = explode('+++',$rpt->todos);
                                foreach ($tod as $vals) {
                                    $todo[] = ucfirst($vals);
                                }
                            }
                            if ($secwork > $eight) { //more than 8 hours
                                $dff = ($eight - $gained);
                                if ($gained == $eight ) {
                                    $progressbar[] =[ self::calcPercentage($rpt->second,$overall),'overtime',secToTime($rpt->second)];
                                }elseif($gained < $eight && $rpt->second > ($eight - $gained)){
                                    $x = $rpt->second - ($eight - $gained);
                                    $progressbar[] =[ self::calcPercentage(($eight - $gained),$overall),'work',secToTime(($eight - $gained))];
                                    $gained +=($eight - $gained);
                                    $progressbar[] =[ self::calcPercentage($x,$overall),'overtime',secToTime($x)];
                                }else{
                                    $progressbar[] =[ self::calcPercentage($rpt->second,$overall),'work',secToTime($rpt->second)];
                                    $gained +=$rpt->second;
                                }
                            }else{//no overtime
                                $progressbar[] =[ self::calcPercentage($rpt->second,$overall),'work',secToTime($rpt->second)];
                            }                       
                        }elseif($rpt->prevcmd == 'brb'){
                            $break[] = $rpt->difference;
                            $progressbar[] =[ self::calcPercentage($rpt->second,$overall),'break',secToTime($rpt->second)];
                        }elseif($rpt->prevcmd == 'out'){
                            $stop[] = $rpt->difference;
                            $progressbar[] =[ self::calcPercentage($rpt->second,$overall),'none',secToTime($rpt->second)];
                        }
                    }
                    if($start == $counter-1){
                        $breakdown[] = [ $rpt->prevcmd,Carbon::createFromFormat('Y-m-d H:i:s', $rpt->prevtime)->format('M d h:i A') ];
                        $breakdown[] = [ $rpt->nextcmd,Carbon::createFromFormat('Y-m-d H:i:s', $rpt->nexttime)->format('M d h:i A') ];
                    }else{
                        $breakdown[] = [ $rpt->prevcmd,Carbon::createFromFormat('Y-m-d H:i:s', $rpt->prevtime)->format('M d h:i A') ];
                        $start += 1;
                    }
                }
                // dd($reports);
                $wrk = (new TimeAdder($work))->getTotalTime();
                $data[$day]['breakdown'] = $breakdown;
                $data[$day]['progressbar'] = $progressbar;
                $data[$day]['worktime'] = toReportFormat($wrk);
                $data[$day]['breaktime'] = toReportFormat((new TimeAdder($break))->getTotalTime());
                $data[$day]['extendedtime'] = ($secwork <= 28800 ) ? '0 hour 0 minutes': toReportFormat(getTimeDiff('08:00:00',$wrk));
                $data[$day]['activity'] = $activity;
                $data[$day]['todo'] = $todo;
            }else{
                $data[$day]['breakdown'] = [];
                $data[$day]['progressbar'] = [[100,'none','00:00:00']];
                $data[$day]['worktime'] = "0 hours 0 minutes";
                $data[$day]['breaktime'] = "0 hours 0 minutes";
                $data[$day]['extendedtime'] = "0 hours 0 minutes";
                $data[$day]['activity'] = [];
                $data[$day]['todo'] = [];
            }
        }
        $total_weekly = secondsToTimeNoSec($weekly);
        if (Auth::user()->isAdminOrOwner()) {
            $members = $this->getUserList(Auth::user()->team_id);
            return view('pages.myweekly_admin',compact('data','team','user','year','week','total_weekly','wk','members','selecteduser'));    
        }else{
    	   return view('pages.myweekly',compact('data','team','user','year','week','total_weekly','wk'));
        }
    }
    public function getUserList($team_id)
    {
      $users = $this->urepo->getTeamMembers($team_id);
      if($users){
        $user = [];
        foreach ($users as $key => $value) {
            $user[] = ['user_id'=>$value->user_id,'user_name'=>$value->user_name,'avatar'=>$value->user_avatar];
        }
        return $user;
      }
      return [];
    }
    public static function getPrevUrl($year,$week)
    {
        $wk =  getWeekNumByYear($year);
        if ($week == 1 ) {
            return ($year-1)."/".getWeekNumByYear(($year-1));
        }else{
            return $year."/".($week-1);
        }
    }
    public static function getNextUrl($year,$week)
    {
        
        $wk =  getWeekNumByYear($year);
        if ($week == $wk ) {
            return ($year+1)."/01";
        }else{
            return $year."/".($week+1);
        }   
    }
    public function validateYearWeek($year,$week)
    {
        if (!($year > date('Y',time()) || $year < 0) && !($week < 1 || $week > 53))
        {
            $currentyear = date('Y');
            $currentweek = date('W');
            return ( ($year == $currentyear && $week <= $currentweek) 
                    || ($year <  $currentyear && $week <= $this->getWeekNumByYear($year))); 
        }
        return false;
    }
    public static function calcPercentage($seconds,$overall){
        if ($overall == 0 || $seconds > $overall) {
            return 0;
        }
        return ($seconds * 100 )/$overall;
    }
    public function getFirstIn($user_id,$date)
    {
        return DB::select("SELECT id FROM `time_log` WHERE user_id='".$user_id."' AND command = 'in' AND created_at BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59' ORDER BY id ASC LIMIT 1");
    }
    public function getLastIn($user_id,$date)
    {
        return DB::select("SELECT id FROM `time_log` WHERE user_id='".$user_id."' AND command = 'in' AND created_at BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59' ORDER BY id DESC LIMIT 1");
    }
    public function getFirstOutOfLastIn($user_id,$last_in)
    {
        return DB::select("SELECT id FROM `time_log` WHERE user_id='".$user_id."' AND command = 'out' AND id > ".$last_in." ORDER BY id ASC LIMIT 1");
    }
    public function getLastOutOfFirstIn($user_id,$last_in)
    {
        return DB::select("SELECT id FROM `time_log` WHERE user_id='".$user_id."' AND command = 'out' AND id < ".$last_in." ORDER BY id DESC LIMIT 1");
    }
    public function getUserReports($user_id,$date)
    {
        $in  = $this->getFirstIn($user_id,$date);
        if (!$in) { return [];} //no sign in at that date
        $last_in = $this->getLastIn($user_id,$date);
        if (!$last_in) { return [];} //error fetching
        $out = $this->getFirstOutOfLastIn($user_id,$last_in[0]->id);
        if (!$out && $in < $last_in) {
            $out = $this->getLastOutOfFirstIn($user_id,$last_in[0]->id);
        }
        if (!$out) return []; // still no out 
        return DB::select("SELECT c.id,c.user_id,c.location,c.created_at as prevtime,c.command as prevcmd, (SELECT c2.created_at from time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ) as nexttime, IFNULL((SELECT c2.command FROM time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ),'last') as nextcmd, IFNULL((SELECT sec_to_time(timestampdiff(second, c.created_at, c2.created_at)) FROM time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ),(SELECT sec_to_time(timestampdiff(second, c.created_at, c.created_at)))) as difference,IFNULL((SELECT timestampdiff(second, c.created_at, c2.created_at) FROM time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ),(SELECT timestampdiff(second, c.created_at, c.created_at))) as second,(SELECT GROUP_CONCAT(p.description SEPARATOR '+++') FROM progress p WHERE p.id = c.id LIMIT 1) as activity,(SELECT GROUP_CONCAT(x.description SEPARATOR '+++') FROM todo x WHERE x.id = c.id LIMIT 1) as todos FROM time_log c WHERE c.user_id='".$user_id."' and c.id BETWEEN ".$in[0]->id." AND ".($out[0]->id-1)."  ORDER BY c.user_id,c.id,c.created_at");
    }
}
