<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Scheduler;
use App\Http\Requests;
use Carbon\Carbon;
use App\Event;
use App\User;
use App\Team;
use Response;
use DB;

class TeamSchedController extends Controller
{

    public function index(){
        $selecteduser = (!empty(Input::get('selecteduser'))) ? User::where('user_id',$selecteduser)->first() : Auth::user();
        $user = User::where('team_id',Auth::user()->team_id)->get();
        $team = Team::where('team_id',Auth::user()->team_id)->first();
        $timezone = $team->team_tz;
        return view('pages.team_schedule',compact('timezone','user','selecteduser'));
    }
    public function view($team,$user,$name){
        $mem = User::where('user_id',$name)->first();
        $selecteduser = (!empty($name) && $mem) ? $mem : Auth::user();
        $user = User::where('team_id',Auth::user()->team_id)->get();
        $team = Team::where('team_id',Auth::user()->team_id)->first();
        $timezone = $team->team_tz;
        return view('pages.team_schedule',compact('timezone','user','selecteduser'));
    }
    public function getTimezones()
    {
        return DB::select('SELECT DISTINCT * FROM zone ORDER BY zone_name');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $req)
    {
        $type = $req->get('type');
        $response = [];
        if($type == 'new'){
            $event = new Event();
            $event->title = $req->get('title');
            $event->startdate = $req->get('startdate');
            $event->enddate = $req->get('startdate');
            $event->user_id = Auth::user()->user_id;
            $event->type = $this->getType($req->get('title'));
            $event->save();
            $response = ['status'=>'success','eventid'=>$event->id,'backgroundColor'=>$this->getColor($event->type)];
        }elseif($type == 'newClick'){
            $event = new Event();
            $event->title = $req->get('title');
            $event->startdate = $req->get('startdate');
            $event->enddate = $req->get('startdate');
            $event->user_id = Auth::user()->user_id;
            $event->type = $req->get('eventType');
            $event->save();
            $response = ['status'=>'success','eventid'=>$event->id,'backgroundColor'=>$this->getColor($event->type)];
        }elseif($type == 'changetitle'){
            $event = Event::where('id',$req->get('eventid'))->first();
            $event->title = $req->get('title');
            $event->save();
            if ($event) {
                $response = ['status'=>'success'];   
            }else{
                $response = ['status'=>'failed'];
            }
        }elseif($type == 'remove'){
            Event::destroy($req->eventid);
            $response = array('status'=>'success');

        }elseif($type == 'resetdate'){
            $event = Event::where('id',$req->eventid)->first();
            $event->title = $req->title;
            $event->startdate = $req->start;
            if (strpos($req->end,'+') !== false){ 
                $event->enddate = $req->end;
            }else{
                $st = explode(" ", $req->end);
                $event->enddate = trim($st[0])."+".trim($st[1]);
            }
            $event->save();

            if($event)
                $response = array('status'=>'success');
            else
                $response = array('status'=>'failed');

        }elseif($type == 'resize'){
            $event = Event::where('id',$req->eventid)->first();
            $event->title = $req->title;
            $event->startdate = $req->start;
            if (strpos($req->end,'+') !== false){ 
                $event->enddate = $req->end;
            }else{
                $st = explode(" ", $req->end);
                $event->enddate = trim($st[0])."+".trim($st[1]);
            }
            $event->save();

            if($event)
                $response = array('status'=>'success');
            else
                $response = array('status'=>'failed');

        }elseif($type == 'changetitle'){
            $event = Event::where('id',$req->eventid)->first();
            $event->title = $req->title;
            $event->save();

            if($event)
                $response = array('status'=>'success');
            else
                $response = array('status'=>'failed');
        }elseif($type == 'fetch'){
            if($req->user_id == Auth::user()->user_id){
                $query = Event::where('user_id',$req->user_id)->get();
            }else{
                $query = Event::where('user_id',$req->user_id)->where('type','<>','private1')->where('type','<>','private0')->get();
            }
            if(!empty($query)){
                foreach($query as $row) {
                    $e = array();
                    $e['id'] = $row->id;
                    $e['title'] = $row->title;
                    $e['start'] = $row->startdate;
                    $e['end'] = $row->enddate;
                    $e['allDay'] = ($row->allDay == "true") ? true : false;
                    $e['backgroundColor'] = $this->getColor($row->type);
                    array_push($response, $e);
                }
            }
        }
            
        return json_encode($response);    
    }
    public function getType($title)
    {
        if ($title == 'Public Event') { return 'public0';}
        // elseif ($title == 'Public (Notify)') { return 'public1'; }
        // elseif ($title == 'Private (Notify)') { return 'private1'; }
        elseif ($title == 'Private Event') { return 'private0';}
        // elseif ($title == 'Special (Notify)') { return 'special1'; }
        elseif ($title == 'Special Event') { return 'special0'; }
        else return 'default';
    }
    public function getColor($type)
    {
        $color = [
            'public0'  =>'#388E3C',
            // 'public1'  =>'#66BB6A',
            'private0' =>'#1976D2',
            // 'private1' =>'#42A5F5',
            'special0' =>'#D32F2F',
            // 'special1' =>'#EF5350',
            'default'  =>'#6A2C91'
        ];
        return $color[$type];
    }
}
