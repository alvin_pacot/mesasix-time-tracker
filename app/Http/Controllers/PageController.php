<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class PageController extends Controller
{
    public function slackchat()
    {
        return view('slackchat');
    }
    public function registrationStart()
    {
        return view('pages.team_setup_start');
    }
    public function index(Request $req)
    {
        if($req->has('success')) { 
            alert()->message("You successfully registered your team to Mesasix Time Tracker. Visit the support page for more details of how Mesasix Time Tracker works","Success!")->persistent('OK'); 
            // $req->session()->flush();
        }
        return view('pages.home');
    }
    public function login()
    {
        $email = Session::has('email') ? Session::get('email') : ''; 
        return view('pages.login',compact('email'));
    }
    public function support()
    {
        return view('pages.support');
    }
    public function explore()
    {
        return view('pages.explore');
    }
    public function mySchedule()
    {
        return view('pages.myschedule');
    }
    public function teamSchedule()
    {
        return view('pages.team_schedule');
    }
    
}
