<?php
namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Repositories\ResponseRepository;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use App\Repositories\LogRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SlackHelper;
use Illuminate\Support\Facades\DB;
use Input;
use App\Team;
use App\User;
use App\UserActivity;
use App\UserTodo;

class OutGoingWebhookController extends Controller
{
	protected $response;
	protected $slack;
	protected $teamrepo;
	protected $userrepo;
	protected $command_list = ['in','out','brb','back','++','--'];
    protected $log;

	public function __construct(LogRepository $log,ResponseRepository $response,SlackHelper $slack,TeamRepository $teamrepo,UserRepository $userrepo)
    {
    	$this->response = $response;
    	$this->slack = $slack;
    	$this->userrepo = $userrepo;
    	$this->teamrepo = $teamrepo;
        $this->log = $log;
    }
    public function handleOutgoingWebhook($data)
    {   
    	if (self::isValidTrigger($this->getTrigger($data['text']),$this->command_list)) {
            //first timer
            if (!$this->userrepo->isRegistered($data['user_id'])) {
                $team = $this->teamrepo->getTeamById($data['team_id']);
                $user = $this->slack->getUserInfo($team->access_token,$data['user_id']);
                if (!$user || !$this->userrepo->isValidMember($user)) {
                  //possible error: user not found in slack,invalid token,account inactive,account restriction (guest,clients), bot, deleted account etc
                    return $this->response('Error ['.$user.'], This could be your first sign in and we have trouble fetching your information from Slack. Please report this problem to our page.');
                }
                else
                {
                    $pass = Str::random(10);
                    $this->userrepo->createUser($user,$team->team_id,$team->access_token,$pass);
                    $this->notifyUserforPassword($data['user_name'],$user['profile']['email'],$pass,$team->access_token);
                    return $this->response($this->validateCmd($data));     
                }    
            }
            else // old user
                return $this->response($this->validateCmd($data)); 
        }
        else
            return $this->response($this->response->wrong_command($data['user_name']));
    }
    public function validateCmd($data)
    {   //get last time log
        $last_log = $this->log->getLastLogByUser($data['user_id']);
        $prv = (!empty($last_log)) ? $last_log[0]->command : '';
        $nxt  = strtolower($data['trigger_word']);
        $user  = User::where('user_id',$data['user_id'])->first();
        $to = $data['user_name'];
        if ($prv == 'out' && $nxt == 'in' || empty($prv) && $nxt == 'in') {
            $user->notified = 0; //set unnotified not necessary but may help during some circumtances
            $user->save();
            $this->log->storeLog(['location'=>self::atHome($data['text']),'command'=>$nxt,'user_id'=>$data['user_id']]);
            return $this->response->in($to);
        }elseif ($prv == 'in' && $nxt == 'brb' || $prv == 'back' && $nxt == 'brb') {
            if ($user->notified == 1) {
                return $this->response->notify_brb($to); 
            }else{
                $this->log->storeLog(['location'=>self::atHome($data['text']),'command'=>$nxt,'user_id'=>$data['user_id']]);
                return $this->response->brb($to); 
            }
        }elseif ($prv == 'back' && $nxt == 'out' || $prv == 'in' && $nxt == 'out') {
            $user->notified = 0;
            $user->save();
            $this->log->storeLog(['location'=>self::atHome($data['text']),'command'=>$nxt,'user_id'=>$data['user_id']]);
            return $this->response->out($to)."  ||  Your time is  ".$this->log->getCurrentShift($data['user_id']);
        }elseif ($prv == 'brb' && $nxt == 'back') { //<---to above are valid
            $this->log->storeLog(['location'=>self::atHome($data['text']),'command'=>$nxt,'user_id'=>$data['user_id']]);
            return $this->response->back($to);
        }elseif ($prv == 'in' && $nxt == 'back') {
            return $this->response->in_back($to);
        }elseif ($prv == 'brb' && $nxt == 'in') {
            return $this->response->brb_in($to);
        }elseif ($prv == 'brb' && $nxt == 'out') {
            return $this->response->brb_out($to);
        }elseif ($prv == 'back' && $nxt == 'in') {
            return $this->response->back_in($to);
        }elseif ($prv == 'out' && $nxt == 'brb') {
            return $this->response->out_brb($to);
        }elseif ($prv == 'out' && $nxt == 'out') {
            return $this->response->out_out($to);
        }elseif ($prv == 'in' && $nxt == 'in') {
            return $this->response->in_in($to);
        }elseif ($prv == 'brb' && $nxt == 'brb') {
            return $this->response->brb_brb($to);
        }elseif ($prv == 'back' && $nxt == 'back') {
            return $this->response->back_back($to);
        }elseif ($prv == 'out' && $nxt == 'back') {
            return $this->response->out_back($to);
        }elseif ($nxt == '++') {
            if($this->storeActivity($data))
                return "@".$data['user_name']." activity was saved.";
            return "@".$data['user_name']." activity was NOT saved. You dont have a progress yet";
        }elseif ($nxt == '--') {
            if($this->storeTodo($data))
                return "@".$data['user_name']." todo list was saved.";
            return "@".$data['user_name']." todo was NOT saved. You dont have a progress yet";
        }else{ //unknown command not possible but a good way to provide feedback
            return $this->response->wrong_command($to);
        }
    }
    public function notifyUserforPassword($username,$email,$password,$token)
    {
      $msg = "Your team has been registered to Mesasix Tracker. Your Mesasix Tracker account has been created. Username: ".$email." Password: ".$password." Change your password after you login.";
      $data = array(
          'text' => $msg,
          'channel' => "@".$username,
          'username'=>env('APP_NAME'),
          'unfurl_links'=>true,
        );
      $this->slack->sendToSlack($token,$data);
    }
    public static function atHome($string)
    {
        return (stripos($string, 'home') === false) ? false : true ;
    }
    public function storeActivity($data)
    {   
        $acts = explode('++',$data['text']);
        $in = $this->log->getLastInId($data['user_id']);
        if (empty($in[0])) {
            return false;
        }
        foreach ($acts as $key => $act) {
            if (!empty($act)) {
            $newAct = new UserActivity();
            $newAct->description = trim($act,'+<>');
            $newAct->id = $in->id;
            $newAct->user_id = trim($data['user_id']);
            $newAct->save();
            }
        }
        return true;
    }
    public function storeTodo($data)
    {   
        $acts = explode('--',$data['text']);
        $in = $this->log->getLastInId($data['user_id']);
        if (empty($in[0])) {
            return false;
        }
        foreach ($acts as $key => $act) {
            if (!empty($act)) {
            $newAct = new UserTodo();
            $newAct->description = trim($act,'-<>');
            $newAct->id = $in->id;
            $newAct->user_id = trim($data['user_id']);
            $newAct->save();
            }
        }
        return true;
    }
    public function getTrigger($string)
    {
        return explode(' ',$string)[0];
    }
    public static function isValidTrigger($cmd,$command_list)
    {
        return (in_array(strtolower($cmd), $command_list) || strpos($cmd,'++') !==false || strpos($cmd,'--') !==false );
    }
    public function response($text)
    {
        return json_encode(['text'=>$text,'username'=>env('APP_NAME'),'unfurl_links'=>true,'unfurl_media'=>true,'icon_url'=>env('APP_LOGO')]);
    }
}//end class