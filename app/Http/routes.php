<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/
//homepage
Route::get('/', ['as'=>'home','uses'=>'PageController@index']);
Route::get('/support', ['as'=>'support','uses'=>'PageController@support']);
Route::get('/explore', ['as' => 'explore', 'uses' =>'PageController@explore']);

//handle slack request
Route::post('webhook/handler',['as'=>'handler','middleware'=>'slack','uses'=>'APIController@index']);

//dashboard
Route::group(['prefix'=>'dashboard','as'=>'dashboard::','middleware' => 'auth'], function () {
	//dashboard for admins or not
	Route::group(['prefix'=>'/{team}'], function () {
		//profile
		Route::get('/{user}/myprofile',['as'=>'myprofile','uses'=>'ProfileController@myProfile']);
		Route::get('/{user}/myprofile/update',['as'=>'updateuserinfo','uses'=>'ProfileController@updateProfile']);
		Route::post('/{user}/myprofile/mytime',['as'=>'mytime','uses'=>'ProfileController@myTime']);
		//self record
		Route::get('/{user}/myweekly/{year}/{week}',['as'=>'myrecords','uses'=>'MyWeeklyController@index']);
		Route::get('/{user}/mymonthly/{year}/{month}',['as'=>'mymonthly','uses'=>'MyMonthlyController@index']);
		//sched
		Route::get('/{user}/mysched',['as'=>'mysched','uses'=>'TeamSchedController@index']);
		Route::get('/{user}/teamsched/{name}',['as'=>'teamsched','uses'=>'TeamSchedController@view']);
		Route::post('/{user}/schedule',['as'=>'scheds','uses'=>'TeamSchedController@handle']);
	});
		Route::get('/{team}/teamactivity/{date}',['as'=>'teamactivity','uses'=>'TeamActivityController@index']);
		
		//feature for admins and owners only
			//team records
		Route::get('/{team}/{user}/weekly/{year}/{week}/{id}',['as'=>'theyrecords','middleware'=>'admin','uses'=>'MyWeeklyController@index']);
		Route::get('/{team}/{user}/monthly/{year}/{month}/{id}',['as'=>'theymonthly','uses'=>'MyMonthlyController@index']);
			//team project
		Route::get('/{team}/teamproject',['as'=>'teamproject','middleware'=>'admin','uses'=>'ProjectController@index']);
		Route::post('/{team}/teamproject/create',['as'=>'createproject','middleware'=>'admin','uses'=>'ProjectController@createProject']);
		Route::post('/{team}/teamproject/remove',['as'=>'removeproject','middleware'=>'admin','uses'=>'ProjectController@removeProject']);
		Route::post('/{team}/teamproject/update',['as'=>'updateproject','middleware'=>'admin','uses'=>'ProjectController@updateProject']);
		Route::post('/{team}/teamproject/addtask',['as'=>'addprojecttask','middleware'=>'admin','uses'=>'ProjectController@addTask']);
		Route::post('/{team}/teamproject/remtask',['as'=>'removeprojecttask','middleware'=>'admin','uses'=>'ProjectController@removeTask']);
			//team setting		
		Route::get('/{team}/teamsetting',['as'=>'teamsetting','middleware'=>'admin','uses'=>'TeamSettingController@index']);
		Route::get('/{team}/teamsetting/update',['as'=>'teamupdateinfo','middleware'=>'admin','uses'=>'TeamSettingController@updateTeam']);
		Route::post('/{team}/teamsetting/update',['as'=>'teamupdatesetting','middleware'=>'admin','uses'=>'TeamSettingController@updateSetting']);
		//for team owners only
			//team setting
		Route::post('/{team}/teamsetting/enable',['as'=>'teamupdateuserenable','middleware'=>'owner','uses'=>'TeamSettingController@userEnable']);
		Route::post('/{team}/teamsetting/disable',['as'=>'teamupdateuserdisable','middleware'=>'owner','uses'=>'TeamSettingController@userDisable']);
});

// route for team registraion
Route::get('promptRegistration/{teamId}', ['as'=>'teamRegWithId','uses'=>'TeamRegistrationController@promptRegistrationWithId']);
Route::get('team/registration', ['as'=>'teamRegStart','uses'=>'PageController@registrationStart']);
Route::post('team/registration', ['as'=>'teamRegistration','uses'=>'TeamRegistrationController@registrationRedirectToSlackButton']);
Route::get('team/registration/callback',['as'=>'teamRegCallback','uses'=>'TeamRegistrationController@handleSlackCallback']);

//signin using slack account routes
Route::get('login/slack',['as'=>'slack_login','uses'=>'SlackAuthController@signinWithSlack']);
Route::get('login/token',['as'=>'slack_login_token','uses'=>'SlackAuthController@handleSlackCallback']);
Route::get('login/slack/callback',['as'=>'slack_callback','uses'=>'SlackAuthController@handleSlackCallback']);

//signin with app accounts
Route::group(['prefix'=>'login','as'=>'login::','middleware'=>'guest'],function(){
	Route::get('/',['as'=>'login','uses'=>'PageController@login']);
	Route::get('/email',['as'=>'email','uses'=>'AuthenticationController@getTeams']);
	Route::post('/signin',['as'=>'password','uses'=>'AuthenticationController@signIn']);
});
//change password
Route::post('/changepass',['as'=>'changepass','middleware'=>'auth','uses'=>'AuthenticationController@changePass']);

//logout
Route::get('logout',['as'=>'logout','uses'=>'SlackAuthController@userHasLogout']);

/* testing Routes */
//testing route for cron for testing purpose only, realtime use console from artisan commad
Route::get('abcdefghijklmnopqrstuvwxyz',['uses'=>'CronController@handle']);
//test chat
Route::get('/chat', ['uses'=>'PageController@slackchat']);


