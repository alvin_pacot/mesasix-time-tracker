<?php
/******url related*******/
function set_active ($url)
{
    return Request::is($url) ? 'active' : '';
}
function set_disabled ($is)
{   
    //disabled for non owner
    return ($is == 0 || $is == false) ? 'readonly ' : '';
}
/*******calendar related********/
function getCalendar($val,$year){
   	$months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
   	return $months[($val-1)]." ".$year;
}
function getWeekNumByYear($year){
    return ((new \DateTime)->setISODate($year,53)->format('W')==="53") ? 53 :52;
}
function getStartAndEndDate($year,$week) {
    $dto = new \DateTime();
    $start = $dto->setISODate($year, $week);
    $ret['week_start'] = $start->format('M, d Y');
    $ret['wk_st'] = $start->format('M d');
    $ret['start']= $start->format('Y-m-d');
    $end = $dto->modify('+6 days');
    $ret['week_end'] = $end->format('M, d Y');
    $ret['wk_en'] = $end->format('M d');
    $ret['end'] = $end->format('Y-m-d');
    $ret['timezone'] = date('e');
    return $ret;
}
function dateRange($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while( $current <= $last ) {
    $dates[] = date($output_format, $current);
    $current = strtotime($step, $current);
    }
     return $dates;
}
/******time related******/
function timeToSec($time) {
    $sec = 0;
   foreach (array_reverse(explode(':', $time)) as $k => $v) $sec += pow(60, $k) * $v;
   return $sec;
}
function secToTime($seconds) {
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}
function secondsToTime($seconds) {
    $dtF = new \DateTime("@0");
    $dtT = new \DateTime("@$seconds");
    return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
}
function secondsToTimeNoSec($seconds) {
    $dtF = new \DateTime("@0");
    $dtT = new \DateTime("@$seconds");
    return $dtF->diff($dtT)->format('%a days, %h hours and %i minutes');
}
function secondsToTimeNoSecShort($seconds) {
    $dtF = new \DateTime("@0");
    $dtT = new \DateTime("@$seconds");
    return $dtF->diff($dtT)->format('%h hrs and %i mins');
}
function secondsToDays($seconds) {
    $dtF = new \DateTime("@0");
    $dtT = new \DateTime("@$seconds");
    return $dtF->diff($dtT)->format('%a days');
}
function toReportFormat($totaltime){
    $d = explode(':', $totaltime);
   	return (integer)$d[0].' hours '.$d[1].' minutes';
}
function toReadableFormat($totaltime){
    $d = explode(':', $totaltime);
    return $d[0].' hour(s) and '.$d[1].' minute(s)';
}
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->format($differenceFormat);   
}
/*function getTimeDiff($low,$high){
    $a = new \DateTime($low);
    $b = new \DateTime($high);
    $interval = $a->diff($b);
    return $interval->format("%H:%I:%S");
}*/
function getTimeDiff($low,$high){
    $a = timeToSec($low);
    $b = timeToSec($high);
    $seconds = $b - $a;
    return gmdate("H:i:s", $seconds%86400);
}
/***** Timezone related******/
function getOffSet($tz,$base = 'UTC'){
    $dateTimeZoneBase = new \DateTimeZone($base);
    $dateTimeZoneTo = new \DateTimeZone($tz);
    $dateTimeBase = new \DateTime("now", $dateTimeZoneBase);
    $dateTimeTo = new \DateTime("now", $dateTimeZoneTo);
    return $dateTimeZoneTo->getOffset($dateTimeBase);
}
function convertToTimezone($date,$tz){
    $dateTo = new \DateTime($date);
    $dateTo->setTimezone(new \DateTimeZone($tz));
    return $dateTo->format('Y-m-d H:i:s');
}
function convertBetweenTimeZone($date,$from_tz,$to_tz){
    $src_tz =  new \DateTimeZone($from_tz);
    $dest_tz = new \DateTimeZone($to_tz);
    $dt = new DateTime($date, $from_tz);
    $dt->setTimeZone($to_tz);
    return $dt->format('Y-m-d H:i:s');
}