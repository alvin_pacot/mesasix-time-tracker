<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 11/3/2015
 * Time: 7:32 AM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class Scheduler extends Eloquent{
	use SoftDeletes;

    // Laravel auto update timestamp feature.
    public $timestamps = true;

    // Set database for this model.
    protected $table = 'schedule';

    // Set primary key.
    protected $primaryKey = 'sched_id';
    
    // Used for softdeleting.
    protected $dates = ['deleted_at'];

    // Set fillable fields.
    protected $fillable = ['user_id', 'start_time', 'end_time', 'date', 'event'];

    // Set validator.
    private $validator = null;

    // Validation rules.
    private $rules = [];

    public function store( $input ){
        $this->user_id = Auth::user()->user_id;
        $this->date = $input['date'];
        $this->start_time = $input['start_time'];
        $this->end_time = $input['end_time'];
        $this->event = $input['event'];
        $response = $this->save();
        print_r(json_encode(['error'=>$response]));
    }

    public function display($id){
       /* $data_arr = [];
        $datas = Scheduler::where('user_id', $id);
        foreach($datas as $key => $data){
            $data_arr = ['title'=>$data->event, 'start'=>$data->];
        }*/
       return [
           ['title'=>'Working Hours','start'=>'2015-11-05T10:00:00','end'=>'2015-11-05T22:00:00'],
           ['title'=>'Working Hour','start'=>'2015-11-06T10:00:00','end'=>'2015-11-06T23:00:00']
       ];
    }

    /**
     * This will strip out "required" rule from each validation rule.
     * This means a field is not supposed to be required when validating data.
     *
     * @return array
     */
    public function noRequiredValidationRules(){
        $newRules = array();

        //Let's get each validation rule.
        foreach ($this->rules as $key => $value) {

            //Let's replace some required strings here.
            $value = str_replace('required|', '', $value);
            $value = str_replace('required', '', $value);

            //Set new validation rule.
            $newRules[$key] = $value;
        }

        $this->rules = $newRules;

        return $this->rules;
    }

    /**
     * This will allow you to customize or update the $rules
     * on the fly before validating a data.
     *
     * @param $field
     * @param $rule
     */
    public function setValidationRule($field, $rule){
        $this->rules[$field] = $rule;
    }

    /**
     * This will validate $data.
     *
     * @param array $data
     * @return bool
     */
    public function valid($data = array()){
        $this->validator = Validator::make($data, $this->rules);

        if ($this->validator->fails()) {

            return false;
        }

        return true;
    }

    /**
     * Get validation errors.
     *
     * @return mixed
     */
    public function errors(){
        return $this->validator->messages();
    }

    /**
     * Get validator instance.
     *
     * @return null
     */
    public function validator(){
        return $this->validator;
    }
}

#END OF PHP