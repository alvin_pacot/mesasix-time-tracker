<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Team extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'team';
    
    protected $primaryKey = 'team_id';
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at','deleted_at','updated_at'];

    public function getOwner()
    {
        return DB::table('user')->where('team_id',$this->team_id)->where('is_owner',1)->get();
    }
    public function getAdmins()
    {
        return DB::table('user')->where('team_id',$this->team_id)->where('is_admin',1)->where('is_owner',0)->get();
    }
    public function getAllMembers()
    {
        return DB::table('user')->where('team_id',$this->team_id)->get();
    }
    public function getMembers()
    {
        return DB::table('user')->where('team_id',$this->team_id)->where('is_owner',0)->where('is_admin',0)->get();
    }
}
