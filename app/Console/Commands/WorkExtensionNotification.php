<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use App\Repositories\LogRepository;
use App\Repositories\ResponseRepository;
use App\Repositories\SlackHelper;
use Carbon\Carbon;
use App\UserLog;

class WorkExtensionNotification extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:workextend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ask user with working hour who reach 8 hours to extends their working time';
    protected $team;
    protected $slack;
    protected $member;
    protected $msg;
    protected $log;

    public function __construct(ResponseRepository $msg,TeamRepository $team, SlackHelper $slack ,UserRepository $member,LogRepository $log)
    {
        $this->team = $team;
        $this->slack = $slack;
        $this->msg = $msg;
        $this->log = $log;
        $this->member = $member;
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $teams = $this->team->getTeamWithNotifyOn();
        foreach ($teams as $key => $team) {
            $users = $this->member->getWorkingTeamMembers($team->team_id);
            foreach ($users as $key => $user) {
                if ($user->notified == 1) { //notified but does not extend
                    $this->doCutWork($user->user_id);
                    $this->member->doSetUserNotify($user->user_id,0);
                    $this->doNotify($team->channel,$team,$this->msg->doesnt_extend($user->user_name));
                }elseif ($user->notified == 2) { //extend
                    if($this->isWorkOver($user->user_id,16)) //16 hours of work during shift is non-productive
                    {
                        $this->doCutWork($user->user_id);
                        $this->member->doSetUserNotify($user->user_id,0);
                        $this->doNotify("@".$user->user_name,$team,$this->msg->already_extend($user->user_name));
                    }//else ignore this user
                }elseif ($user->notified == 0) { //neither notified nor extend
        // var_dump($this->isWorkOver($user->user_id,8));
                    if ($this->isWorkOver($user->user_id,8)) {
                        $this->member->doSetUserNotify($user->user_id,1);
                        $this->doNotify("@".$user->user_name,$team,$this->msg->ask_extend($user->user_name));
                    }//else ignore this user
                }//if it reach this the notified has invalid value
            }
        }
        echo "DONE";
    }
    public function doCutWork($user_id)
    {
       $newLog = new UserLog();
       $newLog->command = 'out';
       $newLog->user_id = $user_id;
       $newLog->save(); 
    }
    public function doNotify($channel,$team,$msg)
    {
        $access_token = $team->access_token;
        $data = array(
          'text' => $msg,
          'channel' => $channel,
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true
        );
      $this->slack->sendToSlack($access_token,$data);
    }
    public function isWorkOver($user_id,$hour)
    {
        $last_record = $this->log->getLastInId($user_id);
        if(!$last_record){
          return false;
        }else{
          $work_time = $this->log->getUserWorkingHour($user_id,$last_record[0]->id);//seconds
          return ($work_time >= ($hour*60*60)) ? true : false;
        }
    }
}
