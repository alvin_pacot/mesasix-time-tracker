<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ProjectRepository;
use App\Project;

class ProjectMonitoring extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'command:monitorproject';
    protected $signature = 'command:timecutter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitor project status and deadline.';

    protected $proj;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProjectRepository $project)
    {
        $this->proj = $project;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //if a project is already started then change stat 
        Project::where('project_start','<=', date('Y-m-d 00:00:00',time()))
                ->where('project_stat','=','NOT YET STARTED')
                ->update(['project_stat' => 'ONGOING']);
        //if a project is due but the stat is not due otr complete then set to due, setting to complete is task of admin
        Project::where('project_start','<', date('Y-m-d 00:00:00',time()))
                ->where('project_due','<', date('Y-m-d 00:00:00',time()))
                ->where('project_stat','<>','COMPLETED')
                ->where('project_stat','<>','DUE')
                ->update(['project_stat' => 'DUE']);
        echo "DONE";
    }
}
