<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
       /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'project';
/*  protected $assign = null;
  protected $bar = null;*/
  protected $primaryKey = 'project_id';

  protected $guarded = ['created_at','deleted_at','updated_at'];
}
