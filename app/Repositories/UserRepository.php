<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\User;
use Hash;
use App\Repositories\SlackHelper;
use Illuminate\Support\Str;

class UserRepository {
    protected $helper;
    public function __contruct(SlackHelper $slack){
        $this->helper = $slack;
    }
    public function getUser($user_email,$team_id,$password)
    {
        $user = User::where('user_email','=',$user_email)->where('team_id','=',$team_id)->first();
        if ($user) {
            if(Hash::check($password,$user->user_pass))
                return $user;
            return null;
        }
        return 'disabled';

    }
    public function isRegistered($user_id){
        $user = User::where('user_id', '=', $user_id)->first();
        if (!$user) { return false; } 
        else { return true; }
    }
    public function findByUserIdOrCreate($userData,$token) {
        // if($this->isValidMember($v)){ return null; }
        $user = User::where('user_id', '=', $userData['self']['id'])->first();
        if(!$user) {
            $index = $this->getIndex($userData);
            $pass = Str::random(10);
            $self = $this->createUser($userData['users'][$index],$userData['team']['id'],$token,$pass);
            $this->notifyUserforPassword($self->user_name,$self->user_email,$pass,$token);
            return $self;
        }else
        {
            $this->checkIfUserNeedUpdating($userData, $user,$token);
            return $user;
        }
    }
    public function notifyUserforPassword($username,$email,$password,$token)
    {
      $msg = "Your team has been registered to ".env('APP_NAME').". Your ".env('APP_NAME')." account has been created. Username: ".$email." Password: ".$password." visit ".env('APP_LINK')."login to login. You can change your password after you login.";
      $data = array(
          'text' => $msg,
          'channel' => "@".$username,
          'username'=>env('APP_NAME'),
          'icon_url'=>env('APP_LOGO'),
          'unfurl_links'=>true,
          'unfurl_media'=>true
        );
      $this->helper->sendToSlack($token,$data);
    }
    public function getIndex($data)
    {
        foreach ($data['users'] as $key => $value) {
            if ($data['self']['id'] == $value['id']) {
                return $key;
            }
        }
        return null;
    }
    //client will not be added to group
    public function isValidMember($val)
    {
      return (!$val['deleted'] && !$val['is_bot'] && !$val['is_restricted'] && !$val['is_ultra_restricted'] && $val['id'] !== "USLACKBOT");
    }

    public function createUser($userData,$team_id,$token = "",$temppass){
        return User::create([
                'user_id' => $userData['id'],
                'user_name' => $userData['name'],
                'user_email' => $userData['profile']['email'],
                'user_avatar' => $userData['profile']['image_192'],
                'real_name'=>(empty($userData['profile']['real_name'])) ? $userData['name'] : $userData['profile']['real_name'],
                'title'=> (empty($userData['profile']['title']))? env('DEF_TITLE'):$userData['profile']['title'],
                'is_admin' =>$this->toBool($userData['is_admin']),
                'is_owner' =>$this->toBool($userData['is_owner']),
                'user_pass' =>Hash::make($temppass),
                'tz' => empty($userData['tz']) ? env('DEF_TZ') : $userData['tz'],
                'team_id' => $team_id,
                'token' => $token,
            ]);
    }
    public function checkIfUserNeedUpdating($userData, $user,$token) {
        $index = $this->getIndex($userData);
        $slackData = [
            'avatar' => $userData['users'][$index]['profile']['image_192'],
            'email' => $userData['users'][$index]['profile']['email'],
            'username' => $userData['users'][$index]['name'],
            'title'=>(empty($userData['users'][$index]['profile']['title']))?'':$userData['users'][$index]['profile']['title'],
            'is_admin' =>$this->toBool($userData['users'][$index]['is_admin']),
            'is_owner' =>$this->toBool($userData['users'][$index]['is_owner']),
            'real_name'=>(empty($userData['users'][$index]['profile']['real_name'])) ? $userData['users'][$index]['name'] : $userData['users'][$index]['profile']['real_name'],
            'tz' => empty($userData['users'][$index]['tz']) ? env('DEF_TZ') : $userData['users'][$index]['tz'],
            'token' => $token,
        ];
        $dbData = [
            'avatar' => $user->user_avatar,
            'email' => $user->user_email,
            'username' => $user->user_name,
            'title'=>$user->title,
            'is_admin' =>$user->is_admin,
            'is_owner' =>$user->is_owner,
            'real_name'=>$user->real_name,
            'tz'=>$user->tz,
            'token' => $token,
        ];

        if (!empty(array_diff($slackData, $dbData))) {
            $tuser = User::where('user_id', '=', $userData['users'][$index]['id'])->first();
            $tuser->user_avatar = $userData['users'][$index]['profile']['image_192'];
            $tuser->user_email = $userData['users'][$index]['profile']['email'];
            $tuser->user_name = $userData['users'][$index]['name'];
            $tuser->title = (empty($userData['users'][$index]['profile']['title']))?'':$userData['users'][$index]['profile']['title'];
            $tuser->is_admin = $this->toBool($userData['users'][$index]['is_admin']);
            $tuser->is_owner = $this->toBool($userData['users'][$index]['is_owner']);
            $tuser->real_name = (empty($userData['users'][$index]['profile']['real_name'])) ? $userData['users'][$index]['name'] : $userData['users'][$index]['profile']['real_name'];
            $tuser->tz = empty($userData['users'][$index]['tz']) ? env('DEF_TZ') : $userData['users'][$index]['tz'];
            $tuser->token = $token;
            $tuser->save();
        }
    }
    public function updateUserInfo($new, $user) {
        $slackData = [
            'avatar' => $new['profile']['image_192'],
            'email' => $new['profile']['email'],
            'username' => $new['name'],
            'title'=>(empty($new['profile']['title']))?'':$new['profile']['title'],
            'is_admin' =>$this->toBool($new['is_admin']),
            'is_owner' =>$this->toBool($new['is_owner']),
            'real_name'=>(empty($new['profile']['real_name'])) ? $new['name'] : $new['profile']['real_name'],
            'tz' => empty($new['tz']) ? env('DEF_TZ') : $new['tz'],
        ];
        $dbData = [
            'avatar' => $user->user_avatar,
            'email' => $user->user_email,
            'username' => $user->user_name,
            'title'=>$user->title,
            'is_admin' =>$user->is_admin,
            'is_owner' =>$user->is_owner,
            'real_name'=>$user->real_name,
            'tz'=>$user->tz,
        ];
        if (!empty(array_diff($slackData, $dbData))) {
            $tuser = User::where('user_id', $new['id'])->first();
            $tuser->user_avatar = $new['profile']['image_192'];
            $tuser->user_email = $new['profile']['email'];
            $tuser->user_name = $new['name'];
            $tuser->title = (empty($new['profile']['title']))?'':$new['profile']['title'];
            $tuser->is_admin = $this->toBool($new['is_admin']);
            $tuser->is_owner = $this->toBool($new['is_owner']);
            $tuser->real_name = (empty($new['profile']['real_name'])) ? $new['name'] : $new['profile']['real_name'];
            $tuser->tz = empty($new['tz']) ? env('DEF_TZ') : $new['tz'];
            $tuser->save();
            return true;
        }
        return false;
    }
    private function toBool($bool){
        return ($bool == 1) ? true : false;
    }
    public function isUserNotified($user_id)
    {
        $user = DB::select("SELECT * FROM user WHERE notified = 1 AND user_id ='".$user_id."' LIMIT 1");
        if (!$user) {
            return false;
        }
        return true;
    }
    public function getTeamMembers($team_id)
    {
        return User::where('team_id',$team_id)->orderBy('real_name','ASC')->get();
    }
    public function getTeamMembersWithTrashed($team_id)
    {
        return User::withTrashed()->where('team_id',$team_id)->orderBy('real_name','ASC')->get();
    }
    public function getTeamMemberByName($team_id,$user_name)
    {
        return DB::select("SELECT * FROM user WHERE  `team_id` ='".$team_id."' AND `user_name`='".$user_name."' LIMIT 1");
    }
    public function getWorkingTeamMembers($team_id)
    {
        return DB::select("SELECT `u`.* FROM user u WHERE (SELECT `command` FROM time_log t where `t`.`user_id` = `u`.`user_id` ORDER BY `t`.`created_at` DESC LIMIT 1  ) <> 'out' AND `u`.`team_id` ='".$team_id."'");
    }
    public function doSetUserNotify($user_id,$val)
    {
        DB::table('user')->where('user_id', $user_id)->update(['notified' => $val]);
    }
}