<?php
namespace App\Repositories;

use Illuminate\Support\Collection;

class ResponseRepository
{
    
	public function notRegistered()
	{
		return "Your team is not registered to ".env('APP_NAME').".If you are an admin or owner of the team, click <".env('APP_LINK')."/login/slack?action=registration|here> to sign up.";
	}
	public function in($to)
	{
		return Collection::make([
            "<@channel> Today I randomly selected <@".$to."> to sing a song for us. :troll:",
            "<@".$to."> to kick start your shift can you dance new thang for us? :joy:" ,
            "<@channel> Let's all welcome the manager of stardust! <@".$to."> :joy:",
            "Welcome Monsieur to the party! Rock and roll to the world!! <@".$to.">",
            "<@channel> A round of applause to <@".$to."> for not being absent today! :joy:",
            "Cheer up and enjoy working your project will be done soon. Oh yeah <@".$to.">",
            "The waves splash against my face, carrying a message: Welcome, <@".$to."> thanks for your effort. :smile:"
        ])->random();
	}
	public function brb($to)
	{
		return Collection::make([
            'Please bring pizza after :smile:',
            'I cannot rest, I must draw, however poor the result, and when I have a bad time come over me it is a stronger desire than ever. - Beatrix Potter',
            "Okay I understand just be safe and don`t forget to come back. :troll:",
            "Rest and be thankful -William Wordsworth. :smile:",
            "Of course you are working hard and you need a break. Enjoy your break. @".$to
        ])->random();
	}
	public function back($to)
	{
		return Collection::make([
            "Welcome back <@".$to.">. (click) Timer continue",
            "Fabulous. How's it going with you?",
            'Did you bring some pizza?',
            "To our old hands, welcome back.",
            "I have been waiting, waiting and waiting forever for you. :troll:"
        ])->random();
	}
	public function out($to)
	{
		return Collection::make([
            "Please take care of yourself buddy.",
            "Wishing you well.",
            "Farewell to you my friend.",
            "Go safely and leave something of the happiness you bring.",
            "Because no one needs to live forever.",
            "Leaving is not always easy but it has to be done sometimes.",
            "Farewell!  God knows when we shall meet again."
        ])->random();
	}
	public function brb_out($to)
    {
        return Collection::make([
            "Hello <@".$to.">! Please say `back` after taking a break. Then followed by `out` if you want to logout",
            "Sorry <@".$to."> but you're suppose to say `back` if you're done with your break. Then followed by `out` if you want to logout",
            "Hi <@".$to.">, I know you are eager to rest but I need you to say `back` after you say `brb`. Then followed by `out` if you want to logout"
        ])->random();
    }
    public function brb_in($to)
	{
		return Collection::make([
            "Hello <@".$to.">! Please say `back` after taking a break.",
            "Sorry <@".$to."> but you're suppose to say `back` if you're done with your break.",
            "Hi <@".$to.">, I know you are eager to work but I need you to say `back` after you say `brb`."
        ])->random();
	}
	public function in_in($to)
	{
		return Collection::make([
            "Invalid command. Please check your status always, <@".$to.">",
            "You are currently logged in <@".$to.">, no need to type `in`",
            "Hi <@".$to.">, You`re already logged in. Try another command."
        ])->random();
	}
	public function brb_brb($to)
	{
		return Collection::make([
            "You're still on break <@".$to."> ",
            "Having two breaks is already an abuse <@".$to.">",
            "Do you hate me that much that you need two breaks at a time <@".$to.">?"
        ])->random();
	}
	public function back_back($to)
	{
		return Collection::make([
            "Chill, you are already back <@".$to."> ",
            "Your comeback has already been recorded <@".$to.">"
        ])->random();
	}
    public function out_out($to)
    {
        return Collection::make([
           "You are already on signed out <@".$to.">. Please sign in if you want to continue working",
            "You really love to leave me twice <@".$to.">? :broken_heart: ",
            "<@".$to.">, Type `in` first before you type `out`.",
            "Wrong move <@".$to."> because you are not sign in."
        ])->random();
    }
    public function in_back($to)
    {
        return Collection::make([
           "No need to be back <@".$to."> if you haven't gone anywhere else :wink: ",
           "There's no turning back <@".$to."> when you haven't left me in the first place :heart_eyes: ",
           "You need to type 'brb' first before you type `back`. Simply because `back` is for `brb` just like <@".$to."> is for me :heart_eyes_cat: "
        ])->random();
    }
    public function back_in($to)
    {
        return Collection::make([
           "Hi <@".$to."> Please check your status by this command /m6 check my status",
            "Oppps!.`Back` is for `brb`, and `in` is for `out` just like <@".$to."> is for her :heartbeat: "
        ])->random();
    }
    public function out_back($to)
    {
        return Collection::make([
           "You're still logged out <@".$to."> , so please use `in` instead of `back` ",
            "Hi <@".$to."> you have typed a wrong command. To check your status type: /m6 check my status",
            "Hello <@".$to."> You can only use `back` after you used the `brb` command.",
            "Yo sorry <@".$to."> but I follow a certain protocol and you need to say `in` because your last command is `out`",
            "Happy to hear you are back <@".$to.">, but I want to hear the command `in` because your last command is `out`",
            "Hey <@".$to.">, Please say `in` to continue."
        ])->random();
    }
    public function notify_brb($to)
    {
        return "@".$to.", Unable to process your command. Please confirm first if you want to extend your shift otherwise I will sign you out at any moment now";
    }
    public function error($to)
    {
        return Collection::make([
           'Yo! Sorry but currently I am experiencing some trouble',
            'Call my doctor I need a debug.',
            'I am very sorry but I need a debug'
        ])->random();
    }
    public function log_error($to)
    {
        return Collection::make([
           'Hey <@'.$to.'>! Something unusual happened and I can`t log your time. Please report this bug.',
           "I can`t find anything in my records. Please check your status."
        ])->random();
    }
    public function wrong_command($to)
    {
        return Collection::make([
            '<@'.$to.'> this could be your first sign in. Please start typing `in`',
            'Can`t process anything using that command. Review the instructions and try again.',
            '<@'.$to.'>, How about an `in` first :smile:'
        ])->random();
    }
    public function ask_extend($to)
    {
        return "@".$to.", You have already reach 8 hours of your shift. Type `/m6 extend` to continue working, otherwise i'll sign out you in the next 15 minutes.";
    }
    public function valid_extend()
    {
        return Collection::make([
            'Good, keep it up.Please be remember that you can only have a max of 16 hours during your shifts, after that i will automatically sign you out.'
        ])->random();
    }
    public function doesnt_extend($to)
    {
        return "@".$to.", For disregarding the notification earlier, You've been signed out.";
    }
    public function already_extend($to)
    {
        return Collection::make([
            "@".$to.", You have already reach 16 hours of your shift. You've been signed out."
        ])->random();       
    }
    public function invalid_extend()
    {
        return Collection::make([
            'Sorry, but i don`t remember asking you for an extension.'
        ])->random();
    }
    public function help()
    {
        return 'Here is the list that you can use.
                *Time Tracking:*
                Note: "You have to use the right command"
                `in` | `in home`  - Start tracking the time. Default location is office
                `out` - Stop tracking
                `brb` - Pause the time
                `back` - Restart it again from pause
                *Saving your progress reports*
                 prepend with `++` followed by your reports,will save in the last in
                *Saving your to do reports*
                 prepend with `--` followed by your reports,will save in the last in
                *Checking your time:*
                `/m6 c|check my st|status` - check your last command 
                `/m6 c|check my sh|shift` - check time from the time you in 
                `/m6 c|check my w|week` - check your total time this week [Mon to Sun]
                `/m6 c|check my m|month` - check your total time this month
                `/m6 c|check my y|year` - check your total time this year
                *Checking other`s time:*
                NOTE: name = slack user display name. You can use slash command anywhere
                `/m6 c|check @name [st]status|[sh]shift|[w]week|[m]month|[y]year`';
    }
}