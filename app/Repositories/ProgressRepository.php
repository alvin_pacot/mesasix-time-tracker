<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Team;
// use App\UserLog;
// use Carbon\Carbon;
// use Config;
class ProgressRepository 
{
    protected $user;
    protected $team;

    public function  __construct(User $user,Team $team){
        $this->user = $user;
        $this->team = $team;
    }
}
