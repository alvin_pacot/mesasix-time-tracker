<?php
namespace App\Repositories;

use App\Team;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;

class TeamRepository{
    
    protected $user;

    public function	 __construct(UserRepository $user){
        $this->user = $user;
    }

	public function isRegistered($team_id){
		$team = Team::where('team_id', '=', $team_id)->first();
		if (!$team) { return false; } 
        else { return true; }
	}

	public function findByTeamIdOrCreate($tData) {
        $index = $this->user->getIndex($tData);
        $team = Team::where('team_id', '=', $tData['team']['id'])->first();
        if(!$team) {
            return Team::create([
            'team_id' => $tData['team']['id'],
            'team_name' => $tData['team']['name'],
            'team_domain' => $tData['team']['domain'],
            'team_status' => 'FREE',
            'channel'=>$tData['channel'],
            'config_url'=> $tData['config_url'],
            'hook_url' =>$tData['hook_url'],
            'team_tz' => empty($tData['users'][$index]['tz']) ? env('DEF_TZ') : $tData['users'][$index]['tz'],
            'access_token' =>$tData['access_token'],
            'notify' =>true
            ]);
        }else{
            // $this->checkIfTeamNeedUpdating($tData,$team);
            return $team;
        }
    }

    public function checkIfTeamNeedUpdating($tData, $team) {
        $socialData = [
            'team_name' => $tData['team_name'],
            'team_domain' => $tData['team_domain'],
            'team_status' => $tData['team_status'],
            'channel'=>$tData['channel'],
            'config_url'=> $tData['config_url'],
            'hook_url' =>$tData['hook_url'],
            'team_tz' => empty($tData['users'][$index]['tz']) ? env('DEF_TZ') : $tData['users'][$index]['tz'],
            'access_token' =>$tData['access_token'],
        ];
        $dbData = [
            'team_name' => $team->team_name,
            'team_domain' => $team->team_domain,
            'team_status' => $team->team_status,
            'channel'=> $team->channel,
            'config_url'=> $team->config_url,
            'hook_url' => $team->hook_url,
            'team_tz' => $team->team_tz,
            'access_token' =>$team->access_token,
        ];
        if (!empty(array_diff($socialData, $dbData))) {
            $team = Team::where('team_id', '=', $team->team_id)->first();
            $team->team_name = $tData['team_name'];
            $team->team_domain = $tData['team_domain'];
            $team->team_status = $tData['team_status'];
            $team->channel = $tData['channel'];
            $team->config_url = $tData['config_url'];
            $team->hook_url = $tData['hook_url'];
            $team->team_tz = empty($tData['users'][$index]['tz']) ? env('DEF_TZ') : $tData['users'][$index]['tz'];
            $team->access_token = $tData['access_token'];
            $team->save();
        }
    }
    public function updateTeam($new, $current) {
        $slackData = [
            'team_name' => $new['name'],
            'team_domain' => $new['domain'],
        ];
        $dbData = [
            'team_name' => $current->team_name,
            'team_domain' => $current->team_domain,
        ];
        if (!empty(array_diff($slackData, $dbData))) {
            $team = Team::where('team_id', '=', $current->team_id)->first();
            $team->team_name = $new['name'];
            $team->team_domain = $new['domain'];
            $team->save();
            return true;
        }
        return false;
    }
    public function getTeamById($team_id)
    {
        return Team::where('team_id', '=', $team_id)->first();
    }
    public function getTeamWithNotifyOn()
    {
        return DB::select('SELECT * FROM team WHERE `notify` = 1');
    }
    public function getTeamByUserEmail($email)
    {
        return DB::select("SELECT DISTINCT `t`.`team_domain`,`t`.`team_id`,`t`.`team_name` FROM team t, user u WHERE `t`.`team_id` = `u`.`team_id` AND `u`.`user_email`='".$email."' ORDER BY `t`.`team_name`");
    }
}