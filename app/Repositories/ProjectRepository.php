<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Team;
use App\Project;
use App\UserTask;
class ProjectRepository{
    
    protected $team;

    public function createProject($data = array())
    {
        return Project::create([
            'project_name' =>$data['name'],
            'project_desc' =>$data['desc'],
            'project_start' =>$data['start'],
            'project_due' =>$data['end'],
            'project_stat' =>$data['stat'],
            'channel' =>$data['channel'],
            'team_id' =>$data['team_id']
            ]);
    }
    public function createUserTask($data = array())
    {
        return UserTask::create([
            'project_id' =>$data['project_id'],
            'user_id' =>$data['user_id'],
            'task' =>$data['task']
            ]);
    }
    public function getProjectChannel($project_id)
    {
        return DB::select("SELECT channel FROM project WHERE project_id ='".$project_id."' LIMIT 1");
    }
    public function getProjectDetail($team_id)
    {
        return DB::select("SELECT *,IFNULL((TIMESTAMPDIFF(SECOND,project_start,project_due)),'NOT SET') as span FROM project WHERE team_id ='".$team_id."' ORDER BY project_name");
    }
    public function getProjectUser($project_id)
    {
        return DB::select("SELECT pu.pu_id,pu.user_id,u.user_name,pu.task FROM project_user pu, user u WHERE u.user_id=pu.user_id AND pu.project_id = ".$project_id."");
    }
    public function getProjects()
    {
        return DB::select('SELECT * FROM project')->take(50)->get();
    }
    public function getProjectAll($team_id)
    {
        return DB::select("SELECT p.*,u.user_id,u.user_name,pu.task,
                IFNULL((TIMESTAMPDIFF(SECOND,p.project_start,p.project_due)),'NOT SET') as span
                FROM project p,user u,project_user pu 
                WHERE p.project_id = pu.project_id AND u.user_id = pu.user_id AND p.team_id = '".$team_id."' ORDER BY p.project_name,u.user_name");
    }
    public function getProjectTask($user_id)
    {
        return DB::select("SELECT group_concat(p.task SEPARATOR '<br>') as task,s.* FROM project_user p,project s WHERE p.project_id = s.project_id AND p.user_id = '".$user_id."' GROUP BY p.project_id");
    }

}