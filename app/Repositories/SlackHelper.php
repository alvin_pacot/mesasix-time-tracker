<?php
namespace App\Repositories;

use Frlnc\Slack\Http\SlackResponseFactory;
use Frlnc\Slack\Http\CurlInteractor;
use App\Http\Requests\Commander;

/**
*Helper for Slack API related queries
*/
class SlackHelper{

    protected $request;
    protected $interactor;	
    protected $factory;
    protected $commander;

    public function __construct(CurlInteractor $interactor, SlackResponseFactory $factory){
        $this->interactor = $interactor;
        $this->factory = $factory;
        $this->interactor->setResponseFactory($this->factory);
        $this->commander = new Commander(null,$this->interactor);
	}
    //exchange code for access_token
    public function getToken($code){
        $response = $this->commander->execute('oauth.access', [
                    'code' => $code,
                    'client_id' => env('CLIENT_ID'),
                    'client_secret' => env('CLIENT_SECRET'),
                    'redirect_uri' => env('REDIRECT_URI')
                ]);
        $arr = $response->toArray();
        if ($this->hasResult($arr)) {
            return $arr['body']['access_token'];
        }else{
            return null;
        }
    }
    //get team and members data
    public function getAllData($token){
        $this->commander->setToken($token);
        $all = [];//self,team,users
        $response = $this->commander->execute('auth.test', []);
        $arr = $response->toArray();
        $user_id = '';
        if ($this->hasResult($arr)) {
            $user_id = $arr['body']['user_id'];
        }else{ return []; }
        $response1 = $this->commander->execute('users.info', ['user'=>$user_id]);
        $arr1 = $response1->toArray();
        if ($this->hasResult($arr1)) {
            $all['self'] = $arr1['body']['user'];
        }else{ return []; }
        $response2 = $this->commander->execute('team.info', []);
        $arr2 = $response2->toArray();
        if ($this->hasResult($arr2)) {
            $all['team'] = $arr2['body']['team'];
        }else{ return []; }
        $response3 = $this->commander->execute('users.list', []);
        $arr3 = $response3->toArray();
        if ($this->hasResult($arr3)) {
            $all['users'] = $arr3['body']['members'];
        }else{ return []; }
        return $all;
    }
    //get user info by token and user_id
    public function getUserInfo($token,$user_id){
        $this->commander->setToken($token);
        $response = $this->commander->execute('users.info', ['user'=>$user_id]);
        $arr = $response->toArray();
        if ($this->hasResult($arr)) {
            return $arr['body']['user'];
        }else{
            return $arr['body']['error'];
        }
    }
    //get user info by token and user_id
    public function getChannelList($token){
        $this->commander->setToken($token);
        $response = $this->commander->execute('channels.list', ['exclude_archived'=>1]);
        $arr = $response->toArray();
        if ($this->hasResult($arr)) {
            return $arr['body']['channels'];
        }else{
            return [];
        }
    }
    //exchange code for access token and incoming-webhook setup
    public function getTeamWebhook($code)
    {
    	$response = $this->commander->execute('oauth.access', [
                'code' => $code,
                'client_id' => env('CLIENT_ID'),
                'client_secret' => env('CLIENT_SECRET'),
                'redirect_uri' =>  env('REDIRECT_URI_BUTTON'),
                ]);
        $arr = $response->toArray();
        if ($this->hasResult($arr)) {
        	return [
        		'access_token' => $arr['body']['access_token'],
        		'team_name' => $arr['body']['team_name'],
        		'channel' => $arr['body']['incoming_webhook']['channel'],
        		'config_url' => $arr['body']['incoming_webhook']['configuration_url'],
        		'hook_url' => $arr['body']['incoming_webhook']['url']
        	]; 
        }else{
        	return null;
        }
    }
    //can send private/public chat to slack using auth token
    public function sendToSlack($token,$option)
    {
        $this->commander->setToken($token);
        return $this->commander->execute('chat.postMessage', $option);
    }
    //Check if response return ok 
    public function hasResult(array $response)
    {
        return ($response['body']['ok']) ? true : false;
    }
    //can post to default channel using incoming-webhook url
    /*public function postToSlack($data = array(),$url){
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        return curl_exec($ch);
    }*/
}