<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Repositories\TeamRepository;
use App\User;
use App\Team;
use App\UserLog;
use Carbon\Carbon;
use Config;

class LogRepository 
{
    protected $user;
    protected $team;
    protected $tz;

    public function  __construct(User $user,Team $team){
        $this->user = $user;
        $this->team = $team;
    }
    public function storeLog($data)
    {
        return UserLog::create([
            'location' =>(!$data['location']) ? 'office':'home',
            'command'=>$data['command'],
            'user_id'=>$data['user_id'],
            ]);
    }
    public function getLastRecord($user_id)
    {
        return DB::table('time_log')->where('user_id','=',$user_id)->orderBy('created_at', 'desc')->take(1)->get();
    }
    public function getLastInId($user_id)
    {
        return DB::table('time_log')->where('user_id','=',$user_id)->where('command','=','in')->orderBy('created_at', 'desc')->take(1)->get();
    }
    public function getLastInIdWeek($user_id)
    {
        return DB::select("SELECT id FROM time_log WHERE user_id='".$user_id."' AND week(`created_at`,3) = week(CURRENT_TIMESTAMP,3)  AND year(`created_at`)=year(CURRENT_TIMESTAMP) AND command = 'in' ORDER BY id ASC LIMIT 1");
    }
    public function getLastInIdMonth($user_id)
    {
        return DB::select("SELECT id FROM time_log WHERE user_id='".$user_id."' AND month(`created_at`) = month(CURRENT_TIMESTAMP)  AND year(`created_at`)=year(CURRENT_TIMESTAMP) AND command = 'in' ORDER BY id ASC LIMIT 1");
    }
    public function getLastInIdYear($user_id)
    {
        return DB::select("SELECT id FROM time_log WHERE user_id='".$user_id."' AND year(`created_at`)=year(CURRENT_TIMESTAMP) AND command = 'in' ORDER BY id ASC LIMIT 1");
    }
    public function getAllLogsByUserByDate($user_id,$date)
    {
        return DB::select("SELECT u.real_name,u.title,u.user_avatar,t.command,t.created_at,t.id as tid,p.id as pid,p.prog_id, (select GROUP_CONCAT(f.description SEPARATOR '-----') from progress f WHERE f.id = t.id ) AS progress FROM user u 
                LEFT JOIN time_log t ON t.user_id=u.user_id
                LEFT JOIN progress p on t.id=p.id 
                WHERE  t.id BETWEEN 1 AND 150
                ORDER BY u.user_id,t.id");
    }
    public function getAllLogsByUserSinceIn($user_id,$id)//and 
    {
        return DB::select("SELECT IFNULL(
                                (SELECT timestampdiff(second, `c`.`created_at`, `c2`.`created_at`) FROM time_log c2 WHERE `c2`.`user_id` = `c`.`user_id` AND `c2`.`created_at` > `c`.`created_at` 
                                    ORDER BY `c2`.`created_at` LIMIT 1 ),
                                (SELECT timestampdiff(second, `c`.`created_at`, CURRENT_TIMESTAMP))) as seconds 
                            FROM time_log c where `c`.`id` >= ".$id." AND `c`.`user_id`='".$user_id."' AND `c`.`command` in ('in','back') 
                            ORDER BY `c`.`user_id`,`c`.`created_at`");
    }
    public function getAllLogsByUserSinceDate($user_id,$date)
    {
        return DB::table('time_log')->where('user_id','=',$user_id)->havingRaw('created_at >= '.$date)->orderBy('created_at', 'asc')->get();
    }
    public function getLastLogByUser($user_id)
    {
        return DB::table('time_log')->where('user_id','=',$user_id)->orderBy('created_at', 'desc')->take(1)->get();
    }
    public static function getDiff($ts1,$ts2)
    {
        return DB::select("SELECT SEC_TO_TIME(TIMESTAMPDIFF(SECOND,'".$ts1."','".$ts2."')) as diff");
    }
    public function getUserWorkingHour($user_id,$id)
    {
        $logs = $this->getAllLogsByUserSinceIn($user_id,$id);
        $seconds = 0;
        foreach ($logs as $key => $log) {
            $seconds += $log->seconds;
        }
        return $seconds;
    }
    public function getCurrentShift($user_id) //shift
    {
        $record = $this->getLastInId($user_id)[0];
        return secondsToTimeNoSec($this->getUserWorkingHour($user_id,$record->id));
    }
    public function checkCurrentStatus($user_id) //shift
    {
        $last = $this->getLastRecord($user_id);
        return (!$last) ? 'out [no record yet]': $last[0]->command;
    }
    public function checkCurrentShift($user_id,$tz) //shift
    {
        $last = $this->getLastRecord($user_id);
        if (!$last ||$last[0]->command == 'out') {
            return " 0 hour. [Not sign in]";
        }else {
            $record = $this->getLastInId($user_id);
            $gained = $this->getUserWorkingHour($user_id,$record[0]->id);//seconds
            $format = secondsToTimeNoSec($gained);
            $since  = convertToTimezone($record[0]->created_at,$tz);
            $since  = Carbon::createFromFormat('Y-m-d H:i:s',$since)->toDayDateTimeString();
            return $format." [ since ".$since." ".$tz." Time ]";
        }
    }
    public function checkCurrentShiftLite($user_id) //shift
    {
        $last = $this->getLastRecord($user_id);
        if (!$last ||$last[0]->command == 'out') {
            if($lastin = $this->getLastInId($user_id))
                return ['cmd'=>'out','shift'=>Carbon::createFromFormat('Y-m-d H:i:s',$lastin[0]->created_at)->format('M, j Y g:ia')];
            return ['cmd'=>'out','shift'=>'No record yet'];
        }else {
            $record = $this->getLastInId($user_id);
            $gained = $this->getUserWorkingHour($user_id,$record[0]->id);//seconds
            $format = secondsToTimeNoSecShort($gained);
            return ['cmd'=>$last[0]->command,'shift'=>$format];
        }
    }
    public function checkCurrentWeek($user_id) //week
    {
        $record = $this->getLastInIdWeek($user_id);
        $dt = getStartAndEndDate(date('Y',time()),date('W',time()));
        $week_start = $dt['week_start'];
        $week_end = $dt['week_end'];
        if (!$record) {
            return " 0 hour. A new week has been started and no record found yet for this week.[ ".$week_start." - ".$week_end." ]";
        }
        $gained = $this->getUserWorkingHour($user_id,$record[0]->id);
        $format = secondsToTimeNoSec($gained);
        return $format." [ ".$week_start." - ".$week_end." ]";
    }
    public function checkCurrentMonth($user_id)
    {
        $record = $this->getLastInIdMonth($user_id);
        $m_start = date('M 1 Y');
        $m_end = date('M t Y');
        if (!$record) {
            return " 0 hour. A new month has been started and no record found yet for this month.[ ".$m_start." - ".$m_end." ]";
        }
        $gained = $this->getUserWorkingHour($user_id,$record[0]->id);
        $format = secondsToTimeNoSec($gained);
        return $format." [ ".$m_start." - ".$m_end." ]";   
    }
    public function checkCurrentYear($user_id)
    {
        $record = $this->getLastInIdYear($user_id);
        if (!$record) {
            return " 0 hour. A new year has been started and no record found yet for this year. [ Jan 1 - Dec 31 ".date('Y')." ]";
        }
        $gained = $this->getUserWorkingHour($user_id,$record[0]->id);
        $format = secondsToTimeNoSec($gained);
        return $format." [ Jan 1 - Dec 31 ".date('Y')." ]";   
    }
}
/*SELECT `c`.`id`,`c`.`user_id`,`c`.`created_at` as prevtime,`c`.`command` as prevcmd, IFNULL((SELECT `c2`.`created_at` from time_log c2 where `c2`.`user_id` = `c`.`user_id` and `c2`.`created_at` > `c`.`created_at` order by `c2`.`created_at limit 1` ),CURRENT_TIMESTAMP) as nexttime, IFNULL((SELECT `c2`.`command` from time_log c2 where `c2`.`user_id` = `c`.`user_id` and `c2`.`created_at` > `c`.`created_at` order by `c2`.`created_at` limit 1 ),'current') as nextcmd, IFNULL((SELECT sec_to_time(timestampdiff(second, `c`.`created_at`, `c2`.`created_at`)) from time_log c2 where `c2`.`user_id` = `c`.`user_id` and `c2`.`created_at` > `c`.`created_at` order by `c2`.`created_at` limit 1 ),(select sec_to_time(timestampdiff(second, `c`.`created_at`, CURRENT_TIMESTAMP)))) as hours from time_log c where `c`.`id` >= ".$id." AND `c`.`user_id`='".$user_id."' AND `c`.`command` in('in','back') order by `c`.`user_id`,`c`.`created_at`*/
//SELECT CONVERT_TZ(`date_field`,'+00:00',@@global.time_zone) FROM `table`
//select now() a, now() - interval 1 day + interval 4 hour + interval 8 minute b;

/*select (select CONCAT(k.real_name,k.title,k.user_avatar) FROM user k WHERE c.user_id = k.user_id LIMIT 1) as user_info,c.id,c.user_id,c.created_at as prevtime,c.command as prevcmd, IFNULL((select c2.created_at from time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ),CURRENT_TIMESTAMP) as nexttime, IFNULL((select c2.command from time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ),'NOW') as nextcmd, IFNULL((select sec_to_time(timestampdiff(second, c.created_at, c2.created_at)) from time_log c2 where c2.user_id = c.user_id and c2.created_at > c.created_at order by c2.created_at limit 1 ),(select sec_to_time(timestampdiff(second, c.created_at, CURRENT_TIMESTAMP)))) as difference,(SELECT GROUP_CONCAT(p.description SEPARATOR '---') from progress p WHERE p.id = c.id LIMIT 1) as activity from time_log c  order by c.user_id,c.created_at*/