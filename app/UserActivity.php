<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
   /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'progress';
  
  protected $primaryKey = 'prog_id';

  protected $guarded = ['created_at','deleted_at','updated_at'];
}
