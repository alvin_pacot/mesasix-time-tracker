<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserTask extends Model
{
   /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'project_user';
  
  protected $primaryKey = 'pu_id';

  protected $fillable = ['project_id','user_id','task'];

  public $timestamps = false;
}
