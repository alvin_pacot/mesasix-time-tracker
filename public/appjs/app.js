$( document ).ready(function() {
/*	$('.sidebar-menu li').hover(
		function(){
			expand()
		},
		function(){
			collapse()
		}
	);*/
	$('.button-collapse').sideNav({
      menuWidth: 250, // Default is 240
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true // Closes side-nav on click
	});
	$("#toggle").click(function(){
		if ($(this).data('name') != 'collapse') {
			collapse()
	    	$(this).data('name', 'collapse');
		}else {
			expand()
	    	$(this).data('name', 'expand');
		}			
	});
 });
function expand(){
	var style1 = { "background-position": "-18px -21px", "width": "244px","height": "59px"};
	    $(".logo-img").css(style1);
	    $(".nav-wrapper").animate({ paddingLeft: '240px'},{ duration: 700, queue: false });
	    $(".inner-wrapper,.content-wrapper").animate({ paddingLeft: '250px'},{ duration: 700, queue: false });
	    $(".sidebar-wrapper,.logo").animate({ width: '250px' },{ duration: 700, queue: false });
	    $('span#toggle').animate({"left":'229px'},{ duration: 700, queue: false });
	    $(".nav-text").css({ "display": 'block'});
}
function collapse(){
		var style2 ={ "background-position": "-203px -73px","width":"69px","height": "59px","transform": "translate(2%,7%)" };
	    $(".inner-wrapper,.content-wrapper").animate({ paddingLeft: "70px"}, { duration: 700, queue: false });
	    $(".sidebar-wrapper,.logo").animate({ width: '70px' }, { duration: 700, queue: false });
	    $(".nav-wrapper").animate({ paddingLeft: '60px'},{ duration: 700, queue: false });
	    $('span#toggle').animate({"left":'50px'},{ duration: 700, queue: false });
	    $(".logo-img").css(style2);
	    $(".nav-text").css({ "display": 'none'});
}
function expandHeight() {
	var doc_height = $('.inner-wrapper').height();
	var win_height = $(window).height();
	var sidebar_height = $(".sidebar-wrapper").height();
	var screen_width = $(window).width();
	if (sidebar_height != 70) {//not on mobile
		if (win_height <= doc_height) {
			$(".sidebar-wrapper").height(doc_height);
		}else if(win_height > doc_height ){
		    $(".sidebar-wrapper").height(win_height);
		}
	}else{
		$(".sidebar-wrapper").css({"height":"100% !important"});
	}
	$('.nav-img-footer').css({"position":"fixed","bottom":"10px"});
}
function toggleSchedInfo() {
	if ($('#external-events').hasClass('hide')) {
        $('#external-events').switchClass('hide','l3 m4',10);
        $('#calendar,#clock').switchClass('l12 m12','l9 m8',500);
        setTimeout(function(){$('#calendar').fullCalendar('option', 'height',$('#external-events').height())},500);
        $('.fc-slideInfo-button').text('hide')
    }else{
        $('#external-events').switchClass('l3 m4','hide',10);
        $('#calendar,#clock').switchClass('l9 m8','l12 m12',500);
        setTimeout(function(){$('#calendar').fullCalendar('option', 'height','auto')},500);
        $('.fc-slideInfo-button').text('show')
    }
}
function pad(value) {
    return value < 10 ? '0' + value : value;
}
function createOffset(date) {
    var sign = (date.getTimezoneOffset() > 0) ? "-" : "+";
    var offset = Math.abs(date.getTimezoneOffset());
    var hours = pad(Math.floor(offset / 60));
    var minutes = pad(offset % 60);
    return sign + hours + ":" + minutes;
}
	