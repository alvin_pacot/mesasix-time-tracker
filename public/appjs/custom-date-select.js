$( document ).ready(function() {
    var urlData = new Array();
    var site_url = '';
    var href_url = '';
    if(urlData.length === 0){
        var get_url = window.location.href;
        var urlArray = get_url.split('/');
        if (urlArray.length > 1) {
            $.each( urlArray , function( index, value){
                var ds_date  = value.split(":");
                if(ds_date.length == 3){
                    //http://mesasixdashboard.com/yodashboard/public/dashboard/T047ZJ4LK/teamdaily/2015:09:18
                    for( var i = 0; i < urlArray.length ; i++){
                        if( i < index-3 )
                            site_url = site_url.concat(urlArray[i]) + "/";
                        if(i < index)
                            href_url = href_url.concat(urlArray[i] + "/");
                    }
                    //ajax to get the next and last date
                    getApiData( site_url+'nextday/'+urlArray[index], function( date ){
                        $('#right-arrow').attr("href", href_url + date);
                    });
                    getApiData(site_url+'lastday/'+urlArray[index], function( date ){
                        $('#left-arrow').attr("href", href_url + date);
                    });
                }
            });
        }
    }

    //date picker
    $( "#datepicker").datepicker({
        showOn: "button",
        // buttonImage: site_url+"appimg/calendar-blue.gif",
        buttonImageOnly: true,
        // buttonText: "Select date"
    }).datepicker('widget').wrap('<div class="ll-skin-melon" style="overflow: hidden;" />');
    $( "#datepicker").datepicker("option", "dateFormat", "yy-mm-dd");

    $("#datepicker").change(function(){
        var p_date =  $("#datepicker").val().replace('-',':').replace('-',':');
        window.location.replace( href_url + p_date );
    });
    //api function
    function getApiData(url, callback){
        var data = $.ajax({
            type: 'GET',
            url: url
        }).done(callback).error(function(){
            alert('Oppps...something went wrong')
        });
        return data;
    };
});