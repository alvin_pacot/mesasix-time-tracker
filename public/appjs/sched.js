$( document ).ready(function() {
   		$('select').material_select();
    	function getOffset(tzone){ return moment.tz(new Date(),tzone).format('Z')}
        function ConvertTo(date,tzone){ 
            var fromdate = moment(date);
            var st = tzone.split("/");
            return fromdate.tz(tzone).format('MM/DD h:mma ')+((st[1] == undefined) ? 'UTC' : st[1]);
        }
        function getFreshEvents(){
            $.ajax({
                url: '{{route("dashboard::scheds",["user"=>Auth::user()->user_name,"team"=>Auth::user()->getTeamDomain()->team_domain])}}',
                type: 'POST', // Send post data
                data: 'type=fetch&_token='+$("#_token").val()+"&user_id={{$selecteduser->user_id}}",
                async: false,
                success: function(s){
                    freshevents = s;
                }
            });
            $('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
        }

        function isElemOverDiv() {
            var trashEl = jQuery('#trash');

            var ofs = trashEl.offset();

            var x1 = ofs.left;
            var x2 = ofs.left + trashEl.outerWidth(true);
            var y1 = ofs.top;
            var y2 = ofs.top + trashEl.outerHeight(true);

            if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
                currentMousePos.y >= y1 && currentMousePos.y <= y2) {
                return true;
            }
            return false;
        }
}