@extends('master.homelayout')

@section('main-nav')
	@if(!Auth::check())
	 	@include('partial.main-nav-guest')
	@else
	@include('partial.main-nav-auth')
	@endif
@stop

@section('css')
<style type="text/css">	
	.inner-wrapper{
		padding-left: 0px;
	}
</style>
@stop

@section('content')
<div class="container">
	<div class="card row" style="width:100%;">
	    @if(!empty($error))
	      <div class="warning error" style="overflow:visible !important;padding: 15px;">{{ $error }}</div>
	    @elseif(!empty($success))
	      <div class="green"  style="overflow:visible !important;padding: 15px;">{{ $success }}</div>
	    @endif
	</div>
</div>
@stop