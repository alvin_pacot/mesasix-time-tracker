<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>{{ env('APP_NAME') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{asset ('/bower_components/materialize/dist/css/materialize.min.css')}}"/>
      {!! HTML::style('/appcss/app.css') !!}
      {!! HTML::style('/appcss/media-query.css') !!}
      <link rel="stylesheet" type="text/css" href="{{ asset('/bower_components/sweetalert/dist/sweetalert.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="main-wrapper bg-gradient">
      <div class="main-header">
        @yield('main-nav')
      </div>
      <div class="inner-wrapper">
        @yield('content')
      </div>
      @yield('footer')  
    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery -->
    <!-- // <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->
    <script src="{{ asset("/bower_components/jquery/dist/jquery.min.js") }}"></script>
    <!-- MATERIALIZE JS -->
    <script src="{{ asset ("/bower_components/materialize/dist/js/materialize.min.js") }}" type="text/javascript"></script>
    <!-- App -->
    <script src="{{ asset ("/appjs/app.js") }}" type="text/javascript"></script>
    <script src="{{ asset('/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script> 
    @include('sweet::alert')
      @yield('js')
    <script type="text/javascript">
      
    </script>
  </div>
  </body>
  </html>