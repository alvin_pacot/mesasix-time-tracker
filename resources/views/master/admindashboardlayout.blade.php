<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>{{ env('APP_NAME') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('/bower_components/materialize/dist/css/materialize.min.css') }}"/>
      {!! HTML::style('/appcss/app.css') !!}
      {!! HTML::style('/appcss/media-query.css') !!}
    <link rel="stylesheet" type="text/css" href="{{ asset('/bower_components/sweetalert/dist/sweetalert.css') }}">
    <!--link rel="stylesheet" type="text/css" href=" asset('/bower_components/sweetalert/themes/google/google.css')"-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style type="text/css">html{ background: #EDECEC;}</style>
    @yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="main-wrapper">
      <!-- Sidebar menu-->
      <div class="sidebar-wrapper">@include('partial.sidebar')</div>
      <!-- Header -->
      <div class="header-wrapper z-depth-1 navbar-fixed hide-on-med-and-down">@include('partial.header')</div>
      <div class="clear-fixed"></div>
      <!-- Content -->
      <div class="content-wrapper">
        <div class="timesheet">
          @yield('content')   
          @yield('slidediv')
        </div>
      </div>
    </div>
              

    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery -->
    <!-- <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->
    <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>-->
    <script src="{{ asset("/bower_components/jquery/dist/jquery.min.js") }}"></script>
    <!-- MATERIALIZE JS -->
    <script src="{{ asset ("/bower_components/materialize/dist/js/materialize.min.js") }}" type="text/javascript"></script>
    <!-- App -->
    <script src="{{ asset ("/appjs/app.js") }}" type="text/javascript"></script>
    <script src="{{ asset('/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script> 
    @include('sweet::alert')
    @yield('js')
  </body>
</html>