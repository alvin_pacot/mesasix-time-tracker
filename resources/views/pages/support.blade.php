@extends('master.homelayout')

@section('main-nav')
	@if(!Auth::check())
	 	@include('partial.main-nav-guest')
	@else
		@include('partial.main-nav-auth')
	@endif
@stop

@section('css')
<style type="text/css">	
	.inner-wrapper{ padding-left: 0px;}
	.collapsible-body{ background: #F5F5F0;}
	.support-content{ overflow-x: hidden;padding: 5px 20px;display: block;}
	.padded{ padding-top: 20px !important;}
	p{ text-align: left !important;}
	img {display: block !important;}
	.bg-img{
		background-image: url(appimg/tech-guy.png);	
    	display: block;
    	margin-left: auto;
    	margin-right: auto;
    	background-repeat: no-repeat;
   	    background-position: 0px 50px;
   	 	background-size: 232px 300px;
    }
    .support{ top: 320px;position: relative;left: -20px;}
    .bg-gradient{background: inherit;}
    .support-main{ border:0px !important;margin-top:20px;}
</style>
@stop

@section('content')
<div class="container tech-support">
<div class="row padded bg-img">
	<div class="col s12 m12 l3 support">
		<div class="row"><h4 style="text-align:center;">Supports</h4></div>
		<div class="row"><h6 style="text-align:center;">We are here to help.</h6></div>
	</div>
	<div class="col s12 m12 l9">
		 <ul class="collapsible support-main" data-collapsible="accordion">
		    <li>
		      <div class="theme-text collapsible-header"><i class="icon material-icons">add</i>How to Setup Outgoing Webhook Integration</div>
		      <div class="collapsible-body">
		      	<div class="support-content">
		      		<h5>Setting up Outgoing Webhook Integration</h5>
		      		<blockquote>After the successful team registration, you need to create an Outgoing Webhook Integration in order to fully utilize the commands for the app. If you haven't yet setup an Outgoing Webhook or updated your current change, this section will guide you on how to setup.</blockquote>
		      		<br>1. Click <a href="https://my.slack.com/services/new/outgoing-webhook" target="_blank">Outgoing Webhook</a>.
		      		<br>2. Click <b>Add Outgoing WebHooks Integration</b>. In the Integration settings,
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/outgoing2.png') }}">
		      		<br>3. Select the public channel in the <strong>Channel</strong> field, where will be used for signing in, out, brb and back.
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/outgoing4.png') }}">
		      		<br>4. In the <strong>Trigger Word(s)</strong> field, input this trigger words: <i class="theme-text">in, out, brb, back, ++, -- </i>  (comma include)
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/outgoing3.png') }}">
		      		<br>5. In the <strong>URL</strong> field, copy and paste this url to the URL textfield <i class="theme-text">{{env('APP_HANDLER')}}</i> 
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/url.png') }}">
		      		<br>6. The rest of the setting are optional.
		      		<br>7. Save the setting.
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/outgoing6.png') }}">
		      	</div>	
		      </div>
		    </li>
		    <li>
		      <div class="theme-text collapsible-header"><i class="icon material-icons">add</i>How to Setup Slash Command Integration</div>
		      <div class="collapsible-body">
		      	<div class="support-content">
					<h5>Setting up Slash Command Integration</h5>
					<blockquote>This setup was already configured during the team registration but incase you find trouble in slash command, this section will guide you in creating a slash command for your team.</blockquote>
					<br>1. Create a <a href="https://my.slack.com/services/new/slash-commands" target="_blank">Slash Command Integration</a>.
					<br>2. In the Command field, input: <I class="theme-text">/m6</I> then <strong>Click Add Slash Command Integration</strong>.
					<img class="support-img responsive-img" src="{{ asset('/appimg/slash2.png') }}">
					<br>3. In the <strong>URL</strong> field, copy and paste this url: <I class="theme-text">{{env('APP_HANDLER')}}</I>
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/url.png') }}">
		      		<br>4. In the <strong>Method</strong> field, select <I class="theme-text">POST</I>
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/slash3.png') }}">
		      		<br>5. For <strong>Autocomplete help text</strong>, just check the checkbox for the hints. (Optional)
		      		<br>6. Save the Integration.
		      	</div>
		      </div>
		    </li>
		    <li>
		      <div class="theme-text collapsible-header"><i class="icon material-icons">add</i>How to Setup Incoming Webhook Integration</div>
		      <div class="collapsible-body">
		      	<div class="support-content">
		      		<h5>Setting up Incoming WebHooks</h5>
		      		<blockquote>After team was succesfully registered to {{env('APP_NAME')}}, an Incoming Webhook was already configured during the setup process.Updating or changing the chosen channel during the setup process are not yet supported. Below is the setup process for Incoming Webhook Integration.</blockquote>
		      		<br>1. After you successfully authenticated, Click PROCEED  to start setting up the Incoming WebHooks.
		      		<img class="support-img responsive-img" src="{{ asset('/appimg/incoming1.png') }}">
					<br>2. Select the desired Channel where the messages will be posted from external sources. (Note: <qoute>Overriding or changing channel once setup is not currently supported. [Slack]</qoute>) 
					<img class="support-img responsive-img" src="{{ asset('/appimg/incoming2.png') }}">
					<br>3. Click <strong>Authorize</strong>, it allows {{env('APP_NAME')}} to fetch required information of the team and members ONLY from Slack. {{env('APP_NAME')}} will NOT fetch files and messages.
		      	</div>
		      </div>
		    </li>
		    <li>
		    	<div class="theme-text collapsible-header"><i class="icon material-icons">add</i>How to add new members</div>
		    	<div class="collapsible-body">
		    		<div class="support-content">
		    		<blockquote>In adding new members, the member must signin in the dedicated channel or the member login to this app using his Slack account.</blockquote> 
		    		<blockquote>User must have a full membership to the team, unrestricted and not disabled.</blockquote>
		    		</div>
		    	</div>
		    </li>
		    <li>
		    	<div class="theme-text collapsible-header"><i class="icon material-icons">add</i>How to update team info and settings</div>
		    	<div class="collapsible-body">
		    		<div class="support-content">
		    		<blockquote>Updating team information and setting are features for team admin and team owners only and can be found at the dashboard. </blockquote>
		    		</div>
		    	</div>
		    </li>
		    <li>
		      <div class="theme-text collapsible-header active"><i class="icon material-icons">remove</i>How {{env('APP_NAME')}} works</div>
		      <div class="collapsible-body">
		      	<div class="support-content">
		      			<blockquote>{{env('APP_NAME')}} display based on user`s shifts instead of a daily basis, this allows users to view their accumulated time from the time they signed in till the time they sign out without being cut at midnight.</blockquote>
		      			<blockquote>{{env('APP_NAME')}} display the breakdown of command in the timezone of the user. If a user need to change their view for timezone, they need to change their timezone in Slack then update their profile to accomodate the changes made.</blockquote>
		      			<blockquote>{{env('APP_NAME')}} notify user if they reach 8 hours during their shift and will ask for extension of work, failure to respond to the notification at a given time will result to automatically signout from shift. This to ensure a user does not idle during work time.</blockquote>
		      			<blockquote>{{env('APP_NAME')}} limit the working shift of a user to max of 16 hours (breaks not included) and will automatically signout after that. (We believe that working after 16 hours of work are not productive)</blockquote>
		      	</div>
		      </div>
		    </li>
		  </ul>
	</div>
</div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(function(){
	$('.collapsible-header').click(function(){
		var x = $(this).find('.material-icons').text();
		$('.material-icons').text('add');
		if (x == 'add') { $(this).find('.material-icons').text('remove'); }
		if (x == 'remove') { $(this).find('.material-icons').text('add'); }
	});
});
</script>
@stop