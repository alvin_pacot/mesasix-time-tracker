@extends('master.admindashboardlayout')
@section('page-title')
    Team Projects
@endsection

@section('css')
    <!-- progressbar -->
    <link href="{{ asset("/appcss/progress-bar.css") }}" rel="stylesheet" type="text/css" />
    <!-- jquery-ui datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/bower_components/jquery-ui/themes/flick/jquery-ui.min.css') }}">
   <style type="text/css">
      .padded{ padding: 10px;}
      .cursor{ cursor: pointer;}
      .button-group{ padding-bottom: 10px;}
      .m-bottom{ margin-bottom: 10px ;}
      .ui-datepicker-title{display: block;}
      .pagination li.active{background: teal;}
   </style>
@stop

@section('content')
<div class="row">
    <ul class="pagination full-width-on-small col s12 m6 l6">
      {!! $projects->render() !!}
    </ul>
    
    <div class="col s12 m6 l6"style="height:36px !important;float:right;">
        <button class="m-bottom right full-width-on-small waves-effect btn-flat teal lighten-2 white-text modal-trigger" href="#create-project"  title="New Project">NEW PROJECT</button>
    </div>
    <div class="col s12 m12 l12">
          <div class="timebar my-timebar theme row">
            <div class="white-text">
              <div class="col s12 m6 l4 head-title" style="text-indent:30px;">Project Title</div>
              <div class="col s12 m3 l3 head-title hide-on-med-and-down">Project Lifespan</div>
              <div class="col s12 m6 l5 head-title hide-on-small-only" style="text-align:center;">Project Lifespan Progress</div>
            </div>
          </div>
          <div class="row">
            <ul class="collapsible" data-collapsible="expandable">
            @if(!empty($projects))
              @foreach($projects as $proj)
                <li>
                  <div class="collapsible-header row gray project-expand-header" style="padding:1px 15px !important;">
                    <div class="col s12 m6 l4 "><i class="material-icons">perm_data_setting</i>{{$proj['name']}}</div>
                    <div class="col s12 m3 l3 hide-on-med-and-down">{{ $proj['span'] }}</div>
                    <div class="col s12 m6 l5">
                        <div class="progressed progress-large progress-stacked progressed-stripe progress-animate" style="height: 40px;position:relative;top:2px;">
                          @foreach($proj['bar'] as $bar)
                            <span style="width:{{$bar[0]}}%" title="{{ $bar[2] }}" class="{{ $bar[1] }}"></span>
                          @endforeach
                        </div>
                    </div>
                  </div>
                  <div class="collapsible-body row" style="padding:1px 15px !important;">
                      <div class="col s12 m5 l5">
                          <table class="compressed-table">
                              <tr>
                                <td><strong>Project Details</strong></td>
                                <td>
                                  <a class="cursor theme-text" onclick="setUpdateInfo({{$proj->id}},'{{$proj->name}}','{{$proj->desc}}','{{$proj->startformat}}','{{$proj->dueformat}}','{{$proj->channel}}','{{$proj->project_stat}}')">UPDATE</a> | 
                                  <a class="cursor theme-text" onclick="setRemoveId({{$proj->id}},'{{$proj->name}}')">REMOVE</a>
                                </td>
                              </tr>
                              <tr><td><i class="material-icons tiny">description</i> Description :</td><td>{{ $proj['desc'] }}</td></tr>
                              <tr><td><i class="material-icons tiny">info</i> Status :</td><td>{{ $proj['project_stat'] }}</td></tr>
                              <tr><td><i class="material-icons tiny">play_for_work</i> Project Start :</td><td>{{ $proj['project_start_det'] }}</td></tr>
                              <tr><td><i class="material-icons tiny">launch</i> Project Due :</td><td>{{ $proj['project_due_det'] }}</td></tr>
                              <tr class="hide-on-large-only"><td><i class="material-icons tiny">web</i> Lifespan :</td><td>{{ $proj['span'] }}</td></tr>
                              <tr><td><i class="material-icons tiny">announcement</i> Slack Channel :</td><td>{{ $proj['channel'] }}</td></tr>
                          </table>
                      </div>
                      <hr class="hide-on-med-and-up" />
                      <div class="col s12 m7 l7">
                        <table class="compressed-table highlight">
                            <tr><td><strong>Assigned Member</strong></td><td><strong>Tasks</strong></td><td><a class="cursor theme-text" onclick="newtask('{{$proj['id']}}')">NEW TASK</a></td></tr>
                        @if(!empty($proj['assigned']))
                            @foreach($proj['assigned'] as $abc)
                                <tr>
                                  <td><i class="material-icons tiny">person_pin</i> {{$abc[0]}}</td>
                                  <td>{{$abc[1]}}</td><td><a href="#" class="btn-small" onclick="setIds({{$abc[2]}},'{{$abc[0]}}',{{$proj['id']}},'{{$abc[3]}}')">REMOVE TASK</a></td>
                                </tr>
                            @endforeach
                        @endif
                        </table> 
                      </div>
                  </div>
                </li>
                @endforeach
            @else
                <li class="padded green white-text lighten-3">No project yet. </li>
            @endif
              </ul>
          </div>
    </div>
</div>
@stop

@section('slidediv')
  @include('partial.remove_project')
  @include('partial.update_project_form')
  @include('partial.add_project_task_form')
  @include('partial.create_project_form')
  @include('partial.remove_task')
@stop

@section('js')
<!-- moment -->
<script src="{{ asset ("/appjs/moment.js")  }}" type="text/javascript"></script>
<!-- jquery-ui js -->
<script type="text/javascript" src="{{ asset ("/bower_components/jquery-ui/jquery-ui.min.js")  }}"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#ustat').click(function(){
    var s = moment($('#udatepicker-start').val()).format();
    var n = moment($('#udatepicker-end').val()).format();
    var c = moment().format();
    $('#ustat option:gt(0)').prop('disabled','');
    if (s > c) { $('#ustat option:gt(1)').prop('disabled','disabled'); }//ongoing or not yet started
    if (n < c) { $('#ustat option:lt(3)').prop('disabled','disabled'); }
  });
  $('.modal-trigger').leanModal();
  $('select').material_select();
  $('.close5,.cancel5').click(function(){ $( ".toggles-update" ).toggleClass("hide"); });
  $( "#datepicker-start" ).datepicker({
      defaultDate: 0,
      minDate:0,
      dateFormat:"yy-mm-dd", 
      changeMonth: true,
      monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", 
                 "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      onClose: function( selectedDate ) {
        $( '#datepicker-end' ).datepicker( "option", "minDate", selectedDate );
        if (moment(selectedDate) <= moment()) { $('#stat').val('ONGOING');
        }else{ $('#stat').val('NOT YET STARTED'); }
      }
    });
    $( "#udatepicker-start" ).datepicker({
      minDate:0,
      dateFormat:"yy-mm-dd", 
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( '#udatepicker-end' ).datepicker( "option", "minDate", selectedDate );
        if (moment(selectedDate) <= moment()) { $('#ustat').val('ONGOING');
        }else{ $('#ustat').val('NOT YET STARTED'); }
      }
    });
    $( "#datepicker-end" ).datepicker({
      defaultDate: 0,
      minDate:0,
      changeMonth: true,
      dateFormat:"yy-mm-dd",
    });
    $( "#udatepicker-end" ).datepicker({
      minDate:0,
      changeMonth: true,
      dateFormat:"yy-mm-dd",
    });
    $("#formCreate").submit(function(event){
      if($("#datepicker-start").val().length === 0){
        Materialize.toast('Please select a starting date.',4000,'theme white-text');
        event.preventDefault();
      }else if ($("#channel").val() === null) {
        Materialize.toast('Please select a channel.',4000,'theme white-text');
        event.preventDefault();
      }
    });
    $("#formUpdate").submit(function(event){
      if($("#udatepicker-start").val().length === 0){
        Materialize.toast('Please select a starting date.',4000,'theme white-text');
        event.preventDefault();
      }else if ($("#uchannel").val() === null) {
        Materialize.toast('Please select a channel.',4000,'theme white-text');
        event.preventDefault();
      }
    });
    $("#formAddTask").submit(function(event){
      if ($("#assigned").val() === null) {
        Materialize.toast('Please select a user for the task.',4000,'theme white-text');
        event.preventDefault();
      } 
    });
});
function newtask(pid){
    $( 'input#project_id' ).val(pid);
    $( "#add-task" ).openModal();
}
function setIds(puid,name,projid,uid) {
  $('#username').text(name);
  $('#removepuid').val(puid);
  $('#removeprojectid').val(projid);
  $('#removeuserid').val(uid);
  $('#remove').openModal();
}
function setRemoveId(id,name){
  $('#delprojectid').val(id);
  $('#projectname').text(name);
  $('#remove-project').openModal(); 
}
function setUpdateInfo(id,name,desc,start,due,channel,stat){
  $('#uproject_id').val(id);
  $('#uname').val(name);
  $('#uoldname').val(name);
  $('#udesc').val(desc);
  $('#udatepicker-start').val(start);
  if (moment(start) > moment()) {
    $('#udatepicker-start' ).datepicker( "option", "minDate", 0);
  }else{
    $('#udatepicker-start' ).datepicker( "option", "minDate",start );
  }
  $('#udatepicker-end' ).datepicker( "option", "minDate",start );
  $('#udatepicker-end').val(due);
  $("#uchannel option[value='"+channel+"']").attr('selected',true);
  $('#ustat').val(stat);
  $( "#update-project" ).openModal();
}
</script>
@stop