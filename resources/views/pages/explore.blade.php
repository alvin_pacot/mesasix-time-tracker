@extends('master.homelayout')

@section('main-nav')
	@if(!Auth::check())
	 	@include('partial.main-nav-guest')
	@else
	@include('partial.main-nav-auth')
	@endif
@stop

@section('css')
<style type="text/css">	
	.inner-wrapper{
		padding-left: 0px;
	}
</style>
@stop

@section('content')
<div class="container" style="text-align:left;">
	<div><h4 style="text-align:center;">Application Features</h4></div>
	<section>
		<ul class="collapsible popout" data-collapsible="accordion">
    	<li>
      		<div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
    	</li>
    	<li>
     		 <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
      		 <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
    	</li>
    	<li>
      		<div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">face</i>Fourth</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">favorite</i>Fifth</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">class</i>Sixth</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">grade</i>Seventh</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">loyalty</i>Eight</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">schedule</i>Ninth</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
   		 <li>
      		<div class="collapsible-header"><i class="material-icons">tab</i>Tenth</div>
      		<div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   		 </li>
  		 </ul>
	</section>
</div>

@stop