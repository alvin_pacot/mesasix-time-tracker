@extends('master.admindashboardlayout')
@section('page-title') Weekly Records @endsection
@section('css')
    <!-- progressbar -->
    <link href="{{ asset("/appcss/progress-bar.css") }}" rel="stylesheet" type="text/css" />
    <!-- jquery-ui theme css -->
    <link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/jquery-ui/themes/flick/jquery-ui.min.css") }}">
    <style type="text/css">
    #nextweek{  float: right !important;top: 5px;position: relative;}
    #prevweeks{position: relative;top: 5px;}
    .week-label{ padding: 0 10px !important; position: relative;}
    .week-group{position: relative; top: 12px;}
    .button-group{ height:36px;padding: 5px 0px;}
    .icons{ width: 180px; border-radius: 2px;}
    .weekly-cal{ float: right !important}
    .select-dropdown{border-bottom: transparent !important;}
    input.select-dropdown{
      color: white;
      text-transform: uppercase;
      line-height: normal !important;
      text-align: center;
      height: 36px !important;
    }
    .caret{ 
      color: white !important;
      font-weight: bolder;
      top: 11px !important;
      right: 10px !important;
    }
    </style>
@stop

@section('content')
<div class="row button-group">
  <div class="col s12 m6 l3" style="height:36px !important;max-width: 185px;">
    <select class="icons teal lighten-2 full-width-on-small" name="user_id">
        <option value="{{Auth::user()->user_id}}" disabled selected>Select User</option>
      @foreach($members as $member)
        @if($member['user_id'] == $selecteduser)
        <option value="{{$member['user_id']}}" selected data-icon="{{$member['avatar']}}" class="circle">{{$member['user_name']}}</option>
        @else
        <option value="{{$member['user_id']}}" data-icon="{{$member['avatar']}}" class="circle">{{$member['user_name']}}</option>
        @endif
      @endforeach
    </select>
  </div>
  <div class="col s12 m3 l5 hide-on-med-and-down"><!-- tab-group -->
    <a href="{{ route('dashboard::theyrecords',['id'=>Auth::user()->user_id,'week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="full-width-on-small btn-flat teal yellow-text wave waves-effect lighten-2">Weekly</a>
    <a href="{{ route('dashboard::theymonthly',['id'=>Auth::user()->user_id,'month'=>date('n'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="full-width-on-small btn-flat teal white-text wave waves-effect lighten-2">Monthly</a>
  </div>
  <div class="col s12 m3 l7 row weekly-cal"><!-- right row week-group -->
     <a class="prevweek col s1" href="{{ route('dashboard::theyrecords',['team'=>auth()->user()->getTeamDomain()->team_domain,'user'=>auth()->user()->user_name ])}}/{{ App\Http\Controllers\MyWeeklyController:: getPrevUrl($year,$week)}}/{{$selecteduser}}">
        <img id="prevweeks" src="{{ asset('/appimg/teal-arrow-left.png') }}" alt="prev week"/>
    </a>
      <input class="week-picker" type="hidden">
      <button class="col s10 week-label blue-text btn-flat">{{$wk['week_start'] or (new DateTime)->setISODate($year, $week)->format('M, d Y')}} - {{ $wk['week_end'] or (new DateTime)->setISODate($year, $week)->modify('+6 days')->format('M, d Y')}}</button>
      <a class="nxtweek col s1" id="nxtwk" href="{{ route('dashboard::theyrecords',['team'=>auth()->user()->getTeamDomain()->team_domain,'user'=>auth()->user()->user_name ])}}/{{ App\Http\Controllers\MyWeeklyController:: getNextUrl($year,$week)}}/{{$selecteduser}}">
        <img id="nextweek" src="{{ asset('/appimg/teal-arrow-right.png') }}" alt="next week"/>
      </a>
  </div>
    <input id="y" type="hidden" value="{{$year}}">
    <input id="w" type="hidden" value="{{$week}}">
    <input type="hidden" id="url" value="{{ route('dashboard::theyrecords',['team'=>auth()->user()->getTeamDomain()->team_domain,'user'=>auth()->user()->user_name ]) }}">
</div>

      <div class="timebar theme row">
        <div class="row white-text">
          <div class="col s5 m5 l3 head-title" style="text-indent:30px;">Day</div>
          <div class="col s7 m7 l2 head-title">Total Worked Time</div>
          <div class="col s12 m7 l7 head-title hide-on-med-and-down" style="text-align:center;">Activities</div>
        </div>
      </div>
      @if(!empty($data))
      <ul class="row collapsible" data-collapsible="expandable">
        @foreach($data as $key => $day)
        <li>
          <div class="row collapsible-header gray expand-header record-expand-header">
            <div class="col s12 l6">
              <div class="row">
                <div class="col s5 l6">
                    <strong style="position:relative;top:11px;left:15px;">{{ $day['day'] }}</strong>
                </div>
                <div class="col s7 l6">
                  <strong style="position: relative;top: 13px;">{{ $day['worktime'] }}</strong><br/>
                </div>
              </div>
            </div>
            <div class="col s12 l6 row">
                <div class="progressed progress-large progress-stacked activity-bar progressed-stripe progress-animate" style="height: 55px;position:relative;top:2px;">
                  @foreach($day['progressbar'] as $key => $bar)
                    <span style="width:{{$bar[0]}}%" title="{{$bar[2]}}" class="{{ $bar[1] }}"></span>
                  @endforeach
                </div>
              
            </div>
          </div>
          <div class="collapsible-body expand-body row">
            <div class="col s12 m6 l3">
            <h6>Summary</h6>
              <table class="compressed-table">
                <tr><td><i class="material-icons green-text">schedule</i><span> Worktime</span></td><td>{{ $day['worktime']}}</td></tr>
                <tr><td><i class="material-icons blue-text">schedule</i><span> Breaktime</span></td><td>{{ $day['breaktime']}}</td></tr>
                <tr><td><i class="material-icons overtime-text">schedule</i><span> Overtime</span></td><td>{{ $day['extendedtime']}}</td></tr>
              </table>
            </div>
            <div class="col s12 m6 l3">
              <h6>Breakdown <span class="theme-text">( {{Auth::user()->tz}} Time)</span></h6>
              @if(!empty($day['breakdown']))
                <table class="compressed-table">
                  @foreach($day['breakdown'] as $brk)
                    <tr><td>{{ $brk[0] }}</td><td>{{ $brk[1] }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
            <div class="col s12 m12 l3">
              <h6>Activity Reports</h6>
              @if(!empty($day['activity']))
                <table class="compressed-table">
                  @foreach($day['activity'] as $act)
                    <tr><td><i class="tiny material-icons">done</i> {{ $act }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
            <div class="col s12 m12 l3">
              <h6>To Do</h6>
              @if(!empty($day['todo']))
                <table class="compressed-table">
                  @foreach( $day['todo'] as $tod )
                    <tr><td><i class="tiny material-icons">assignment</i> {{ $tod }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
          </div>
        </li>
        @endforeach
      </ul>
      <div class="row theme white-text total-accu">
        <div class="col s12 m5 l3" style="text-indent:15px;">Total Weekly Working Time : </div>
        <div class="col s12 m7 l9 totals">{{$total_weekly}}</div>
      </div>
      @endif
      @if(!empty($error))
        <div class="card-panel red white-text lighten-3">{{ $error }}</div>
      @endif
@stop
@section('slidediv')
<div class="fixed-action-btn hide-on-large-only" style="bottom: 40px; right: 25px;">
    <a class="btn-floating btn-large red">
      <i class="large mdi-navigation-menu">mode_edit</i>
    </a>
    <ul>
      <li><a href="{{ route('dashboard::theyrecords',['id'=>Auth::user()->user_id,'week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="btn-floating wave waves-effect black"><b>W</b></a></li>
      <li><a href="{{ route('dashboard::theymonthly',['id'=>Auth::user()->user_id,'month'=>date('n'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="btn-floating wave waves-effect blue"><b>M</b></a></li>
    </ul>
  </div>
@stop

@section('js')
<script type="text/javascript" src="{{ asset ("/bower_components/jquery-ui/jquery-ui.min.js")  }}"></script>
<script type="text/javascript">
$(function() {
    $('select').material_select();
    $('select').change(function () {
      var y = $('#y').val();
      var w = $('#w').val();
      window.location = $('#url').val()+"/"+y+"/"+w+"/"+$(this).val();
    });
    $('.week-label').click(function(){
      $('.week-picker').datepicker('show');  
    });
    $('.nxtweek').click(function(event){
      var href = $(this).attr('href');
      // var wk = href.substr(href.lastIndexOf('/') + 1);
      var wk = href.split('/')[10];
      if (wk > (new Date()).getWeek()) {
        Materialize.toast('You can`t select week higher than the current week.',1000,'theme white-text');
        event.preventDefault();
      }
    });
    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }   
    Date.prototype.getWeek = function() {
        var onejan = new Date(this.getFullYear(),0,1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
    }
     function getLastWeek(){
      var today = new Date();
      var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
      return lastWeek ;
    }
    var startDate;
    var endDate;
    $('.week-picker').datepicker( {
        firstDay:1,
        selectWeek:true,
        showWeek:true,
        maxDate:new Date(),
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function() { 
            selectCurrentWeek();
            var date = $(this).datepicker('getDate');
            var today = new Date();
            var currentWeek = today.getWeek();
            var selectedWeek = $.datepicker.iso8601Week(date);
            if (selectedWeek > currentWeek) {
              Materialize.toast('You can`t select a week higher than the current week:'+currentWeek,1000,'theme white-text');
            }else{
              window.location = $('#url').val()+"/"+date.getFullYear()+"/"+selectedWeek+"/"+$('select').val();
            }
        },
        beforeShow: function() { setTimeout("applyWeeklyHighlight()", 100);},
        beforeShowDay: function(date) {
          setTimeout("applyWeeklyHighlight()", 100);
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
            setTimeout("applyWeeklyHighlight()", 100);
        }
    });
  $(document.documentElement).keyup(function (e) {
      if (e.keyCode == 39){
        var href = $('.nxtweek').attr('href');
        // var wk = href.substr(href.lastIndexOf('/') + 1);
        var wk = href.split('/')[10];
        if (wk > (new Date()).getWeek()-1) {
          Materialize.toast('You can`t select week higher than the current week.',1000,'theme white-text');
          event.preventDefault();
        }else{
            window.location = href;;
        }
      }
      if (e.keyCode == 37){
        window.location = $('.prevweek').attr('href');
      } 
    });
});
function applyWeeklyHighlight()
{
    $('.ui-datepicker-calendar tr').each(function() {
        if($(this).parent().get(0).tagName == 'TBODY'){
            $(this).mouseover(function() {
                    $(this).find('a').css({'background':'#ffffcc','border':'1px solid #dddddd'});
                    $(this).find('a').removeClass('ui-state-default');
                    $(this).css('background', '#ffffcc');
            });
            $(this).mouseout(function() {
                    $(this).css('background', '#ffffff');
                    $(this).find('a').css('background','');
                    $(this).find('a').addClass('ui-state-default');
            });
        }
    });
}
</script>
@stop
