@extends('master.admindashboardlayout')
@section('page-title') Schedule @endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/jquery-ui/themes/flick/jquery-ui.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/bower_components/fullcalendar/dist/fullcalendar.min.css") }}"/>
    <style type="text/css">    
    .ui-datepicker table{ font-size:  64.65% !important; }
    .ui-datepicker-inline{ width: inherit; }
    .fc-ltr.ui-widget{ padding: 15px 5px !important; background: rgb(245, 245, 240);}
    .timesheet{ padding: 10px;}
    </style>
@stop
@section('content')
<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
<div class="clear-fixed"></div>
<!-- <div class="clear-fixed"></div> -->
<div class="row">
    <div class="col s12 m12 l12" id='calendar'></div>
    <div class="col s12 hide event-panel" id='external-events'>
            <div class="input-field">
            <select class="icons teal lighten-2 full-width-on-small" id="user_id">
            <option value="{{Auth::user()->user_id}}" disabled selected>Select User</option>
              @foreach($user as $member)
                @if($member->user_id == $selecteduser->user_id)
                <option value="{{$member->user_id}}" selected data-icon="{{$member->user_avatar}}" class="circle">{{$member->user_name}} {{($member->user_id == Auth::user()->user_id) ? '(self)' : ''}}</option>
                @else
                <option value="{{$member->user_id}}" data-icon="{{$member->user_avatar}}" class="circle">{{$member->user_name}} {{($member->user_id == Auth::user()->user_id) ? '(self)' : ''}}</option>
                @endif
              @endforeach
            </select>
            </div>
        <div class="draggable row">
            <h6 class="hide-on-med-and-down purple-text">Drag and Drop Events</h6>
            <div class='col s4 m4 l12 fc-event green darken-2 white-text' title="Add a public event" >Public Event</div>
            <!-- <div class='col m4 l6 fc-event green lighten-1 white-text' title="Add a public event" >Public (Normal)</div> -->
            <div class='col s4 m4 l12 fc-event blue darken-2 white-text' title="Add a private event" >Private Event</div>
            <!-- <div class='col m4 l6 fc-event blue lighten-1 white-text' title="Add a private event" >Private (Normal)</div> -->
            <div class='col s4 m4 l12 fc-event red darken-2 white-text' title="Add a special event" >Special Event</div>
            <!-- <div class='col m4 l6 fc-event red lighten-1 white-text lighten-1' title="Add a special event" >Special (Normal)</div> -->
            <div class="drop_div white-text col m12" id="trash" >
                <p><i class="material-icons small">delete</i><span class=""> Drop here to delete</span></p>
            </div>
        </div>
        <div id="datepicker"></div>
    </div>
    <div class="clear-fixed"></div>
    <div class="col s12 m12 l12 row" id="clock">
        <div class="col s12 m12 l3">
            <label>Team timezone : </label>
            <label class="purple-text" id="team_tz">{{$timezone}}</label>
            <label class="purple-text toffset"></label>
        </div>
        <div class="col s12 m12 l3">
            <label>Team timezone time : </label>
            <label class="purple-text" id="team_clock"></label>
        </div>
        <div class="col s12 m12 l3">
            <label>Your timezone : </label>
            <label class="orange-text" id="your_tz">{{Auth::user()->tz}}</label>
            <label class="orange-text yoffset"></label>
        </div>
        <div class="col s12 m12 l3">
            <label>Your timezone time : </label>
            <label class="orange-text" id="your_clock"></label>
        </div>
    </div>
</div>
@stop
@section('js')
    <script type="text/javascript" src="{{ asset("/bower_components/jquery-ui/jquery-ui.min.js") }}"></script>
    <script src="{{ asset("/bower_components/moment/min/moment.min.js")  }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js" type="text/javascript"></script>
    <script type="text/javascript" src=" asset("/appjs/moment-timezone-with-data.js") "></script>
    <script type="text/javascript" src="{{ asset('/bower_components/fullcalendar/dist/fullcalendar.js') }}"></script>
    <script src="{{ asset('/bower_components/jquery.ui.touch/jquery.ui.touch.js')}}"></script> 
    <script type="text/javascript">
    $( document ).ready(function() {
        $('select').material_select();
        var your_tz = $('#your_tz').text();
        $('.toffset').text(moment.tz(new Date(),$('#team_tz').text()).format('Z'));
        $('.yoffset').text(moment.tz(new Date(),your_tz).format('Z'));
        setInterval(function(){
            var teamdate = moment.tz(new Date(),$('#team_tz').text());
            var yourdate = teamdate.clone().tz(your_tz);
            $('#team_clock').text(teamdate.format('lll'));
            $('#your_clock').text(yourdate.format('lll'));
        },1000);
        var tzone = $('#team_tz').text();
        var yzone = $('#your_tz').text();
        var zone = getOffset(your_tz);
        $('select#user_id').change(function(){
            var id = $('select#user_id').val();
            window.location = "{{route('dashboard::teamsched',['user'=>Auth::user()->user_name,'team'=>Auth::user()->getTeamDomain()->team_domain])}}/"+id;
        });
        $( ".draggable div.fc-event" ).mousedown(function(){
            if(!isSchedOwner()){
                Materialize.toast('Cannot add event on {{$selecteduser->user_name}} calendar',4000,'theme white-text');
                return false;
            } return true;
        });
        var currentMousePos = { x: -1, y: -1 };
        jQuery(document).on("mousemove", function (event) {
            currentMousePos.x = event.pageX;
            currentMousePos.y = event.pageY;
        });
        // initialize the datepicker
        $('#datepicker').datepicker({
            dateFormat: 'yy-mm-dd', // needed for defaultDate
            defaultDate: 0,
            inline: true,
            monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            onSelect: function(dateText, inst) {
                $('#calendar').fullCalendar('gotoDate', new Date(dateText));
            }
        });
        /* initialize the external events
        -----------------------------------------------------------------*/
        $('#external-events .fc-event').each(function() {
            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
        });
        var handlerURL = '{{route("dashboard::scheds",["user"=>Auth::user()->user_name,"team"=>Auth::user()->getTeamDomain()->team_domain])}}'; 
        var token = '{{csrf_token()}}';
function renderCalendar() {
        $.ajax({
                url: handlerURL,
                type: 'POST', // Send post data
                data: 'type=fetch&_token='+token+"&user_id={{$selecteduser->user_id}}",
                async: false,
                success: function(s){ json_events = s }
        }); 
        //initialize calendar
        $('#calendar').fullCalendar({
            height:"auto",
            customButtons: { slideInfo: { text: 'show', click: function() { toggleSchedInfo(); } } },
            events:JSON.parse(json_events),
            utc: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,slideInfo'
                },//endheader
            defaultView:'month',
            editable: isSchedOwner(),
            eventDurationEditable:isSchedOwner(),
            droppable: isSchedOwner(),
            timezone:zone,
            theme:true,
            eventReceive: function(event,element){
               var title = event.title;
               var start = moment(event.start.format()).format();
               // console.log(start)
               $.ajax({
                 url: handlerURL,
                 data: { type:'new', title:title, startdate:start, _token:token },
                 type: 'POST',
                 dataType: 'json',
                 success: function(response){
                   event.id = response.eventid;
                   event.backgroundColor = response.backgroundColor;
                   $('#calendar').fullCalendar('renderEvent',event);
                 },
                 error: function(e){ console.log(e.responseText); }
               });
               $('#calendar').fullCalendar('updateEvent',event);
            },//endevenrecieved
            eventClick: function(event, jsEvent, view) {
                if(!isSchedOwner()){
                  Materialize.toast('Only event owner can update this',3000,'theme white-text');
                  return;  
                } 
                swal({   
                        title:"Event title",   
                        text: "Enter new title",
                        type: "input",   
                        inputValue:event.title,
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top"
                        }, function(inputValue){
                           if (inputValue === "" || inputValue === false) {
                                swal.showInputError("Title cannot be empty!");
                                return false
                            }else{      
                                event.title = inputValue;
                                $.ajax({
                                 url: handlerURL,
                                 data: { type:'changetitle', title:inputValue,
                                        eventid:event.id, _token:token
                                 },
                                 type: 'POST',
                                 dataType: 'json',
                                 success: function(response){
                                   if(response.status == 'success')
                                   $('#calendar').fullCalendar('updateEvent',event);
                                   swal("Nice!","Title updated",'success');
                                 },
                                 error: function(e){ displayError('New title',e); }
                               });
                            }
                    });
            },//endeventclick
            eventDrop: function(event, delta, revertFunc) {
                var title = event.title;
                var start = moment(event.start).format();
                var end = (event.end == null) ? start : moment(event.end).format();;
                $.ajax({
                    url: handlerURL,
                    data: {
                            eventid:event.id, title:title,end:end,
                            start:start, type:'resetdate', _token:token
                        },
                    type: 'POST',
                    dataType: 'json',
                    success: function(response){
                        if(response.status != 'success')                            
                        revertFunc();
                    },
                    error: function(e){                     
                        revertFunc();
                        displayError('Update',e)
                    }
                });
            },//endeventdrop
            dayClick: function(date, allDay, jsEvent, view) {
                if(!isSchedOwner()){
                  Materialize.toast('Only event owner can add events on their calendar',3000,'theme white-text');
                  return;  
                }
                swal({   
                        title: 'New Event',
                        text: "Enter event name, This event is public if you want a private click `show` button and choose your event",
                        type: "input",
                        inputPlaceholder:"Event name", 
                        showCancelButton: true,
                        closeOnConfirm: false 
                    }, 
                    function(inputValue) {
                        if (inputValue === "" || inputValue === false) {
                            swal.showInputError("Title cannot be empty!");
                            return false
                        }else{ 
                            $.ajax({
                                    url:handlerURL,
                                    type:'POST',dataType:'json',
                                    data: { // re-use event's data
                                        type:'newClick',
                                        title: inputValue,
                                        eventType:'public0',
                                        startdate: moment(date).format(),
                                        allDay:'true',
                                        _token:token
                                    },
                                    success: function(response){
                                       var id = response.eventid;
                                       var bgColor = response.backgroundColor;
                                       $('#calendar').fullCalendar('renderEvent', { id:id,title: inputValue, start: date, allDay: true,backgroundColor:bgColor}, true );
                                       swal('Success','Event Added','success');
                                     },
                                     error: function(e){ displayError('New Event',e) }
                            });
                        }
                    });        
            },
            eventResize: function(event, delta, revertFunc) {
                var title = event.title;
                var start = moment(event.start).format();
                var end = moment(event.end).format();
                $.ajax({
                    url: handlerURL,
                    type: 'POST', // Send post data
                    dataType: 'json',
                    data: {
                            eventid:event.id,
                            title:title,
                            end:end,
                            start:start,
                            type:'resize',
                            _token:$("#_token").val()
                        },
                    success: function(response){
                        if(response.status != 'success')                            
                        revertFunc();
                    },
                    error: function(e){                     
                        revertFunc();
                        console.log(e)
                        // displayError('Update Event',e);
                    }
                });
            },//endeventResize
            eventRender:function(event, element){
                $(element).addTouch();
                // render the timezone equivalent below the event title
                    if (event.start.hasZone()) {
                        element.find('.fc-title').after(
                            $('<div class="tzo"/>').text(ConvertTo(event.start.format(),$('#team_tz').text() )),
                            $('<div class="tzo"/>').text(ConvertTo(event.start.format(),$('#your_tz').text() ))
                        );
                    }
            },
            eventDragStop: function (event, jsEvent, ui, view) {
                if (isElemOverDiv()) {
                        swal({   
                            title: "Delete Event",   
                            text: "Are you sure to delete this event permanently?",   
                            type: "warning",   
                            showCancelButton: true,   
                            confirmButtonColor: "#DD6B55",   
                            confirmButtonText: "Yes, delete it!",   
                            closeOnConfirm: false 
                            }, function(){  
                                $.ajax({
                                    url: handlerURL,
                                    data: 'type=remove&eventid='+event.id+"&_token="+token,
                                    type: 'POST',
                                    dataType: 'json',
                                    success: function(response){
                                        if(response.status == 'success'){
                                            $('#calendar').fullCalendar('removeEvents');
                                            getFreshEvents();
                                            swal("Deleted!", "Event deleted.","success"); 
                                        }
                                    },
                                    error: function(e){  displayError('Delete Event',e); }
                                });
                        });   
                }
            }//endeventdragstop    
        });//calendar
    }
        renderCalendar();
    
        function getOffset(tzone){ return moment.tz(new Date(),tzone).format('Z')}
        function ConvertTo(date,tzone){ 
            var fromdate = moment(date);
            var st = tzone.split("/");
            return fromdate.tz(tzone).format('MM/DD h:mma ')+((st[1] == undefined) ? 'UTC' : st[1]);
        }
        function getFreshEvents(){
            $.ajax({
                url: handlerURL,
                type: 'POST', // Send post data
                data: 'type=fetch&_token='+token+"&user_id={{$selecteduser->user_id}}",
                async: false,
                success: function(s){ freshevents = s; }
            });
            $('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
        }

        function isElemOverDiv() {
            var trashEl = jQuery('#trash');
            var ofs = trashEl.offset();
            var x1 = ofs.left;
            var x2 = ofs.left + trashEl.outerWidth(true);
            var y1 = ofs.top;
            var y2 = ofs.top + trashEl.outerHeight(true);
            if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
                currentMousePos.y >= y1 && currentMousePos.y <= y2) {
                return true;
            }
            return false;
        }
    });//enddocready

    function isSchedOwner(){
        if ('{{$selecteduser->user_id}}' == '{{Auth::user()->user_id}}') {
            $('.fc-event').addTouch();
            return true;
        }return false;
    }
    function displayError(title,e){
        swal(title, 'Error processing your request: '+e.responseText, "error");
    }
    </script>
@stop
