@extends('master.admindashboardlayout')
@section('page-title') Profile @endsection
@section('css')
<style type="text/css">
  .card {
    box-shadow: none;
    background: transparent;
  }
  .tab a {background-color: transparent;}
  .tabs { background: transparent;}
  .tab a.active{ color: white !important;}
  .card-content{ padding: 0px 20px !important; }
  .img-title{
    position: relative;
    line-height: 50px;
    font-size: 3.5vh;
    font-weight: normal;
  }
  .profile-wrapper{ display: -webkit-flex; display: -ms-flexbox; display: flex; overflow: hidden;flex-wrap:wrap;}
  .right.flow-text { text-align: right;}
  .flow-text { text-align: left;}
  .card-tabs div.row{ padding: 10px !important;}
  .indent{ padding: 0 20px;}
  .card-avatar{
    -webkit-box-shadow: 11px 1px 19px -8px rgba(82,79,82,0.7);
    -moz-box-shadow: 11px 1px 19px -8px rgba(82,79,82,0.7);
    box-shadow: 11px 1px 19px -8px rgba(82,79,82,0.7);
  }
  .qoute{ padding: 10px !important;}
  img.uavatar {
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-radius: 50%;
    max-width: 100%;
  }
  .timebar{ height: 50px;}
  .frame{ padding: 20px;}
  .collection .collection-item.avatar { min-height: 58px;}
  .collection .collection-item {background-color: transparent;}
  .collection  { border:none;}
  .collection.frame{overflow: hidden;border: none;}
  .prof-button{ top: -54px;right: 18px;}
  blockquote{ width: 100%;text-align: left;}
</style>
@stop
@section('content')
	   <div class="row profile-wrapper clear-fixed bg-gray">
        <div class="col s12 m6 l3 card card-avatar">
            <div class="row">
              <div class="frame">
                <img class="uavatar"  src="{{Auth::user()->user_avatar}}">
                <p class="divider"></p>
              </div>
              <a class="prof-button right large btn-floating waves-effect waves-light theme dropdown-button-edit" data-activates="profile-dropdown"><i class="large material-icons">mode_edit</i></a>
              <div class="card-content frame">
                <span class="img-title theme-text">{{ ucwords(Auth::user()->real_name) }}</span><br/>
                @if(empty(Auth::user()->title))
                <p class="valign-wrapper"><i class="material-icons theme-text">recent_actors</i>&nbsp;<span class="teal-text">{{ env('DEF_TITLE') }}</span></p>
                @else
                <p class="valign-wrapper"><i class="material-icons theme-text">recent_actors</i>&nbsp;<span class="teal-text">{{ ucwords(Auth::user()->title) }}</span>               @endif
                <p class="valign-wrapper"><i class="material-icons theme-text">email</i>&nbsp;<span class="teal-text">{{Auth::user()->user_email}}</span></p>
                <p class="valign-wrapper"><i class="material-icons theme-text">my_location</i>&nbsp;<span class="teal-text">{{Auth::user()->tz}} Time</span></p>
                </div>
              <div class="clear-fixed frame">
                @if($stat['cmd'] == 'out')
                  <blockquote>Status:<span class="stat secondary-content chip red-text">Not sign in</span></blockquote>
                  <blockquote>Last signin:<span class="shift secondary-content chip theme-text">{{$stat['shift']}}</span></blockquote>
                @else
                  <blockquote>Status:<span class="stat secondary-content chip  theme-text">{{$stat['cmd']}}</span></blockquote>
                  <blockquote>Current Shift:<span class="shift secondary-content chip theme-text">{{$stat['shift']}}</span></blockquote>
                @endif
              </div>
              <div class="divider"></div>
              <section class="col s12 m12 l12 frame">
                <p class="theme-text flow-text qoute">{{ $qoute }}</p>
              </section>
            </div>
          </div>
          <div class="col s12 m6 l9 card card-tabs">
              <div class="row">
                  <div class="col s12">
                    <ul class="tabs">
                      <li class="tab col s3">
                        <a class="teal-text active" href="#teamtab"><i class="hide-on-large-only material-icons">supervisor_account</i> <span class="hide-on-med-and-down">TEAM & PROFILES</span></a>
                      </li>
                      <li class="tab col s3">
                        <a class="teal-text" href="#projecttab"><i class="hide-on-large-only material-icons">assignment_ind</i> <span class="hide-on-med-and-down">PROJECT & TASKS</span></a>
                      </li>
                      <li class="tab col s3">
                        <a class="teal-text" href="#reporttab"><i class="hide-on-large-only material-icons">contacts</i> <span class="hide-on-med-and-down">REPORTS ETC</span></a>
                      </li>
                    </ul>
                  </div>
                  <div id="teamtab" class="col s12 row">
                    <section class="col s12 m12 l6 tinfo">
                      <blockquote><span class="black-text">Team name</span><br><span class="theme-text">{{Auth::user()->getTeam()->team_name}}</span></blockquote>
                      <blockquote><span class="black-text">Team domain</span><br><span class="theme-text">{{Auth::user()->getTeam()->team_domain}}.slack.com</span></blockquote>
                      <blockquote><span class="black-text">Team timezone</span><br><span class="theme-text">{{Auth::user()->getTeam()->team_tz}}</span></blockquote>
                      <blockquote>
                        <span class="black-text">Team owner</span><br>
                        <span class="theme-text">
                          @foreach($team->getOwner() as $owner)
                            &#64;{{$owner->user_name}}<br/>
                          @endforeach
                        </span>
                      </blockquote>
                      <blockquote>
                        <span class="black-text">Team admins</span><br>
                        <span class="theme-text">
                          @foreach($team->getAdmins() as $admin)
                            &#64;{{$admin->user_name}}<br/>
                          @endforeach
                        </span>
                      </blockquote>
                    </section>
                    <section class="col s12 m12 l6 tmember">
                      <blockquote>
                        <span class="black-text">All members</span>&nbsp;&nbsp;<span class="chip blue-text">{{count($team->getAllMembers())}}</span><br>
                        <ul class="collection">
                          @foreach($team->getAllMembers() as $member)
                          <li class="collection-item avatar">
                            <img src="{{$member->user_avatar}}" alt="" height="20" class="circle">
                            <span class="title">{{$member->real_name}}</span>
                            <a href="#!" class="secondary-content"><span class="chip blue-text hide-on-small-only">&#64;{{$member->user_name}}</span></a>
                          </li>
                          @endforeach
                        </ul>
                      </blockquote>
                    </section>
                  </div>
                  <div id="projecttab" class="col s12 row">
                  <div class="col s6 timebar theme"><h5 class="indent white-text">Project</h5></div><div class="col s6 timebar theme right-align"><h5 class="indent white-text">Task</h5></div>
                  <hr>
                  <div class="clear-fixed">
                    @if(!empty($projects))
                      @foreach($projects as $proj)
                      <blockquote>
                          <span class="left-align">{{$proj->project_name}}</span>
                          <span class="secondary-content right-align">{!! $proj->task !!}</span>
                          <div class="divider full-width"></div>
                      </blockquote>
                      @endforeach
                    @endif
                    </div>
                  </div>
                  <div id="reporttab" class="col s12 flow-text">No report yet</div>
              </div>
        </div>
    </div>
</div>
@stop
@section('slidediv')
<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
<!-- Dropdown Structure -->
<ul id='profile-dropdown' class='dropdown-content'>
  <li><a href="#password-form" class="modal-trigger" id="changepass">Change Password</a></li>
  <li><a href="{{route('dashboard::updateuserinfo')}}">Update Profile</a></li>
</ul>
<!-- change pass modal -->
<div id="password-form" class="modal theme" style="padding:20px;">
    <form action="{{route('changepass')}}" method="post" id="form">
    <div class="modal-content" style="background:#fafafa;padding:20px!important;">
      <h4 class="theme-text">Change Password</h4>
      <div class="input-field">
        <i class="material-icons prefix teal-text">vpn_key</i>
        <input id="old" type="password" name="old" class="validate black-text" autofocus required >
        <label for="old" data-error="Invalid password">Current password</label>
      </div>
      <div class="input-field">
        <i class="material-icons prefix teal-text">lock</i>
        <input disabled="true" name="new" id="new" type="password" class="validate black-text" required autocomplete="false">
        <label for="new" data-error="Minimum of 6 characters">New password</label>
      </div>
      <div class="input-field">
        <i class="material-icons prefix teal-text">lock</i>
        <input disabled="true" id="confirm" type="password" class="validate black-text" required autocomplete="false">
        <label for="confirm" data-error="New password and confirm password not match!">Confirm password</label>
      </div>
      <input type="hidden" name="_token" value="{{csrf_token()}}">
    </div>
    <div class="modal-footer">
      <div class="full-width-on-small">
        <button class="cancel btn wave waves-effect gray lighten-1 theme-text modal-close" type="reset" id="cancel">Cancel</button>
        <button disabled="true" class="submit btn wave waves-effect lighten-2 theme white-text" type="submit" id="submit">Change</button>
      </div>
    </div>
  </form>
</div>
@stop
@section('js')
<script type="text/javascript">
$(window).load(function() {
  setTimeout(function() {
    Materialize.toast('<span>Hello {{Auth::user()->user_name}} !</span>', 1500,'theme white-text');
  }, 1500);
});
$(function(){
    setInterval(function(){
      $.ajax({
                 url: '{{route("dashboard::mytime",["user"=>Auth::user()->user_name,"team"=>Auth::user()->getTeamDomain()->team_domain])}}',
                 data: { _token:$("#_token").val() },
                 type: 'POST',
                 dataType: 'json',
                 success: function(response){
                    $('.stat').text(response.response['cmd']);
                    $('.shift').text(response.response['shift']);
                   // console.log(response.response);
                 },
                 error: function(e){
                   console.log(e.responseText);
                 }
               });
     },1000 * 60 * 1);
    $('.dropdown-button-edit').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: false, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });
    $('ul.tabs').tabs();
    $('.modal-trigger').leanModal();
    $("#old").keyup(function(){
        if($(this).val().length >= 6){
          $('#new').prop('disabled',false);
          $(this).css("border-bottom","1px solid #4CAF50");
        }else{
          $('#new').prop('disabled',true);
          $(this).css("border-bottom","1px solid #E61A05");
        }
    });
    $("#new").keyup(function(){
        if($(this).val().length >= 6){
          $('#confirm').prop('disabled',false);
          $(this).css("border-bottom","1px solid #4CAF50");
        }else{
          $('#confirm').prop('disabled',true);
          $(this).css("border-bottom","1px solid #E61A05");
        }
    });
    $("#confirm").keyup(function(){
        if($(this).val().length >= 6){
          $('#submit').prop('disabled',false);
          $(this).css("border-bottom","1px solid #4CAF50");
        }else{
          $('#submit').prop('disabled',true)
          $(this).css("border-bottom","1px solid #E61A05");
        }
    });
    $("#form").submit(function(event){
      if (($("#new").val() !== $("#confirm").val())) {
        Materialize.toast('Password not match',4000,'theme white-text');
        event.preventDefault();
      }else if($("#new").val().length < 6 || $("#old").val().length < 6) {
        Materialize.toast('Password should be at least 6 characters',4000,'theme white-text');
        event.preventDefault();
      }else if($("#new").val() === $("#old").val()) {
        Materialize.toast('New password should not be the same with your current password!',4000,'theme white-text');
        event.preventDefault();
      }
    });
    $("#changepass").click(function(){
        $("#password-form").show('fast');
    });
    $("#cancel").click(function(){
        $("#password-form").hide('fast');
    });
});
</script>
@stop