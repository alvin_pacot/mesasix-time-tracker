@extends('master.homelayout')

@section('main-nav')
	@if(!Auth::check())
	 	@include('partial.main-nav-guest')
	@else
		@include('partial.main-nav-auth')
	@endif
@stop

@section('css')
<style type="text/css">	
	.inner-wrapper{ padding-left: 0px; }
	.icon-block { padding: 0 15px; }
	.icon-block .material-icons { font-size: inherit;}
	.description{padding: 2% 15%;}
	.bg-gradient{
	    background: rgba(235,233,249,1);
	    background: -moz-linear-gradient(-45deg, rgba(235,233,249,1) 0%, rgba(193,191,234,1) 43%, rgba(216,208,239,1) 50%, rgba(206,199,236,1) 83%, rgba(206,199,236,1) 100%);
	    background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(235,233,249,1)), color-stop(43%, rgba(193,191,234,1)), color-stop(50%, rgba(216,208,239,1)), color-stop(83%, rgba(206,199,236,1)), color-stop(100%, rgba(206,199,236,1)));
	    background: -webkit-linear-gradient(-45deg, rgba(235,233,249,1) 0%, rgba(193,191,234,1) 43%, rgba(216,208,239,1) 50%, rgba(206,199,236,1) 83%, rgba(206,199,236,1) 100%);
	    background: -o-linear-gradient(-45deg, rgba(235,233,249,1) 0%, rgba(193,191,234,1) 43%, rgba(216,208,239,1) 50%, rgba(206,199,236,1) 83%, rgba(206,199,236,1) 100%);
	    background: -ms-linear-gradient(-45deg, rgba(235,233,249,1) 0%, rgba(193,191,234,1) 43%, rgba(216,208,239,1) 50%, rgba(206,199,236,1) 83%, rgba(206,199,236,1) 100%);
	    background: linear-gradient(135deg, rgba(235,233,249,1) 0%, rgba(193,191,234,1) 43%, rgba(216,208,239,1) 50%, rgba(206,199,236,1) 83%, rgba(206,199,236,1) 100%);
	    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebe9f9', endColorstr='#cec7ec', GradientType=1 );
	}
</style>
@stop

@section('content')
<div class="container-center">
		<div class="row clear-fixed">
			<img class="home-img circle" src="{{ asset('/appimg/mesasix-black.jpg') }}">
			<img class="home-img" src="{{ asset('/appimg/arrows.png') }}">
			<img class="home-img circle white" src="{{ asset('/appimg/slack.png' ) }}">
		</div>
		<div class="row hgroup">
			<h3>MESASIX AND SLACK</h3>
			<h4>COLLABORATION</4>
		</div>
		<div>
			<a href="{{ route('teamRegStart')}}" class="btn btn-large theme waves-effect waves-light">REGISTER YOUR TEAM TODAY!</a>
		</div>
		<div class="description">
			<p>Don't have <strong class="theme-text">Mesasix Time Tracker</strong> yet in your team?<br/>
    			If you are team admin or team owner, then sign up for your team<br/>
    			We will then help you out on setting up the slack integration.</p>
		</div>
		<div class="">
			<div class="description">
			<h5 class="center">What is <a href="#"><strong class="theme-text">{{env('APP_NAME')}}</strong></a>?</h5>
			<p>{{env('APP_NAME')}} is a team collaboration tool for virtual team over the the internet. The app is capable of tracking and monitoring working time, breaks and overtime of its members as well as saving activities and todo list and many more!.</p>
			</div>
		</div>
</div>
<div class="container">
    <div class="section">
      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h1 class="center theme-text"><i class="material-icons">group</i></h1>
            <h5 class="center">Manage and Monitor Team Projects</h5>
            <p class="light">{{env('APP_NAME')}} manages and monitor the timeline of your projects as well sending notification to all assigned members.</p>
          </div>
        </div>
        <div class="col s12 m4">
          <div class="icon-block">
            <h1 class="center theme-text"><i class="material-icons">schedule</i></h1>
            <h5 class="center">Manage and Monitor Employee Timesheets</h5>
            <p class="light">We have provided detailed reports from the inputs of your employees and present them in a way you and them will understand.</p>
          </div>
        </div>
        <div class="col s12 m4">
          <div class="icon-block">
            <h1 class="center theme-text"><i class="material-icons">swap_horiz</i></h1>
            <h5 class="center">Slack Integration and Notifications</h5>
            <p class="light">{{env('APP_NAME')}} is dependent and connected to Slack and with this feature, sending notifications to Slack team channel or to members will be just a click on a button.</p>
          </div>
        </div>
      </div>
	</div>
</div>
@stop

@section('footer')
	@include('partial.footer')
@stop