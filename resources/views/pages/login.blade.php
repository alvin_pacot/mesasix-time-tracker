@extends('master.homelayout')

@include('partial.main-nav-guest')

@section('css')
<style type="text/css"> 
  .inner-wrapper{padding-left: 0px;}
  .container{ padding-top: 20px;}
  .bg-gradient{ background: inherit;}
</style>
@stop

@section('content') 
<div class="container">
@if(!empty($success))
  <div class="row">
    <form action="{{route('login::password')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="col s12 m12 l6" style="padding: 30px !important;border-right:0px !important;">
      <h5>Select your team</h5>
      <hr/>
      @foreach($teams as $team)
        <p>
          <input class="with-gap" value="{{$team->team_id}}" checked name="myteam" type="radio" id="{{$team->team_id}}"  />
          <label for="{{$team->team_id}}" title="{{$team->team_domain}}.slack.com">{{$team->team_name}}</label>
        </p>
      @endforeach
      </ul>
    </div>
    <div class="col s12 m12 l6 " style="padding: 30px !important;border-right:0px !important;">
      <h5>Enter your password</h5>
      <hr/>  
        <label>Email <a href="{{route('login::login')}}">Change email</a></label>
        <input type="email" readonly name="myemail" value="{{ $email or ''}}" required>
        <label>Enter password</label>
        <input type="password" name="mypass" required autofocus>
        <button type="submit" class=" full-width-on-small waves-effect waves-light btn btn-large theme right" name="signin">LOGIN</button>
    </div>
    </form>
</div>
@else
  <div class="row">
    <div class="col s12 m12 l6 card-padded" >
    <h5>Login with Mesasix account</h5>
      <form action="{{route('login::email')}}" method="get">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <label>Email:</label>
        <input type="email" placeholder="Email address"  value="{{$email or ''}}" required name="email" autofocus>
        <button type="submit" class="full-width-on-small waves-effect waves-light btn btn-large theme right" name="signin">NEXT</button>
      </form>    
    </div>
      <div class="col s12 m12 l6 card-padded">
        <h5>Login with Slack account</h5>
        <section>
          Don't have <strong class="theme-text">Mesasix Time Tracker</strong> account yet? Sign in with your Slack account. <strong class="theme-text">Mesasix Time Tracker</strong> will authenticate user from their <a href="https://slack.com">Slack</a> account using Slack OAuth.
        </section>
        <a class="waves-effect waves-light btn btn-large theme right full-width-on-small " style="margin-top: 16px;" href="{{ route('slack_login',['action'=>'login']) }}">Login with Slack</a>
      </div>
    </div>
@endif
</div>
@stop
