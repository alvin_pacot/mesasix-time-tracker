@extends('master.admindashboardlayout')
@section('page-title') Monthly Records @endsection
@section('css')
    <!-- progressbar -->
    <link href="{{ asset("/appcss/progress-bar.css") }}" rel="stylesheet" type="text/css" />
    <!-- jquery-ui theme css -->
    <link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/jquery-ui/themes/overcast/jquery-ui.min.css") }}">
    <style type="text/css">
    #nextweek{ cursor:pointer; float: right !important;}
    .week-label{ padding: 0 10px !important; position: relative;bottom: 7px;}
    .week-group{ padding-top: 5px !important;}
    .button-group{ height:36px;padding: 5px 0px;}
    .ui-datepicker-calendar { display: none;}
    .ui-datepicker-month{ display: inline-block;height: 34px;background: #F5F5F0; color: black;margin-right: 5px !important;}
    .ui-datepicker-year{ display: inline-block;height: 34px;background: #F5F5F0; color: black;}
    .select-dropdown{border-bottom: transparent !important;}
    .icons{ width: 180px; border-radius: 2px;}
    input.select-dropdown{
      color: white;
      text-transform: uppercase;
      line-height: normal !important;
      text-align: center;
      height: 36px !important;
    }
    .caret{ 
      color: white !important;
      font-weight: bolder;
      top: 11px !important;
      right: 10px !important;
    }
    </style>
@stop

@section('content')
<div class="row button-group">
  <div class="col s12 m6 l2" style="height:36px !important;max-width: 185px;">
    <select class="icons teal lighten-2 full-width-on-small" name="user_id">
        <option value="{{Auth::user()->user_id}}" disabled selected>Select User</option>
      @foreach($members as $member)
        @if($member['user_id'] == $selecteduser)
        <option value="{{$member['user_id']}}" selected data-icon="{{$member['avatar']}}" class="circle">{{$member['user_name']}}</option>
        @else
        <option value="{{$member['user_id']}}" data-icon="{{$member['avatar']}}" class="circle">{{$member['user_name']}}</option>
        @endif
      @endforeach
    </select>
  </div>
  <div class="col s12 m3 l7 hide-on-med-and-down"><!-- tab-group -->
    <a href="{{ route('dashboard::theyrecords',['id'=>Auth::user()->user_id,'week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="full-width-on-small btn-flat teal white-text wave waves-effect lighten-2">Weekly</a>
    <a href="{{ route('dashboard::theymonthly',['id'=>Auth::user()->user_id,'month'=>date('n'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="full-width-on-small btn-flat teal yellow-text wave waves-effect lighten-2 ">Monthly</a>
  </div>
  <div class="col s12 m3 l3 row week-group"><!-- right row week-group -->
     <a class="prevweek col s1" href="{{ route('dashboard::theymonthly',['team'=>auth()->user()->getTeamDomain()->team_domain,'user'=>auth()->user()->user_name ])}}/{{ App\Http\Controllers\MyMonthlyController:: getPrevUrl($year,$month)}}/{{$selecteduser}}">
        <img id="prevweeks" src="{{ asset('/appimg/teal-arrow-left.png') }}" alt="prev month"/>
    </a>
      <input class="datepicker" type="hidden">
      <button class="col s10 week-label blue-text btn-flat">{{ $label or date('F Y') }}</button>
      <a class="nxtweek col s1" id="nxtwk" href="{{ route('dashboard::theymonthly',['team'=>auth()->user()->getTeamDomain()->team_domain,'user'=>auth()->user()->user_name ])}}/{{ App\Http\Controllers\MyMonthlyController:: getNextUrl($year,$month)}}/{{$selecteduser}}">
        <img id="nextweek" src="{{ asset('/appimg/teal-arrow-right.png') }}" alt="next month"/>
      </a>
  </div>
    <input id="y" type="hidden" value="{{$year}}">
    <input id="m" type="hidden" value="{{$month}}">
    <input type="hidden" id="url" value="{{ route('dashboard::theymonthly',['team'=>auth()->user()->getTeamDomain()->team_domain,'user'=>auth()->user()->user_name ]) }}">
</div>
<!-- <div class="row"> -->
      <div class="timebar theme row">
        <div class="row white-text">
          <div class="col s5 m3 l3 head-title" style="text-indent:30px;">Day</div>
          <div class="col s7 m2 l2 head-title">Total Worked Time</div>
          <div class="col s12 m7 l7 head-title hide-on-small-only" style="text-align:center;">Activities</div>
        </div>
      </div>
      @if(!empty($data))
      <ul class="row collapsible" data-collapsible="expandable">
        @foreach($data as $key => $day)
        <li>
          <div class="row collapsible-header gray expand-header record-expand-header">
            <div class="col s12 l6">
              <div class="row">
                <div class="col s5 l6">
                    <strong style="position:relative;top:11px;left:15px;">{{ $day['day'] }}</strong>
                </div>
                <div class="col s7 l6">
                  <strong style="position: relative;top: 13px;">{{ $day['worktime'] }}</strong><br/>
                </div>
              </div>
            </div>
            <div class="col s12 l6 row">
                <div class="progressed progress-large progress-stacked activity-bar progressed-stripe progress-animate" style="height: 55px;position:relative;top:2px;">
                  @foreach($day['progressbar'] as $key => $bar)
                    <span style="width:{{$bar[0]}}%" title="{{$bar[2]}}" class="{{ $bar[1] }}"></span>
                  @endforeach
                </div>
              
            </div>
          </div>
          <div class="collapsible-body expand-body row">
            <div class="col s12 m6 l3">
            <h6>Summary</h6>
              <table class="compressed-table">
                <tr><td><i class="material-icons green-text">schedule</i><span> Worktime</span></td><td>{{ $day['worktime']}}</td></tr>
                <tr><td><i class="material-icons blue-text">schedule</i><span> Breaktime</span></td><td>{{ $day['breaktime']}}</td></tr>
                <tr><td><i class="material-icons overtime-text">schedule</i><span> Overtime</span></td><td>{{ $day['extendedtime']}}</td></tr>
              </table>
            </div>
            <div class="col s12 m6 l3">
              <h6>Breakdown <span class="theme-text">( {{Auth::user()->tz}} )</span></h6>
              @if(!empty($day['breakdown']))
                <table class="compressed-table">
                  @foreach($day['breakdown'] as $brk)
                    <tr><td>{{ $brk[0] }}</td><td>{{ $brk[1] }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
            <div class="col s12 m12 l3">
              <h6>Activity Reports</h6>
              @if(!empty($day['activity']))
                <table class="compressed-table">
                  @foreach($day['activity'] as $act)
                    <tr><td><i class="tiny material-icons">done</i> {{ $act }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
            <div class="col s12 m12 l3">
              <h6>To Do</h6>
              @if(!empty($day['todo']))
                <table class="compressed-table">
                  @foreach( $day['todo'] as $tod )
                    <tr><td><i class="tiny material-icons">assignment</i> {{ $tod }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
          </div>
        </li>
        @endforeach
      </ul>
      <div class="row theme white-text total-accu">
        <div class="col s12 m5 l3" style="text-indent:15px;">Total Monthly Working Time : </div>
        <div class="col s12 m7 l9 totals">{{$total_monthly}}</div>
      </div>
      @endif
@stop
@section('slidediv')
<div class="clear-fixed fixed-action-btn hide-on-large-only" style="bottom: 40px; right: 25px;">
    <a class="btn-floating btn-large red">
      <i class="large mdi-navigation-menu">mode_edit</i>
    </a>
    <ul>
      <li><a href="{{ route('dashboard::theyrecords',['id'=>Auth::user()->user_id,'week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="btn-floating black"><b>W</b></a></li>
      <li><a href="{{ route('dashboard::theymonthly',['id'=>Auth::user()->user_id,'month'=>date('n'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="btn-floating blue"><b>M</b></a></li>
    </ul>
  </div>
@stop
@section('js')
<script type="text/javascript" src="{{ asset ("/bower_components/jquery-ui/jquery-ui.min.js")  }}"></script>
<script type="text/javascript">
$(function() {
    $('select').material_select();
    $('select').change(function () {
      var y = $('#y').val();
      var m = $('#m').val();
      window.location = $('#url').val()+"/"+y+"/"+m+"/"+$(this).val();
    });
  $('.week-label').click(function(){
      $('.datepicker').datepicker('show');  
  });
  $('.nxtweek').click(function(event){
      var href = $(this).attr('href');
      // var mn = href.substr(href.lastIndexOf('/') + 1);
      var mn = href.split('/')[10];
      if (mn > (new Date()).getMonth()+1) {
        Materialize.toast('You can`t select month higher than the current month.',1000,'theme white-text');
        event.preventDefault();
      }
  });
  var year = (new Date).getFullYear();
  var month = (new Date).getMonth();
   $('.datepicker').datepicker({
        dateFormat: "yy/mm",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        maxDate: new Date(year, 11, 31),
        onClose: function() {
          function isDonePressed(){
            return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
          }
          if (isDonePressed()){
            var smonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var syear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
             $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
             $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
            if (syear == year) {
              if (smonth > month) {
                Materialize.toast('You can`t select a month higher than the current month',1000,'theme white-text');
              }else{
                window.location = $('#url').val()+"/"+syear+"/"+(parseInt(smonth)+1)+"/"+$('select').val();
              }
            }else{
              window.location = $('#url').val()+"/"+syear+"/"+(parseInt(smonth)+1)+"/"+$('select').val();
            }
          }                 
         },
        beforeShow: function() {
         if ((selDate = $(this).val()).length > 0){
            iYear = selDate.substring(selDate.length - 4, selDate.length);
            iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5),
                     $(this).datepicker('option', 'monthNames'));
            $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
          }
        }
    });/*end date[icker*/
  $(document.documentElement).keyup(function (e) {
    if (e.keyCode == 39){
      var href = $('.nxtweek').attr('href');
      // var mn = href.substr(href.lastIndexOf('/') + 1);
      var mn = href.split('/')[10];
      if (mn > (new Date()).getMonth()+1) {
        Materialize.toast('You can`t select month higher than the current month.',1000,'theme white-text');
        event.preventDefault();
      }else{
        window.location = href;
      }
    }
    if (e.keyCode == 37){
      window.location = $('.prevweek').attr('href');
    } 
  });
});/*end doc ready*/
</script>
@stop
