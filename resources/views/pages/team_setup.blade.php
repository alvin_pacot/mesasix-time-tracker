@extends('master.homelayout')

@section('main-nav')
	@if(!Auth::check())
	 	@include('partial.main-nav-guest')
	@else
		@include('partial.main-nav-auth')
	@endif
@stop

@section('css')
<style type="text/css">	
	.inner-wrapper{ padding-left: 0px;}
	.btn-padded{
		padding: 3px 10px!important;
    	font-size: 21px;
    	position: relative;
    	top: -5px;
	}
	.bg-gradient{ background: inherit;}
</style>
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="row">
			<div class="col s12 m7 l7 card-padded">
			@if(!empty($stage))
				<h5>Setup <a class="btn btn-padded" href="http://my.slack.com/services/new/outgoing-webhook" target="_blank" id="out">Outgoing Webhook</a> Integration</h5>
				<ul>
					<li>1.For <b>Channel</b>, select a dedicated public channel (this channel will be used for sign in,out,brb,back).</li>
					<li>2.For <b>Trigger Word(s),</b> put this <i class="theme-text"> in, out, brb, back, ++, -- </i></li>
					<li>3.For <b>URL(s)</b>, put this <i class="theme-text">{{env('APP_HANDLER')}}</i></li>
					<li>4.The rest fields are optional.</li>
					<li>5.Save the setting.</li>
				</ul>
				<h5>Setup <a class="btn btn-padded" href="http://my.slack.com/services/new/slash-commands" target="_blank" id="slash">Slash Command</a> Integration</h5>
				<ul>
					<li>1.For <b>Command</b>, put this <i class="theme-text"> /m6 </i></li>
					<li>2.For <b>URL(s)</b>, put this <i class="theme-text">{{env('APP_HANDLER')}}</i></li>
					<li>3.For <b>Method,</b> select <i class="theme-text">POST</i></li>
					<li>4.For <b>Autocomplete help text</b>, Just check the checkbox for the hints (optional).</li>
					<li>5.Save the integration.</li>
				</ul>
			@else
				<h5>Setup Requirements</h5>
				<ul>
					<li><blockquote>The one who setup should be an <b>admin</b> or <b>owner</b> of the team.</blockquote></li>
					<li><blockquote>Authorize <strong class="theme-text">{{env('APP_NAME')}}</strong> to post, act as a client and setup incoming webhook for the team.</blockquote></li>
				</ul>
				<h5>Capabilities and Limitations</h5>
				<ul>
					<li><blockquote>Only user without restriction (guests will excluded), non-bot user and non-deleted user will be added.</blockquote></li>
					<li><blockquote>Automatic creation of member account, user will be automatically notified regarding the registration.</blockquote></li>
					<li><blockquote>Currently only one channel are supported, but can be modify or change it later.</blockquote></li>
					<li><blockquote>Team should setup <a href="http://my.slack.com/services/new/slash-commands" target="_blank">Slash Command</a> and <a href="http://my.slack.com/services/new/outgoing-webhook" target="_blank">Outgoing Webhook</a> integration in order to fully utilize the time tracker.</blockquote></li>
				</ul>
			@endif
			</div>
			<div class="col s12 m5 l5 card-padded">
				<h5>Steps</h5>
				<ul>
					<li><i class="material-icons small theme-text">done</i>Step 1. User Authentication</li>
					<li><i class="material-icons small theme-text">done</i>Step 2. Setup Incoming Webhook Integration</li>
					@if(!empty($stage))
					<li><i class="material-icons small theme-text">library_add</i>Step 3. Setup <a href="http://my.slack.com/services/new/slash-commands" target="_blank" id="slash">Slash Command</a> Integration</li>
					<li><i class="material-icons small theme-text">library_add</i>Step 4. Setup <a  href="http://my.slack.com/services/new/outgoing-webhook" target="_blank" id="out">Outgoing Webhook</a> Integration</li>
					@else
					<li>Step 3. Setup Slash Command Integration</li>
					<li>Step 4. Setup Outgoing Webhook Integration</li>
					@endif
				</ul>
				@if(!empty($stage))
					<h5>Reminders</h5>
					<p>Make sure to setup properly the Slash Command and Outgoing Webhook. Otherwise the time tracker services will not work.</p>
					<p>Notice: Every non restricted member of your team will automatically registered to {{env('APP_NAME')}} and has already sent their account information via slackbot.</p>
					<a href="{{ route('home',['success'=>true]) }}" class="btn btn-large waves-effect wave-light theme right disabled not-active" id="finish">Finish</a>
				@else
					<a href="{{ route('slack_login',['action'=>'registration']) }}" class="btn btn-large waves-effect wave-light theme right">Proceed</a>
				@endif
			</div>
		</div>
	</div>
</div>
@stop

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	
    var clickout = false;
    //uncomment all if slash automatic setup in server is not capable
    // var clickslash = false;
    $('#out').click(function(){
    	window.clickout = true;
        if(window.clickout/* && window.clickslash*/){
            $('#finish').removeClass('disabled');
            $('#finish').removeClass('not-active');
        }
    });

/*    $('#slash').click(function(){
    	window.clickslash = true;
        if(window.clickout && window.clickslash){
            $('#finish').removeClass('disabled');
            $('#finish').removeClass('not-active');
        }
    });*/
});
</script>
@stop