@extends('master.admindashboardlayout')

@section('page-title')
    Team Settings
@endsection

@section('css')
    <!-- progressbar -->
    <link href="{{ asset("/appcss/progress-bar.css") }}" rel="stylesheet" type="text/css" />
    <!-- jquery-ui datepicker css -->
    <style type="text/css">
       .padding{ padding: 20px;}
       .margined{ margin: 5px 10px;}
       .text-style{ color: #6A2C91;text-align: center;font-weight: bold;}
       .collection{ border: none !important;}
       .setting-channel{ position: relative;top: -70px;}
       .setting-switch { position: relative;top: -75px;}
       .setting-timezone { position: relative;top: -75px;}
       .setting-config { position: relative;top: -75px;}
    </style>
@stop

@section('content')
<div class="card padding margined">
	<div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s4 ">
        	<a class="theme-text {{($tab == 1)? 'active':''}}" href="#info"><i class="hide-on-med-and-up small theme-text material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Info">group_work</i><span class="hide-on-small-only">Info</span></a>
        </li>
        <li class="tab col s4">
        	<a class="theme-text {{($tab == 2)? 'active':''}}" href="#member"><i class="hide-on-med-and-up small theme-text material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Members" >recent_actors</i><span class="hide-on-small-only">Members</span></a>
        </li>
        <li class="tab col s4">
        	<a class="theme-text {{($tab == 3)? 'active' :''}}" href="#setting"><i class="hide-on-med-and-up small theme-text material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Setting">settings</i><span class="hide-on-small-only">Setting</span></a>
        </li>
      </ul>
    </div>
    <div id="info" class="col s12">
      <ul class="collection with-header info">
      @if(!empty($teamInfo))
        <li class="collection-header theme-text"><h5>Team Information</h5></li>
        <li class="collection-item"><div class="setting-content"><span class="ilabel">Team Name</span><span class="secondary-content">{{$teamInfo->team_name}}</span></div></li>
        <li class="collection-item"><div class="setting-content"><span class="ilabel">Team Domain</span><span class="secondary-content">{{$teamInfo->team_domain}}.slack.com</span></div></li>
        <li class="collection-item"><div class="setting-content"><span class="ilabel">Registered on</span><span class="secondary-content">{{(new DateTime($teamInfo->created_at))->format('F j, Y g:i A e')}}</span></div></li>
        @endif
      </ul>     
      <a href="{{route('dashboard::teamupdateinfo')}}" class="full-width-on-small btn-flat secondary-content wave waves-effect theme lighten-4 white-text tooltipped" data-position="left" data-delay="50" data-tooltip="If the above information is not correct. Click here to update.">Update Team Info From Slack</a>
    </div>
    <div id="member" class="col s12">
    <ul class="collection with-header member">
    @if(!empty($teamMembers))
    	<li class="theme-text collection-header"><h5>Team Members</h5></li>
    	@foreach($teamMembers as $user)
	    <li class="collection-item avatar">
	      <img src="{{$user->user_avatar}}" alt="" class="circle">
	      <span class="title">{{ucwords($user->real_name)}}</span>
	      <p><a>&#64;{{$user->user_name}}</a><br/>{{$user->title}}<br/>{{$user->user_email}}</p>
	      <div class="secondary-content black-text" style="text-align:right;">
	      	{{$user->tz}}<br/>
	      	@if($user->is_admin)
	      		Team Admin<br/>
	      	@endif
	      	@if($user->is_owner)
	      		Team Owner<br/>
	      	@endif
	      	@if(auth()->user()->is_owner)
	      		@if($user->trashed())
	      			<a href="#" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Enable this user to access the app" onclick="setEnableId('{{$user->user_id}}','{{$user->user_name}}')">Enable</a>
	      		@elseif(!$user->trashed() && auth()->user()->user_id !== $user->user_id)
	      			<a href="#" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Disable this user from accessing the app" onclick="setDisableId('{{$user->user_id}}','{{$user->user_name}}')">Disable</a>
	      		@endif
	      	@endif
	      </div>
	    </li>
	    @endforeach
	@endif
  	</ul>
    </div>
    <div id="setting" class="col s12">
      <form action="{{route('dashboard::teamupdatesetting')}}" method="POST">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <ul class="collection with-header admin">
      @if(!empty($teamInfo))
        <li class="collection-header theme-text"><h5>Admin Setting</h5></li>
        <li class="collection-item">
        	<div class="setting-content">
        	<span class="theme-text ilabel">Slack Notification</span>
        	<p>Whether the notification is allowed during max of 16 hours and extension after 8 hours of work during shift</p>
        	<div class="secondary-content setting-switch">
	        	<div class="switch">
	        	@if($teamInfo->notify==1)
	        		<label>Off<input  checked type="checkbox" id="notify" name="notified"><span class="lever"></span>On</label>
	        	@else
	        		<label>Off<input  type="checkbox" id="notify" name="notified"><span class="lever"></span>On</label>
	        	@endif
	  			</div>
        	</div>
        	</div>
        </li>
        <li class="collection-item">
        	<div  class="setting-content">
        		<span class="theme-text ilabel">Default Channel</span>
        		<p>This channel is used when using incoming webhook. ( Overriding this channel is not yet supported from Slack )</p>
	        	<div class="secondary-content setting-channel">{{$teamInfo->channel}}</strong>
        	</div>
        </li>
        <li class="collection-item">
        	<div  class="setting-content">
        		<span class="theme-text ilabel">Default Timezone</span>
        		<p>This will be the default team timezone (This will be used if the user timezone is absent)</p>
        		<div class="secondary-content setting-timezone  full-width-on-small">
        			<select class="browser-default cursor" name="tz" id="tz">
			          @if(!empty($zones))
			            @foreach($zones as $zone)
			            	@if(strtolower($zone->zone_name) == strtolower($teamInfo->team_tz))
			            		<option selected value="{{$zone->zone_name}}">{{$zone->zone_name}}</option>	
			            	@else
			              	<option value="{{$zone->zone_name}}">{{$zone->zone_name}}</option>	
			            	@endif
			            @endforeach
			          @endif
			        </select>
        		</div>
        	</div>
        </li>
        <li class="collection-item">
        	<div  class="setting-content">
        		<span class="theme-text ilabel">Slack Configuration</span>
        		<p>This is the configuration of {{env('APP_NAME')}} in your team.</p>
        		<div class="secondary-content setting-config">
        			<p class="truncate"><a href="{{$teamInfo->config_url}}" target="_blank">{{$teamInfo->config_url}}</a></p>
        		</div>
        	</div>
        </li>
        @endif
      </ul>
      @if(Auth::user()->isOwner())
  		<button type="submit" class="full-width-on-small btn-flat secondary-content wave waves-effect theme lighten-4 white-text tooltipped" data-position="left" data-delay="50" data-tooltip="If you made a change above. Click here to update.">Update Team Setting</button>
      @else
      <button type="button" class="disabled full-width-on-small btn-flat secondary-content theme lighten-4 white-text tooltipped" data-position="left" data-delay="50" data-tooltip="Only team owner can update the above setting.">Update Team Setting</button>
      @endif
  	</form>
    </div>
  </div>
</div>
@stop

@section('slidediv')
  <!--Disabling Modal Structure -->
  <div id="disable" class="modal theme" style="padding:15px">
    <form action="{{route('dashboard::teamupdateuserdisable',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content" style="background:#fafafa;">
      <h4 class="theme-text">Disabled Account</h4>
      <p>Are you sure you want to disable <span id="username1"></span> account?<br/>This won't affect his/her Slack team account.</p>
      <input type="hidden" value="" id="disableuserid" name="user_id">
    </div>
    <div class="modal-footer">
      <button type="reset" class="wave waves-effect lighten-4 grey theme-text btn btn-small modal-close">CANCEL</button>
      <button type="submit" class="disable btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">YES</a>
    </div>
    </form>
  </div>
  <!--Enabling Modal Structure -->
  <div id="enable" class="modal theme" style="padding:15px">
    <form action="{{route('dashboard::teamupdateuserenable',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content" style="background:#fafafa;">
      <h4 class="theme-text">Enable Account</h4>
      <p>Are you sure you want to enable <span id="username2"></span> account?<br/>Make sure to enable back the account in Slack team</p>
      <input type="hidden" value="" id="enableuserid" name="user_id">
    </div>
    <div class="modal-footer">
      <button type="reset" class="wave waves-effect lighten-4 grey theme-text btn btn-small modal-close">CANCEL</button>
      <button type="submit" class="enable btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">YES</a>
    </div>
    </form>
  </div>
@stop

@section('js')
<script type="text/javascript">
$(document).ready(function(){
  $('select').material_select();
  $('ul.tabs').tabs();
});
function isChanged() {
  var x = document.getElementById('notify').checked;
  if (x) {
  	$('#notify').prop('checked',false);	
  }
  	$('#notify').prop('checked',true);
}
function setEnableId(id,name) {
  $('#username2').text(name);
  $('#enableuserid').val(id);
  $('#enable').openModal();
}
function setDisableId(id,name) {
  $('#username1').text(name);
  $('#disableuserid').val(id);
  $('#disable').openModal();
}
</script>
@stop
