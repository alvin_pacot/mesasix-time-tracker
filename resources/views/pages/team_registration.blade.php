@extends('master.homelayout')

@section('main-nav')
	@if(!Auth::check())
	 	@include('partial.main-nav-guest')
	@else
		@include('partial.main-nav-auth')
	@endif
@stop

@section('css')
<style type="text/css">	
	.inner-wrapper{padding-left: 0px;}
	.bg-gradient{ background: inherit;}
</style>
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="row">
			<div class="col s12 m7 l7 card-padded">
				<h5>Setup Requirements</h5>
				<ul>
					<li><blockquote>The one who setup should be an <b>admin</b> or <b>owner</b> of the team.</blockquote></li>
					<li><blockquote>Authorize <strong class="theme-text">Mesasix Time Tracker</strong> to post, act as a client and setup incoming webhook for the team.</blockquote></li>
				</ul>
				<h5>Capabilities and Limitations</h5>
				<ul>
					<li><blockquote>Only user without restriction (guests will excluded), non-bot user and non-deleted user will be added.</blockquote></li>
					<li><blockquote>Automatic creation of member account, user will be automatically notified regarding the registration.</blockquote></li>
					<li><blockquote>Currently only one channel are supported, but can be modify or change it later.</blockquote></li>
					<li><blockquote>Team should setup <a href="http://my.slack.com/services/new/slash-commands">Slash Command</a> and <a href="http://my.slack.com/services/new/outgoing-webhook">Outgoing Webhook</a> integration in order to fully utilize the time tracker.</blockquote></li>
				</ul>
			</div>
			<div class="col s12 m5 l5 card-padded">
				<h5>Steps</h5>
				<ul>
					<li><i class="material-icons small theme-text">done</i>Step 1. User Authentication</li>
					<li>Step 2. Setup Incoming Webhook Integration</li>
					<li>Step 3. Setup Outgoing Webhook Integration</li>
					<li>Step 4. Setup Slash Command Integration</li>
				</ul>
				<form action="{{route('teamRegistration')}}" method="post">
					<input type="hidden" name="_token" required value="{{csrf_token()}}">
					<input type="hidden" name="team_id" value="{{ $teamId or '' }}">
					<button type="submit" class="full-width-on-small btn btn-large waves-effect wave-light theme">PROCEED</button>
					<a class="full-width-on-small btn btn-large red waves-effect wave-light" href="{{route('home')}}">CANCEL</a>
				</form>
			</div>
		</div>
	</div>
</div>
@stop
