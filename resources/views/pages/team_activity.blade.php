@extends('master.admindashboardlayout')

@section('page-title') Team Daily Records @endsection

@section('css')
    <!-- progressbar -->
    <link href="{{ asset("/appcss/progress-bar.css") }}" rel="stylesheet" type="text/css" />
    <!-- jquery-ui datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/jquery-ui/themes/flick/jquery-ui.min.css") }}">
    <style type="text/css">
      .next-button{cursor:pointer; float: right;}
      .datepicker-button{position: relative;bottom: 7px;}
      .daily-group{ height:36px;padding: 5px 0px;width: 300px;}
      .prevbut,.nxtbut{ cursor: pointer;}
      .name{font-size: 2.2vh;}
    </style>
@stop

@section('content')
    <div class="row">
      <div class="right daily-group">
        <input type="hidden" id="datepicker">
        <a class="prevbut col s1">
          <img class="prev-button" onclick="return prevday()" src="{{ asset('/appimg/teal-arrow-left.png') }}" alt="prev day"/>
        </a>
        <button class="col s10 theme-text btn-flat wave waves-effect datepicker-button">{{ $date or date('l F, j Y',time())}}</button>
        <a class="nxtbut col s1 ">
          <img class="next-button" src="{{ asset('/appimg/teal-arrow-right.png') }}" onclick="return nextday()" alt="next day"/>
        </a>
        <input type="hidden" id="current" value="{{ $current or date('Y-m-d',time()) }}">
        <input type="hidden" id="url" value="{{route('dashboard::teamactivity',['team'=>auth()->user()->getTeamDomain()->team_domain])}}">
        </div>
    </div>
      <div class="timebar my-timebar theme row">
        <div class="row white-text">
          <div class="col s12 m6 l3 head-title" style="text-indent:30px;">Name</div>
          <div class="col s12 m6 l2 head-title hide-on-small-only">Total Worked Time</div>
          <div class="col s12 m12 l7 head-title hide-on-med-and-down" style="text-align:center;">Activities</div>
        </div>
      </div>
      @if(!empty($data))
      <ul class="row collapsible" data-collapsible="expandable">
        @foreach($data as $key => $user)
        <li>
          <div class="row collapsible-header gray expand-header">
            <div class="col s12 m6 l6">
              <div class="row name">
                <div class="col s12 m12 l6 row">
                  <div class="col s5 pricks">
                    <img src="{{ $user['avatar'] }}" class="circle responsive-img" width="40">
                  </div>
                  <div class="col s7 position">
                    <strong>{{ $user['name'] }}</strong><br/>
                    <label>{{ $user['title'] }}</label>
                  </div>
                </div>
                <div class="col l6 hide-on-med-and-down">
                  <strong style="position: relative;top: 13px;">{{ $user['worktime'] }}</strong><br/>
                </div>
              </div>
            </div>
            <div class="col s12 m6 l6 row ">
                <div class="activity-bar progressed progress-large progress-stacked progressed-stripe progress-animate" style="height: 55px;position:relative;top:2px;">
                  @foreach($user['progressbar'] as $key => $bar)
                    <span style="width:{{$bar[0]}}%" title="{{$bar[2]}}" class="{{ $bar[1] }}"></span>
                  @endforeach
                </div>
            </div>
          </div>
          <div class="collapsible-body expand-body row">
            <div class="col s12 m6 l3">
            <h6>Summary</h6>
              <table class="compressed-table">
                <tr><td><i class="material-icons green-text">schedule</i><span> Worktime</span></td><td>{{ $user['worktime']}}</td></tr>
                <tr><td><i class="material-icons blue-text">schedule</i><span> Breaktime</span></td><td>{{ $user['breaktime']}}</td></tr>
                <tr><td><i class="material-icons overtime-text">schedule</i><span> Overtime</span></td><td>{{ $user['extendedtime']}}</td></tr>
              </table>
            </div>
            <div class="col s12 m6 l3">
              <h6>Breakdown <span class="theme-text" title="Your current timezone">({{Auth::user()->tz}} Time)</span></h6>
              @if(!empty($user['breakdown']))
                <table class="compressed-table">
                  @foreach($user['breakdown'] as $brk)
                    <tr><td>{{ $brk[0] }}</td><td>{{ $brk[1] }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
            <div class="col s12 m12 l3">
              <h6>Activity Reports</h6>
              @if(!empty($user['activity']))
                <table class="compressed-table">
                  @foreach($user['activity'] as $act)
                    <tr><td><i class="tiny material-icons">done</i> {{ $act }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
            <div class="col s12 m12 l3">
              <h6>To Do</h6>
              @if(!empty($user['todo']))
                <table class="compressed-table">
                  @foreach( $user['todo'] as $tod )
                    <tr><td><i class="tiny material-icons">assignment</i> {{ $tod }}</td></tr>
                  @endforeach
                </table>
              @endif
            </div>
          </div>
        </li>
        @endforeach
      </ul>
      @else
        <div class="card-panel green white-text lighten-3">No record found for this date.</div>
      @endif
@stop

@section('js')
<!-- moment -->
<script type="text/javascript" src="{{ asset ("/bower_components/moment/moment.js")  }}"></script>
<!-- jquery-ui js -->
<script type="text/javascript" src="{{ asset ("/bower_components/jquery-ui/jquery-ui.min.js")  }}"></script>
<script type="text/javascript">
$(function() {
    var date = new Date();
    $('.datepicker-button').click(function(){
      $('#datepicker').datepicker('show');  
    });
    $.datepicker.setDefaults({
       dateFormat: 'yy-mm-dd'
    });
    $( "#datepicker" ).datepicker({
      gotoCurrent: true,
      maxDate:new Date(),
      firstDay:1,
      defaultDate: new Date(),
      showOtherMonths: true,
      selectOtherMonths: true,
      onSelect: function() { 
            var date =$(this).datepicker().val();
            window.location = $('#url').val()+"/"+date;
        }
    });
  $(document.documentElement).keyup(function (e) {
    if (e.keyCode == 39){
      nextday();
    }
    if (e.keyCode == 37){
      prevday();   
    } 
  });
});
      function prevday()
      {
        var date = moment($('#current').val(),'YYYY-MM-DD').subtract(1,'days').format('YYYY-MM-DD');
        window.location = $('#url').val()+"/"+date;
      }
      function nextday()
      {
        var date = moment($('#current').val(),'YYYY-MM-DD').add(1,'days').format('YYYY-MM-DD');
        if (new Date(date) > new Date()) {
          Materialize.toast('You can`t select date higher than the current date.',1000,'theme white-text');
        }else{
          window.location = $('#url').val()+"/"+date;
        }
      }

</script>
@stop
  