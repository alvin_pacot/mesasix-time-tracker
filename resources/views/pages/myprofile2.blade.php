@extends('master.admindashboardlayout')
@section('page-title') Profile @endsection
@section('css')
<style type="text/css">
  .card {
    margin: 10px !important;
    background: #FAFAFA;
  }
  .card-content{ padding: 10px 20px !important; }
  .img-title{
    position: relative;
    line-height: 50px;
    font-size: 30px;
    font-weight: normal;
  }
  .profile-wrapper{
    padding: 10px !important;
  }
  .qoute{ padding: 10px !important; height: 115px;}
  img.uavatar {
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-radius: 50%;
  }
  .frame{float: left;}
</style>
@stop
@section('content')
<div class="profile-wrapper">
	   <div class="row">
        <div class="col s12 m12 l7">
          <div class="card">
            <div class="row profile-wrapper">
              <div class="frame">
                    <img class="uavatar"  src="{{Auth::user()->user_avatar}}">
              </div>
              <div class="card-content frame">
                <span class="img-title theme-text">{{ ucwords(Auth::user()->real_name) }}</span><br/>
                @if(empty(Auth::user()->title))
                  <span class="grey-text">{{ env('DEF_TITLE') }}</span>
                @else
                  <span class="grey-text">{{ ucwords(Auth::user()->title) }}</span>
                @endif
                <p>{{Auth::user()->user_email}}</p>
                <span><a id="changepass" href="#">Change password</a></span>
                <p><a id="updateInfo" href="{{route('dashboard::updateuserinfo')}}">Update Info From Slack</a></p>
                @if($stat['cmd'] == 'out')
                  <span>Status: <span id="stat1" class="red-text">Not sign in</span></span>
                @else
                  <span>Status: <span id="stat2" class="theme-text">{{$stat['cmd']}}</span></span>
                  <p>Current Shift: <span id="shift" class="theme-text">{{$stat['shift']}}</span></p>
                @endif
              </div>
            </div>
            <hr/>
            <div class="row qoute">
              <blockquote class="theme-text">{{ $qoute or 'Welcome to the '.env('APP_NAME').' dashboard'}}</blockquote>
            </div>
          </div>
        </div>
        <div class="col s12 m12 l5">
        <form action="{{route('changepass')}}" method="post" id="form">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="card grey lighten-5"  id="password-form" data-stat="{{ $error or 'hide'}}">
                <div class="card-content white-text">
                  <h6 class="theme-text">Change Password</h6>
                  <div class="input-field">
                    <i class="material-icons prefix teal-text">vpn_key</i>
                    <input id="old" type="password" name="old" class="validate black-text" autofocus required >
                    <label for="old" data-error="Invalid password">Current password</label>
                  </div>
                  <div class="input-field">
                    <i class="material-icons prefix teal-text">lock</i>
                    <input disabled="true" name="new" id="new" type="password" class="validate black-text" required autocomplete="false">
                    <label for="new" data-error="Minimum of 6 characters">New password</label>
                  </div>
                  <div class="input-field">
                    <i class="material-icons prefix teal-text">lock</i>
                    <input disabled="true" id="confirm" type="password" class="validate black-text" required autocomplete="false">
                    <label for="confirm" data-error="New password and confirm password not match!">Confirm password</label>
                  </div>
                </div>
                <div class="card-action" style="overflow:hidden;">
                  <div class="right full-width-on-small">
                    <button class="cancel btn wave waves-effect gray lighten-1 theme-text" type="reset" id="cancel">Cancel</button>
                    <button disabled="true" class="submit btn wave waves-effect lighten-2 theme white-text" type="submit" id="submit">Change</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        </div>
      </div><!-- end 1st row -->
</div>
<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
@stop
@section('js')
<script type="text/javascript">
$(window).load(function() {
  setTimeout(function() {
    Materialize.toast('<span>Hello {{Auth::user()->user_name}} !</span>', 1500,'theme white-text');
  }, 1500);
});
$(function(){
    if ($("#password-form").data('stat') === 'hide') {
      $("#password-form").hide();
    }
    /*setTimeout(function(){
      $.ajax({
                 url: '{{route("dashboard::mytime",["user"=>Auth::user()->user_name,"team"=>Auth::user()->getTeamDomain()->team_domain])}}',
                 data: { _token:$("#_token").val() },
                 type: 'POST',
                 dataType: 'json',
                 success: function(response){
                   if((response.response['cmd']).trim() == 'out'){
                    $('#stat1').text('Not sign in');
                   }else{
                    $('#stat2').text(response.response['cmd']);
                    $('#shift').text(response.response['shift']);
                   }
                   // console.log(response.response['shift']);
                 },
                 error: function(e){
                   console.log(e.responseText);
                 }
               });
     }, 5000);*/
    $("#old").keyup(function(){
        if($(this).val().length >= 6){
          $('#new').prop('disabled',false);
          $(this).css("border-bottom","1px solid #4CAF50");
        }else{
          $('#new').prop('disabled',true);
          $(this).css("border-bottom","1px solid #E61A05");
        }
    });
    $("#new").keyup(function(){
        if($(this).val().length >= 6){
          $('#confirm').prop('disabled',false);
          $(this).css("border-bottom","1px solid #4CAF50");
        }else{
          $('#confirm').prop('disabled',true);
          $(this).css("border-bottom","1px solid #E61A05");
        }
    });
    $("#confirm").keyup(function(){
        if($(this).val().length >= 6){
          $('#submit').prop('disabled',false);
          $(this).css("border-bottom","1px solid #4CAF50");
        }else{
          $('#submit').prop('disabled',true)
          $(this).css("border-bottom","1px solid #E61A05");
        }
    });
    $("#form").submit(function(event){
      if (($("#new").val() !== $("#confirm").val())) {
        Materialize.toast('Password not match',4000,'theme white-text');
        event.preventDefault();
      }else if($("#new").val().length < 6 || $("#old").val().length < 6) {
        Materialize.toast('Password should be at least 6 characters',4000,'theme white-text');
        event.preventDefault();
      }else if($("#new").val() === $("#old").val()) {
        Materialize.toast('New password should not be the same with your current password!',4000,'theme white-text');
        event.preventDefault();
      }
    });
    $("#changepass").click(function(){
        $("#password-form").show('fast');
    });
    $("#cancel").click(function(){
        $("#password-form").hide('fast');
    });
});
</script>
@stop