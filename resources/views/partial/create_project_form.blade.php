<div id="create-project" class="modal theme" style="padding:15px">
    <form id="formCreate" action="{{route('dashboard::createproject',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content row" style="background:#fafafa;padding:20px!important;">
      <h5 class="theme-text">New Project</h5>
      <div class="col s12 m12 l12 input-field">
        <input id="name" name="name" type="text" class="black-text" required>
        <label for="name">Project Name (Required)</label>
      </div>
      <div class="col s12 m12 l12 input-field">
        <input type="text" id="desc" name="desc" class="black-text">
        <label for="desc">Description (Optional)</label>
      </div>
      <div class="col s12 m6 l6 input-field">
        <input type="text" id="datepicker-start" class="cursor black-text" name="start" readonly required placeholder="Select date">
        <label for="datepicker-start">Project Start (Required)</label>
      </div>
      <div class="col s12 m6 l6 input-field space">
        <input type="text" id="datepicker-end" class="cursor black-text" name="end" readonly placeholder="Select date">
        <label for="datepicker-end">Project End (Optional)</label>
      </div>
      <div class="input-field col s12 m6 l6">
        <select class="browser-default cursor" name="channel" id="channel">
          <option value="0" disabled selected>Select channel to post</option>
          @if(!empty($channels))
            @foreach($channels as $channel)
              <option value="#{{$channel['channel_name']}}">#{{$channel['channel_name']}}</option>
            @endforeach
          @endif
        </select>
      </div>
      <div class="col s12 m6 l6 input-field space">
        <input id="stat" type="text" class="black-text disabled" name="stat" required readonly value="NOT YET STARTED">
        <label for="stat">Project Status</label>
      </div>
      <div class="col s12 m6 l6 input-field">
          <input type="checkbox" class="filled-in" id="notify-create" name="notify" checked="checked" />
          <label for="notify-create">Send notification to channel</label>
      </div>
    </div>
    <div class="modal-footer" style="padding:0px 20px 10px 20px;">
      <button type="reset" class="modal-action modal-close wave waves-effect lighten-4 grey theme-text btn btn-small">CANCEL</button>
      <button type="submit" class="btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">CREATE</button>
    </div>
    </form>
</div>
