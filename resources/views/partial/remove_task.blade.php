<!-- Modal Structure -->
  <div id="remove" class="modal theme" style="padding:15px">
    <form action="{{route('dashboard::removeprojecttask',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content" style="background:#fafafa;">
      <h4 class="theme-text">Remove Task</h4>
      <h6>Are you sure you want to remove <span id="username"></span> task in this project?</h6>
      <input type="hidden" value="" id="removepuid" name="project_user_id">
      <input type="hidden" value="" id="removeprojectid" name="project_id">
      <input type="hidden" value="" id="removeuserid" name="user_id">
      <p class="notify-checkbox">
          <input type="checkbox" class="filled-in" id="filled-in-box3" name="notifyChannelRemove"/>
          <label for="filled-in-box3">Send notification to project channel</label>
      </p>
      <p class="notify-checkbox">
          <input type="checkbox" class="filled-in" id="filled-in-box4" name="notifyUserRemove" />
          <label for="filled-in-box4">Send private notification to user</label>
      </p>
    </div>
    <div class="modal-footer">
      <button type="reset" class="wave waves-effect lighten-4 grey theme-text btn btn-small modal-close">CANCEL</button>
      <button type="submit" class="btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">YES</a>
    </div>
    </form>
  </div>
