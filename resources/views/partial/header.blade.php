<nav>
    <div class="nav-wrapper">    
        <ul id="signout" class="dropdown-content">
            <li><a href="{{ route('home')}}">Home</a></li>
            <li class="{{set_active('dashboard/*/*/myprofile')}}"" ><a href="{{route('dashboard::myprofile',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name])}}">Profile</a></li>
            <li><a href="{{ route('logout')}}">Logout</a></li>
        </ul>
        <span id="toggle" class="menu-btn sprite left" data-name="collapse"></span>
        <h4 class="brand-logo team-name">&nbsp; {{ ucfirst( Auth::user()->getTeamName()->team_name ) }} <span class="flow-text">&nbsp;@yield('page-title')</span></h4>
        <div class="avatar-group right hide-on-small-only valign-wrapper dropdown-button" style="margin-right:30px;cursor:pointer;" data-beloworigin="true"  data-activates="signout">
           <img src="{{ Auth::user()->user_avatar }}" class="circle avatar2 " width="40">
           <a class="black-text">Welcome <strong class="blue-text">{{ Auth::user()->user_name}}</strong></a>
           <a class="greeting sprite nav-img-signout" href="#!"></a>
        </div>
    </div>
</nav>
