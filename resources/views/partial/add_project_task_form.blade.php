<!-- Modal Structure -->
<div id="add-task" class="modal theme" style="padding:15px !important;">
    <form id="formAddTask" action="{{route('dashboard::addprojecttask',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content row" style="background:#fafafa;padding:20px !important;">
      <h5>Assign Task</h5>
      <input type="hidden" name="project_id" id="project_id" value="">
      <div class="col s12 m12 l12 input-field">
        <input id="task" name="task" type="text" class="black-text" required>
        <label for="task">Task Description (Required)</label>
      </div>
      <div class="input-field col s12 m12 l12">
        <select class="browser-default cursor" name="user_id" id="assigned">
          <option value="0" disabled selected>Assign to : (Required)</option>
          @if(!empty($userslist))
            @foreach($userslist as $user)
              <option value="{{$user['user_id']}}" data-icon="{{$user['avatar']}}" class="circle">{{$user['user_name']}}</option>
            @endforeach
          @endif
        </select>
      </div>
      <div class="input-field col s12 m12 l12">
          <input type="checkbox" class="filled-in" id="filled-in-box1" name="notifyChannel" checked="checked" />
          <label for="filled-in-box1">Send notification to project channel</label>
      <br/>
          <input type="checkbox" class="filled-in" id="filled-in-box2" name="notifyUser" checked="checked" />
          <label for="filled-in-box2">Send private notification to user</label>
      </div>
    </div>
    <div class="modal-footer" style="padding:0px 20px !important;">
        <button type="reset" class="modal-action modal-close wave waves-effect lighten-4 grey theme-text btn btn-small">CANCEL</button>
        <button type="submit" class=" btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">ASSIGN</button>
    </div>
    </form>
</div>