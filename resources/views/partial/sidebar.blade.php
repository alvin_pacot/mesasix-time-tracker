
  <div class="inner-sidebar-large">
    <!-- logo -->
    <div class="theme full-width">
      <a class="sprite-logo logo"></a>
      <a href="#" data-activates="dashboard-nav" onclick="Materialize.showStaggeredList('#dashboard-nav')" class="dashboard button-collapse hide-on-large-only white-text"><i class="mdi-navigation-menu "></i></a>
    </div>
    <!-- Sidebar Menu Med and up-->
    <ul class="sidebar-menu hide-on-med-and-down">
        <li class="{{ set_active('dashboard/*/*/mysched') }} {{ set_active('dashboard/*/*/teamsched/*') }} menu-item waves-effect waves-orange tooltipped" data-position="right" data-delay="50" data-tooltip="Team Schedule">
          <a  href="{{ route('dashboard::mysched',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="sprite nav-img-schedule"><span class="nav-text">Team Schedule</span></a>
        </li>
        @if(auth()->user()->isAdminOrOwner())
          <li class="{{ set_active('dashboard/*/*/weekly/*/*/*') }} menu-item  waves-effect waves-orange tooltipped" data-position="right" data-delay="50" data-tooltip="Member Records">
            <a  href="{{ route('dashboard::theyrecords',['id'=>Auth::user()->user_id,'week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="sprite nav-img-my-record"><span class="nav-text">Member Records</span></a>
          </li>
        @else
          <li class="{{ set_active('dashboard/*/*/myweekly/*/*')}} menu-item  waves-effect waves-orange tooltipped" data-position="right" data-delay="50" data-tooltip="My Records">
            <a  href="{{ route('dashboard::myrecords',['week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" class="sprite nav-img-my-record"><span class="nav-text">My Records</span></a>
          </li>
        @endif
        <li class="{{ set_active('dashboard/*/teamactivity/*')}} menu-item waves-effect waves-orange tooltipped" data-position="right" data-delay="50" data-tooltip="Team Records">
          <a  href="{{ route('dashboard::teamactivity', ['team'=>Auth::user()->getTeamDomain()->team_domain,'date'=>date('Y-m-d',(time()-86400))]) }}" class="sprite nav-img-team-record"><span class="nav-text">Team Records</span></a>
        </li>
        @if(auth()->user()->isAdminOrOwner())
        <li class="{{ set_active('dashboard/*/teamproject')}} menu-item waves-effect waves-orange tooltipped" data-position="right" data-delay="50" data-tooltip="Team Projects">
          <a href="{{ route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]) }}" class="sprite nav-img-project"><span class="nav-text">Team Projects</span></a>
        </li>
        <li class="{{ set_active('dashboard/*/teamsetting')}} menu-item waves-effect waves-orange tooltipped" data-position="right" data-delay="50" data-tooltip="Team Setting">
          <a href="{{ route('dashboard::teamsetting',['team'=>Auth::user()->getTeamDomain()->team_domain]) }}" class="sprite nav-img-setting"><span class="nav-text">Team Setting</span></a>
        </li>
        @endif
        <li class=""><a class="sprite nav-img-footer" title="Mesasix"><p class="nav-text nav-footer-text">&nbsp;powered by mesasix</p></a></li>
    </ul>
    <ul class="side-nav hide-on-large-only" id="dashboard-nav">
        <li class="valign-wrapper " style="padding: 15px 25px;cursor: default;">
            <img src="{{ Auth::user()->user_avatar }}" class="circle" width="40">
            <h6 class="theme-text" >&nbsp;{{ Auth::user()->user_name }}</h6>
        </li>
        <li class="full-width waves-effect waves-orange ">
          <a class="theme-text nav-text" href="{{ route('home')}}">Home</a>
        </li>
        <li class="{{ set_active('dashboard/*/*/myprofile') }} full-width waves-effect waves-orange ">
          <a class="theme-text nav-text" href="{{route('dashboard::myprofile',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name])}}">Profile</a>
        </li>
        <li class="{{ set_active('dashboard/*/*/mysched') }} {{ set_active('dashboard/*/*/teamsched/*') }}  full-width waves-effect waves-orange ">
          <a class="theme-text nav-text"  href="{{ route('dashboard::mysched',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}" title="Team Schedule">Team Schedule</a>
        </li>
        @if(auth()->user()->isAdminOrOwner())
        <li class="{{ set_active('dashboard/*/*/weekly/*/*/*') }} full-width waves-effect waves-orange">
          <a  class="theme-text nav-text" href="{{ route('dashboard::theyrecords',['id'=>Auth::user()->user_id,'week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}"  title="Members Records">Member Records</a>
        </li>
        @else
        <li class="{{ set_active('dashboard/*/*/myweekly/*/*')}} full-width waves-effect waves-orange">
          <a class="theme-text nav-text"  href="{{ route('dashboard::myrecords',['week'=>date('W'),'year'=>date('Y'),'team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name]) }}"  title="My Records">My Records</a>
        </li>
        @endif
        <li class="{{ set_active('dashboard/*/teamactivity/*')}} full-width waves-effect waves-orange">
          <a class="theme-text nav-text"  href="{{ route('dashboard::teamactivity', ['team'=>Auth::user()->getTeamDomain()->team_domain,'date'=>date('Y-m-d',time())]) }}" title="Team Records">Team Records</a>
        </li>
         @if(auth()->user()->isAdminOrOwner())
        <li class="{{ set_active('dashboard/*/teamproject')}} full-width waves-effect waves-orange ">
          <a class="theme-text nav-text" href="{{ route('dashboard::teamproject',['team'=>Auth::user()->getTeamDomain()->team_domain]) }}" title="Team Projects">Team Projects</a>
        </li>
        <li class="{{ set_active('dashboard/*/teamsetting')}} full-width waves-effect waves-orange ">
          <a class="theme-text nav-text" href="{{ route('dashboard::teamsetting',['team'=>Auth::user()->getTeamDomain()->team_domain]) }}" title="Team Setting">Team Setting</a>
        </li>
        @endif
        <li class="full-width waves-effect waves-orange "><a class="theme-text nav-text" href="{{ route('logout')}}">Logout</a></li>
      </ul>
</div>