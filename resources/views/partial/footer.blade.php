<footer class="page-footer indigo lighten-2">
    <div class="container">
      <div class="row">
        <div class="col l4 s12">
          <h6 class="white-text">DALLAS OFFICE</h6>
          <p class="grey-text text-lighten-4">
          2430 Victory Park Lane, Suite #2601,<br>
          Dallas, TX, 75219 USA <br>
          Phone: (443)-MESA-SIX, (443)-637-2749<br>
          Email: info@mesasix.com</p>
        </div>
        <div class="col l4 s12">
          <h6 class="white-text">ABU DHABI OFFICE</h6>
          <p class="grey-text text-lighten-4">
          P.O. Box 769317, twofour54,<br>
          Khalifa Park, <br>
          Abu Dhabi 769317 UAE<br>
          Phone: +971 55 818 3290 , +971 2 401 2777<br>
          Email: info@mesasix.com</p>
        </div>
        <div class="col l4 s12">
          <h6 class="white-text">PHILIPPINE OFFICE</h6>
          <p class="grey-text text-lighten-4">
          Doromal Bldg,
          Quezon Avenue Ext,<br>
          Iligan City,<br>
          Philippines 9200<br>
          Phone: 222-8488<br>
          Email: info@mesasix.com</p>
        </div>
        
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Powered by <a class="purple-text text-darken-3" href="http://mesasix.com">Mesasix</a>
      </div>
    </div>
  </footer>