<!-- Main Header -->
<nav class="header-wrapper">
	<div class="nav-wrapper row">
	    <div class="col s12 m11 l3 home-logo"  style="height: 70px !important;">
	        <!-- <a href="{{route('home')}}"><img class="home-logo" src="{{ asset('/appimg/logo3.png')}}"></a> -->
	    </div>
		<a href="#" data-activates="home-nav-guest" onclick="Materialize.showStaggeredList('#home-nav-guest')" class="home-guest button-collapse"><i class="mdi-navigation-menu"></i></a>
	    <div class="col s12 l9">
	        <div class="right home-nav hide-on-med-and-down">
	          <ul>
	          	<li class="{{set_active('/')}} waves-effect waves-purple "><a href="{{route('home')}}" class="theme-text">HOME</a></li>
	          	<!-- <li class="{{set_active('explore')}} waves-effect waves-purple "><a href="{{route('explore')}}" class="theme-text">EXPLORE</a></li> -->
	          	<li class="{{set_active('support')}} waves-effect waves-purple "><a href="{{route('support')}}" class="theme-text">SUPPORT</a></li>
	          	<li class="{{set_active('login')}} {{set_active('login/email')}} waves-effect waves-purple "><a href="{{route('login::login')}}" class="theme-text">LOGIN</a></li>
	          	<li class="waves-effect waves-purple "><a class="theme-text" href="{{ route('teamRegStart')}}">REGISTER TEAM</a></li>
	          </ul>
	        </div>
	    </div>
	          <ul class="side-nav" id="home-nav-guest" >
	          	<li class="{{set_active('/')}} waves-effect waves-purple full-width "><a href="{{route('home')}}" class="theme-text">HOME</a></li>
	          	<!-- <li class="{{set_active('explore')}} waves-effect waves-purple full-width "><a href="{{route('home')}}" class="theme-text">EXPLORE</a></li> -->
	          	<li class="{{set_active('support')}} waves-effect waves-purple full-width "><a href="{{route('support')}}" class="theme-text">SUPPORT</a></li>
	          	<li class="{{set_active('login')}} {{set_active('login/email')}} waves-effect waves-purple full-width "><a href="{{route('login::login')}}" class="theme-text">LOGIN</a></li>
	          	<li class="waves-effect waves-purple full-width "><a class="theme-text" href="{{ route('teamRegStart')}}">REGISTER TEAM</a></li>
	          </ul>
    </div>
</nav>