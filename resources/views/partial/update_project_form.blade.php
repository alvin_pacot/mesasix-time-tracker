<div id="update-project" class="modal theme" style="padding:15px">
    <form id="formUpdate" action="{{route('dashboard::updateproject',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content row" style="background:#fafafa;padding:20px!important;">
      <h5 class="theme-text">Update Project</h5>
      <input type="hidden" id="uproject_id" name="uproject_id">
      <div class="col s12 m12 l12 input-field">
        <label class="active" >Project Name (Required)</label>
        <input id="uname" name="name" type="text" class="black-text" required placeholder="Project Name">
        <input type="hidden" id="uoldname" name="oldname">
      </div>
      <div class="col s12 m12 l12 input-field">
        <label class="active" >Description (Optional)</label>
        <input type="text" id="udesc" name="desc" value="" class="black-text" placeholder="Project Description">
      </div>
      <div class="col s12 m6 l6 input-field">
        <label class="active" >Project Start (Required)</label>
        <input type="text" id="udatepicker-start" value="" class="cursor black-text" name="start" readonly required placeholder="Select date">
      </div>
      <div class="col s12 m6 l6 input-field space">
        <label class="active" >Project End (Optional)</label>
        <input type="text" id="udatepicker-end" class="cursor black-text" value="" name="end" readonly placeholder="Select date">
      </div>
      <div class="input-field col s12 m6 l6">
        <select class="browser-default cursor" name="channel" id="uchannel">
          <option value="0" disabled>Channel to post : (Required)</option>
          @if(!empty($channels))
            @foreach($channels as $channel)
              <option value="#{{$channel['channel_name']}}">#{{$channel['channel_name']}}</option>
            @endforeach
          @endif
        </select>
      </div>
      <div class="col s12 m6 l6 input-field space">
        <select class="browser-default cursor" name="stat" id="ustat">
            <option value="" disabled>SET PROJECT STATUS</option>
            <option value="NOT YET STARTED">NOT YET STARTED</option>
            <option value="ONGOING">ONGOING</option>
            <option value="COMPLETED">COMPLETED</option>
            <option value="DUE">DUE</option>
        </select>
      </div>
      <div class="col s12 m6 l6 input-field">
          <input type="checkbox" class="filled-in" id="filled-in-box" name="notify" checked="checked" />
          <label for="filled-in-box">Send notification to channel</label>
      </div>
    </div>
    <div class="modal-footer" style="padding:0px 20px 10px 20px;">
      <button type="reset" class="modal-action modal-close wave waves-effect lighten-4 grey theme-text btn btn-small">CANCEL</button>
      <button type="submit" class="btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">UPDATE</button>
    </div>
    </form>
</div>
