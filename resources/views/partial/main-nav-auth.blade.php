<!-- Main Header -->
<nav class="header-wrapper">
  <div class="nav-wrapper row">
      <div class="col s12 m11 l3 home-logo" style="height:70px !important;">
          <!-- <img class="home-logo" src="{{ asset('/appimg/logo3.png')}}"> -->
      </div>
      <a href="#" data-activates="home-nav-auth" onclick="Materialize.showStaggeredList('#home-nav-auth')" class="home-auth button-collapse"><i class="mdi-navigation-menu"></i></a>
      <div class="col s12 l9  hide-on-med-and-down">
          <div class="right valign-wrapper home-nav-avatar hide-on-small-only ">
            <h6 class="theme-text" style="margin-right:10px;">{{ Auth::user()->user_name }}</h6>
            <img src="{{ Auth::user()->user_avatar }}" class="circle avatar2" width="40">
          </div>
          <div class="right home-nav">
            <ul>
              @yield('nav-option')
              <li class="{{set_active('/')}} waves-effect waves-purple "><a href="{{route('home')}}" class="theme-text">HOME</a></li>
              <!-- <li class="{{set_active('explore')}} waves-effect waves-purple "><a href="{{route('home')}}" class="theme-text">EXPLORE</a></li> -->
              <li class="{{set_active('support')}} waves-effect waves-purple "><a href="{{route('support')}}" class="theme-text">SUPPORT</a></li>
              <li class="waves-effect waves-purple "><a href="{{route('dashboard::myprofile',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name])}}" class="theme-text">DASHBOARD</a></li>
              <li class="waves-effect waves-purple "><a class="theme-text tooltipped" href="{{ route('logout')}}" data-position="bottom" data-delay="50" data-tooltip="See you later. {{Auth::user()->user_name}}" class="theme-text">LOGOUT</a></li>
            </ul>
          </div> 
      </div>
            <ul class="side-nav" id="home-nav-auth">
              <li class="valign-wrapper" style="padding: 15px 25px;cursor: default;">
                <img src="{{ Auth::user()->user_avatar }}" class="circle" width="40">
                <h6 class="theme-text" >&nbsp;{{ Auth::user()->user_name }}</h6>
              </li>
              <li class="{{set_active('/')}} waves-effect waves-purple full-width "><a href="{{route('home')}}" class="theme-text">HOME</a></li>
              <!-- <li class="{{set_active('explore')}} waves-effect waves-purple full-width "><a href="{{route('home')}}" class="theme-text">EXPLORE</a></li> -->
              <li class="{{set_active('support')}} waves-effect waves-purple full-width "><a href="{{route('support')}}" class="theme-text">SUPPORT</a></li>
              <li class="waves-effect waves-purple full-width "><a href="{{route('dashboard::myprofile',['team'=>Auth::user()->getTeamDomain()->team_domain,'user'=>Auth::user()->user_name])}}" class="theme-text">DASHBOARD</a></li>
              <li class="waves-effect waves-purple full-width "><a class="theme-text tooltipped" href="{{ route('logout')}}" data-position="bottom" data-delay="50" data-tooltip="See you later. {{Auth::user()->user_name}}" class="theme-text">LOGOUT</a></li>
            </ul>
    </div>
</nav>
