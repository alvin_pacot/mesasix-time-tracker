  <div id="remove-project" class="modal theme" style="padding:15px">
    <form action="{{route('dashboard::removeproject',['team'=>auth()->user()->getTeamDomain()->team_domain])}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-content" style="background:#fafafa;">
      <h5 class="theme-text">Remove Project</h5>
      <h6>Are you sure you want to remove <span id="projectname"></span>? <br/>All related task will be deleted as well</h6>
      <input type="hidden" value="" id="delprojectid" name="project_id">
      <p class="notify-checkbox">
          <input type="checkbox" class="filled-in" id="filled-in-box5" name="notifyChannelRemove"/>
          <label for="filled-in-box5">Send notification to project channel</label>
      </p>
    </div>
    <div class="modal-footer">
      <button type="reset" class="wave waves-effect lighten-4 grey theme-text btn btn-small modal-close">CANCEL</button>
      <button type="submit" class="btn btn-small wave theme waves-effect lighten-4" style="margin-right:5px;">YES</a>
    </div>
    </form>
  </div>